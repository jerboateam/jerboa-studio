package fr.up.xlim.ig.jerboa.studio.suppression;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;

/* Raw Imports : */
import fr.up.xlim.ig.jerboa.studio.sew.UnSewA3;
import fr.up.xlim.ig.jerboa.studio.suppression.DeleteConnex;

/* End raw Imports */



/**
 * 
 */



public class IsoAndDeleteVolume extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public IsoAndDeleteVolume(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "IsoAndDeleteVolume", "suppression");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        for(JerboaDart start : hooks){
		   try{
		      for(JerboaDart dart : gmap.collect(start,JerboaOrbit.orbit(0,1,2),JerboaOrbit.orbit(0,1))){
		         if(!(dart.isFree(3))) {
		            JerboaInputHooksGeneric _v_hook1 = new JerboaInputHooksGeneric();
		            _v_hook1.addCol(dart);
		            ((UnSewA3)modeler.getRule("UnSewA3")).applyRule(gmap, _v_hook1);
		         }
		      }
		      
		      JerboaInputHooksGeneric _v_hook2 = new JerboaInputHooksGeneric();
		      _v_hook2.addCol(start);
		      ((DeleteConnex)modeler.getRule("DeleteConnex")).applyRule(gmap, _v_hook2);
		   }
		   catch(JerboaException je){
		      
		   }
		   finally{}
		}
		
		return null;
		// END SCRIPT GENERATION

	}
    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class