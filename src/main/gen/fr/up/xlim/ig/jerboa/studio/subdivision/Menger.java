package fr.up.xlim.ig.jerboa.studio.subdivision;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;



/**
 * 
 */



public class Menger extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public Menger(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "Menger", "subdivision");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2,3), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(-1,1,2,3), 3);
        JerboaRuleNode rn1 = new JerboaRuleNode("n1", 1, JerboaOrbit.orbit(-1,-1,2,3), 3, new MengerExprRn1position());
        JerboaRuleNode rn2 = new JerboaRuleNode("n2", 2, JerboaOrbit.orbit(0,-1,2,3), 3, new MengerExprRn2color(), new MengerExprRn2normal());
        JerboaRuleNode rn3 = new JerboaRuleNode("n3", 3, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn4 = new JerboaRuleNode("n4", 4, JerboaOrbit.orbit(-1,-1,1,-1), 3, new MengerExprRn4color(), new MengerExprRn4normal());
        JerboaRuleNode rn5 = new JerboaRuleNode("n5", 5, JerboaOrbit.orbit(-1,-1,1,-1), 3, new MengerExprRn5color(), new MengerExprRn5normal());
        JerboaRuleNode rn6 = new JerboaRuleNode("n6", 6, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn7 = new JerboaRuleNode("n7", 7, JerboaOrbit.orbit(-1,1,-1,3), 3, new MengerExprRn7position());
        JerboaRuleNode rn8 = new JerboaRuleNode("n8", 8, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn9 = new JerboaRuleNode("n9", 9, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn10 = new JerboaRuleNode("n10", 10, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn11 = new JerboaRuleNode("n11", 11, JerboaOrbit.orbit(0,-1,-1,3), 3);
        JerboaRuleNode rn12 = new JerboaRuleNode("n12", 12, JerboaOrbit.orbit(0,-1,-1,-1), 3, new MengerExprRn12color(), new MengerExprRn12normal());
        JerboaRuleNode rn13 = new JerboaRuleNode("n13", 13, JerboaOrbit.orbit(-1,2,-1,-1), 3);
        JerboaRuleNode rn14 = new JerboaRuleNode("n14", 14, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn15 = new JerboaRuleNode("n15", 15, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn16 = new JerboaRuleNode("n16", 16, JerboaOrbit.orbit(-1,2,1,-1), 3, new MengerExprRn16position());
        JerboaRuleNode rn17 = new JerboaRuleNode("n17", 17, JerboaOrbit.orbit(-1,-1,1,-1), 3);
        JerboaRuleNode rn18 = new JerboaRuleNode("n18", 18, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn19 = new JerboaRuleNode("n19", 19, JerboaOrbit.orbit(0,-1,2,-1), 3);
        right.add(rn0);
        right.add(rn1);
        right.add(rn2);
        right.add(rn3);
        right.add(rn4);
        right.add(rn5);
        right.add(rn6);
        right.add(rn7);
        right.add(rn8);
        right.add(rn9);
        right.add(rn10);
        right.add(rn11);
        right.add(rn12);
        right.add(rn13);
        right.add(rn14);
        right.add(rn15);
        right.add(rn16);
        right.add(rn17);
        right.add(rn18);
        right.add(rn19);
        rn1.setAlpha(0, rn0);
        rn3.setAlpha(1, rn1);
        rn4.setAlpha(2, rn3);
        rn5.setAlpha(3, rn4);
        rn6.setAlpha(1, rn2);
        rn6.setAlpha(2, rn5);
        rn7.setAlpha(0, rn3);
        rn8.setAlpha(0, rn4);
        rn8.setAlpha(2, rn7);
        rn9.setAlpha(0, rn5);
        rn9.setAlpha(3, rn8);
        rn10.setAlpha(0, rn6);
        rn10.setAlpha(2, rn9);
        rn11.setAlpha(1, rn10);
        rn12.setAlpha(2, rn11);
        rn12.setAlpha(3, rn12);
        rn13.setAlpha(1, rn8);
        rn14.setAlpha(1, rn9);
        rn14.setAlpha(3, rn13);
        rn15.setAlpha(1, rn12);
        rn15.setAlpha(2, rn14);
        rn15.setAlpha(3, rn15);
        rn16.setAlpha(0, rn13);
        rn17.setAlpha(0, rn14);
        rn17.setAlpha(3, rn16);
        rn18.setAlpha(0, rn15);
        rn18.setAlpha(2, rn17);
        rn18.setAlpha(3, rn18);
        rn19.setAlpha(1, rn18);
        rn19.setAlpha(3, rn19);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return -1;
        case 2: return -1;
        case 3: return -1;
        case 4: return -1;
        case 5: return -1;
        case 6: return -1;
        case 7: return -1;
        case 8: return -1;
        case 9: return -1;
        case 10: return -1;
        case 11: return -1;
        case 12: return -1;
        case 13: return -1;
        case 14: return -1;
        case 15: return -1;
        case 16: return -1;
        case 17: return -1;
        case 18: return -1;
        case 19: return -1;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return 0;
        case 2: return 0;
        case 3: return 0;
        case 4: return 0;
        case 5: return 0;
        case 6: return 0;
        case 7: return 0;
        case 8: return 0;
        case 9: return 0;
        case 10: return 0;
        case 11: return 0;
        case 12: return 0;
        case 13: return 0;
        case 14: return 0;
        case 15: return 0;
        case 16: return 0;
        case 17: return 0;
        case 18: return 0;
        case 19: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

    private class MengerExprRn1position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.3333333134651184);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(0.6666666865348816);
res.add(p1);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class MengerExprRn2color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class MengerExprRn2normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class MengerExprRn4color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class MengerExprRn4normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class MengerExprRn5color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class MengerExprRn5normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class MengerExprRn7position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.3333333134651184);
res.add(p0);
Point3 p2 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,3),0));
p2.scale(0.6666666865348816);
res.add(p2);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class MengerExprRn12color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class MengerExprRn12normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class MengerExprRn16position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.3333333134651184);
res.add(p0);
Point3 p3 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,2),0));
p3.scale(0.6666666865348816);
res.add(p3);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class