package fr.up.xlim.ig.jerboa.studio.extrusion;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;

/* Raw Imports : */
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeFaceArg;

/* End raw Imports */



/**
 * 
 */



public class ExtrudeFaceAxisUI extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public ExtrudeFaceAxisUI(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "ExtrudeFaceAxisUI", "extrusion");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        right.add(rn0);
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        Point3 axis = new Point3(hooks.dart(0, 0).<fr.up.xlim.ig.jerboa.ebds.Normal3>ebd(3));
		axis = Point3.askPoint("Enter normal vector: ",axis);
		JerboaInputHooksGeneric _v_hook0 = new JerboaInputHooksGeneric();
		_v_hook0.addCol(hooks.dart(0, 0));
		((ExtrudeFaceArg)modeler.getRule("ExtrudeFaceArg")).setVect(axis);
		return ((ExtrudeFaceArg)modeler.getRule("ExtrudeFaceArg")).applyRule(gmap, _v_hook0);
		// END SCRIPT GENERATION

	}
    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class