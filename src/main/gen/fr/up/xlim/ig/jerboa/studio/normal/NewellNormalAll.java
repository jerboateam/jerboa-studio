package fr.up.xlim.ig.jerboa.studio.normal;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;

/* Raw Imports : */
import fr.up.xlim.ig.jerboa.studio.normal.NewellNormalConnex;

/* End raw Imports */



/**
 * 
 */



public class NewellNormalAll extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public NewellNormalAll(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "NewellNormalAll", "normal");

        // -------- LEFT GRAPH

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        JerboaMark marker = gmap.creatFreeMarker();
		for(JerboaDart dart : gmap){
		   if(dart.isNotMarked(marker)) {
		      JerboaInputHooksGeneric _v_hook1 = new JerboaInputHooksGeneric();
		      _v_hook1.addCol(dart);
		      ((NewellNormalConnex)modeler.getRule("NewellNormalConnex")).applyRule(gmap, _v_hook1);
		      gmap.markOrbit(dart,JerboaOrbit.orbit(0,1,2,3), marker);
		   }
		}
		
		return null;
		// END SCRIPT GENERATION

	}
} // end rule Class