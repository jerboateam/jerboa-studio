package fr.up.xlim.ig.jerboa.studio.suppression;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;

/* Raw Imports : */
import fr.up.xlim.ig.jerboa.studio.sew.UnSewA2;
import fr.up.xlim.ig.jerboa.studio.suppression.DeleteFace;

/* End raw Imports */



/**
 * 
 */



public class IsoAndDeleteFace extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public IsoAndDeleteFace(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "IsoAndDeleteFace", "suppression");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        for(JerboaDart start : hooks){
		   try{
		      for(JerboaDart dart : gmap.collect(start,JerboaOrbit.orbit(0,1,3),JerboaOrbit.orbit())){
		         if(!(dart.isFree(2))) {
		            JerboaInputHooksGeneric _v_hook1 = new JerboaInputHooksGeneric();
		            _v_hook1.addCol(dart);
		            ((UnSewA2)modeler.getRule("UnSewA2")).applyRule(gmap, _v_hook1);
		         }
		      }
		      
		      JerboaInputHooksGeneric _v_hook2 = new JerboaInputHooksGeneric();
		      _v_hook2.addCol(start);
		      ((DeleteFace)modeler.getRule("DeleteFace")).applyRule(gmap, _v_hook2);
		   }
		   catch(JerboaException je){
		      
		   }
		   finally{}
		}
		
		return null;
		// END SCRIPT GENERATION

	}
    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class