package fr.up.xlim.ig.jerboa.studio.subdivision;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;



/**
 * 
 */



public class CatmullClark extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 

	protected List<Point3> anchors; 

	// END PARAMETERS 



    public CatmullClark(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "CatmullClark", "subdivision");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2), 3);
        left.add(ln0);
        hooks.add(ln0);
        ln0.setAlpha(3, ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(-1,1,2), 3, new CatmullClarkExprRn0position());
        JerboaRuleNode rn1 = new JerboaRuleNode("n1", 1, JerboaOrbit.orbit(-1,-1,2), 3, new CatmullClarkExprRn1position(), new CatmullClarkExprRn1orient());
        JerboaRuleNode rn2 = new JerboaRuleNode("n2", 2, JerboaOrbit.orbit(2,-1,-1), 3, new CatmullClarkExprRn2orient());
        JerboaRuleNode rn3 = new JerboaRuleNode("n3", 3, JerboaOrbit.orbit(2,1,-1), 3, new CatmullClarkExprRn3orient(), new CatmullClarkExprRn3position());
        right.add(rn0);
        right.add(rn1);
        right.add(rn2);
        right.add(rn3);
        rn0.setAlpha(0, rn1);
        rn1.setAlpha(1, rn2);
        rn2.setAlpha(0, rn3);
        rn0.setAlpha(3, rn0);
        rn1.setAlpha(3, rn1);
        rn2.setAlpha(3, rn2);
        rn3.setAlpha(3, rn3);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
        anchors = new ArrayList<>();;    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return -1;
        case 2: return -1;
        case 3: return -1;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return 0;
        case 2: return 0;
        case 3: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0, List<Point3> anchors) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        setAnchors(anchors);
        return applyRule(gmap, ____jme_hooks);
	}

    private class CatmullClarkExprRn0position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
int n = 0;
java.util.List<fr.up.xlim.ig.jerboa.ebds.Point3> new_face_points = new ArrayList<fr.up.xlim.ig.jerboa.ebds.Point3>();
for(JerboaDart d : gmap.collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),JerboaOrbit.orbit(0,1,3))){
   new_face_points.add(Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(d,JerboaOrbit.orbit(0,1),0)));
}

Point3 avg_face_points = Point3.middle(new_face_points);
java.util.List<fr.up.xlim.ig.jerboa.ebds.Point3> old_edge_midpoints = new ArrayList<fr.up.xlim.ig.jerboa.ebds.Point3>();
for(JerboaDart d : gmap.collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),JerboaOrbit.orbit(0,2,3))){
   old_edge_midpoints.add(Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(d,JerboaOrbit.orbit(0),0)));
   n = (n + 1);
}

Point3 avg_edge_points = Point3.middle(old_edge_midpoints);
Point3 old_coord = new Point3(curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0));
double scalingQ = (1.0 / n);
avg_face_points.scale(scalingQ);
double scalingR = (2.0 / n);
avg_edge_points.scale(scalingR);
double scalingS = ((n - 3.0) / n);
old_coord.scale(scalingS);
old_coord.add(avg_face_points);
old_coord.add(avg_edge_points);
return old_coord;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class CatmullClarkExprRn1position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 face1Mid = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1),0));
Point3 face2Mid = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0).alpha(2),JerboaOrbit.orbit(0,1),0));
Point3 faceMid = Point3.middle(face1Mid,face2Mid);
Point3 edgeMid = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0),0));
return Point3.middle(faceMid,edgeMid);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class CatmullClarkExprRn1orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(2));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class CatmullClarkExprRn2orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<java.lang.Boolean>ebd(2);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class CatmullClarkExprRn3orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(2));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class CatmullClarkExprRn3position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1),0));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

	public List<Point3> getAnchors(){
		return anchors;
	}
	public void setAnchors(List<Point3> _anchors){
		this.anchors = _anchors;
	}
} // end rule Class