package fr.up.xlim.ig.jerboa.studio.subdivision;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;



/**
 * 
 */



public class Menger222 extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public Menger222(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "Menger222", "subdivision");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2,3), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn1 = new JerboaRuleNode("n1", 0, JerboaOrbit.orbit(0,-1,2,-1), 3, new Menger222ExprRn1position(), new Menger222ExprRn1color(), new Menger222ExprRn1normal());
        JerboaRuleNode rn2 = new JerboaRuleNode("n2", 1, JerboaOrbit.orbit(-1,-1,-1,3), 3, new Menger222ExprRn2position(), new Menger222ExprRn2color(), new Menger222ExprRn2normal());
        JerboaRuleNode rn3 = new JerboaRuleNode("n3", 2, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn4 = new JerboaRuleNode("n4", 3, JerboaOrbit.orbit(-1,-1,-1,3), 3, new Menger222ExprRn4position());
        JerboaRuleNode rn5 = new JerboaRuleNode("n5", 4, JerboaOrbit.orbit(-1,-1,-1,-1), 3, new Menger222ExprRn5position(), new Menger222ExprRn5color(), new Menger222ExprRn5normal());
        JerboaRuleNode rn6 = new JerboaRuleNode("n6", 5, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn7 = new JerboaRuleNode("n7", 6, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn8 = new JerboaRuleNode("n8", 7, JerboaOrbit.orbit(0,-1,-1,-1), 3);
        JerboaRuleNode rn9 = new JerboaRuleNode("n9", 8, JerboaOrbit.orbit(-1,-1,-1,3), 3, new Menger222ExprRn9position());
        JerboaRuleNode rn10 = new JerboaRuleNode("n10", 9, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn11 = new JerboaRuleNode("n11", 10, JerboaOrbit.orbit(-1,-1,-1,3), 3, new Menger222ExprRn11position());
        JerboaRuleNode rn12 = new JerboaRuleNode("n12", 11, JerboaOrbit.orbit(0,-1,-1,-1), 3);
        JerboaRuleNode rn13 = new JerboaRuleNode("n13", 12, JerboaOrbit.orbit(-1,-1,1,-1), 3, new Menger222ExprRn13color(), new Menger222ExprRn13normal());
        JerboaRuleNode rn14 = new JerboaRuleNode("n14", 13, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn15 = new JerboaRuleNode("n15", 14, JerboaOrbit.orbit(-1,-1,-1,-1), 3, new Menger222ExprRn15color(), new Menger222ExprRn15normal());
        JerboaRuleNode rn16 = new JerboaRuleNode("n16", 15, JerboaOrbit.orbit(-1,-1,-1,-1), 3, new Menger222ExprRn16color(), new Menger222ExprRn16normal());
        JerboaRuleNode rn17 = new JerboaRuleNode("n17", 16, JerboaOrbit.orbit(-1,-1,-1,-1), 3, new Menger222ExprRn17position());
        JerboaRuleNode rn18 = new JerboaRuleNode("n18", 17, JerboaOrbit.orbit(0,-1,-1,-1), 3, new Menger222ExprRn18position(), new Menger222ExprRn18color(), new Menger222ExprRn18normal());
        JerboaRuleNode rn19 = new JerboaRuleNode("n19", 18, JerboaOrbit.orbit(0,-1,-1,-1), 3, new Menger222ExprRn19color(), new Menger222ExprRn19normal());
        JerboaRuleNode rn20 = new JerboaRuleNode("n20", 19, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn21 = new JerboaRuleNode("n21", 20, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn22 = new JerboaRuleNode("n22", 21, JerboaOrbit.orbit(0,-1,-1,-1), 3, new Menger222ExprRn22position(), new Menger222ExprRn22color(), new Menger222ExprRn22normal());
        JerboaRuleNode rn23 = new JerboaRuleNode("n23", 22, JerboaOrbit.orbit(0,-1,-1,-1), 3);
        JerboaRuleNode rn24 = new JerboaRuleNode("n24", 23, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn25 = new JerboaRuleNode("n25", 24, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn26 = new JerboaRuleNode("n26", 25, JerboaOrbit.orbit(-1,2,-1,-1), 3);
        JerboaRuleNode rn27 = new JerboaRuleNode("n27", 26, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn28 = new JerboaRuleNode("n28", 27, JerboaOrbit.orbit(-1,2,-1,-1), 3);
        JerboaRuleNode rn29 = new JerboaRuleNode("n29", 28, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn30 = new JerboaRuleNode("n30", 29, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn31 = new JerboaRuleNode("n31", 30, JerboaOrbit.orbit(0,1,-1,-1), 3, new Menger222ExprRn31color(), new Menger222ExprRn31normal());
        JerboaRuleNode rn32 = new JerboaRuleNode("n32", 31, JerboaOrbit.orbit(0,-1,-1,-1), 3, new Menger222ExprRn32color(), new Menger222ExprRn32normal());
        JerboaRuleNode rn33 = new JerboaRuleNode("n33", 32, JerboaOrbit.orbit(0,-1,-1,-1), 3);
        JerboaRuleNode rn34 = new JerboaRuleNode("n34", 33, JerboaOrbit.orbit(-1,2,-1,-1), 3);
        JerboaRuleNode rn35 = new JerboaRuleNode("n35", 34, JerboaOrbit.orbit(-1,2,-1,-1), 3);
        JerboaRuleNode rn36 = new JerboaRuleNode("n36", 35, JerboaOrbit.orbit(0,-1,2,3), 3, new Menger222ExprRn36color(), new Menger222ExprRn36normal());
        JerboaRuleNode rn37 = new JerboaRuleNode("n37", 36, JerboaOrbit.orbit(-1,-1,1,-1), 3, new Menger222ExprRn37color(), new Menger222ExprRn37normal());
        JerboaRuleNode rn38 = new JerboaRuleNode("n38", 37, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn39 = new JerboaRuleNode("n39", 38, JerboaOrbit.orbit(0,-1,-1,3), 3);
        JerboaRuleNode rn40 = new JerboaRuleNode("n40", 39, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn41 = new JerboaRuleNode("n41", 40, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn42 = new JerboaRuleNode("n42", 41, JerboaOrbit.orbit(-1,-1,1,-1), 3);
        JerboaRuleNode rn43 = new JerboaRuleNode("n43", 42, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn44 = new JerboaRuleNode("n44", 43, JerboaOrbit.orbit(0,1,-1,-1), 3, new Menger222ExprRn44color(), new Menger222ExprRn44normal());
        JerboaRuleNode rn45 = new JerboaRuleNode("n45", 44, JerboaOrbit.orbit(0,1,2,-1), 3, new Menger222ExprRn45color(), new Menger222ExprRn45normal());
        JerboaRuleNode rn46 = new JerboaRuleNode("n46", 45, JerboaOrbit.orbit(0,1,-1,-1), 3, new Menger222ExprRn46color(), new Menger222ExprRn46normal());
        JerboaRuleNode rn47 = new JerboaRuleNode("n47", 46, JerboaOrbit.orbit(-1,-1,1,-1), 3, new Menger222ExprRn47color(), new Menger222ExprRn47normal());
        JerboaRuleNode rn48 = new JerboaRuleNode("n48", 47, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn49 = new JerboaRuleNode("n49", 48, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 49, JerboaOrbit.orbit(-1,1,2,3), 3);
        JerboaRuleNode rn50 = new JerboaRuleNode("n50", 50, JerboaOrbit.orbit(-1,-1,1,-1), 3);
        JerboaRuleNode rn51 = new JerboaRuleNode("n51", 51, JerboaOrbit.orbit(0,-1,-1,-1), 3);
        JerboaRuleNode rn52 = new JerboaRuleNode("n52", 52, JerboaOrbit.orbit(0,-1,-1,3), 3, new Menger222ExprRn52color(), new Menger222ExprRn52normal());
        JerboaRuleNode rn53 = new JerboaRuleNode("n53", 53, JerboaOrbit.orbit(-1,-1,2,-1), 3);
        JerboaRuleNode rn54 = new JerboaRuleNode("n54", 54, JerboaOrbit.orbit(0,-1,-1,3), 3);
        JerboaRuleNode rn55 = new JerboaRuleNode("n55", 55, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn56 = new JerboaRuleNode("n56", 56, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn57 = new JerboaRuleNode("n57", 57, JerboaOrbit.orbit(-1,-1,2,3), 3);
        JerboaRuleNode rn58 = new JerboaRuleNode("n58", 58, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn59 = new JerboaRuleNode("n59", 59, JerboaOrbit.orbit(-1,-1,-1,3), 3);
        JerboaRuleNode rn60 = new JerboaRuleNode("n60", 60, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn61 = new JerboaRuleNode("n61", 61, JerboaOrbit.orbit(-1,1,-1,3), 3);
        JerboaRuleNode rn62 = new JerboaRuleNode("n62", 62, JerboaOrbit.orbit(-1,-1,-1,-1), 3, new Menger222ExprRn62color(), new Menger222ExprRn62normal());
        JerboaRuleNode rn63 = new JerboaRuleNode("n63", 63, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn64 = new JerboaRuleNode("n64", 64, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn65 = new JerboaRuleNode("n65", 65, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn66 = new JerboaRuleNode("n66", 66, JerboaOrbit.orbit(0,-1,-1,-1), 3);
        JerboaRuleNode rn67 = new JerboaRuleNode("n67", 67, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn68 = new JerboaRuleNode("n68", 68, JerboaOrbit.orbit(-1,-1,2,-1), 3);
        JerboaRuleNode rn69 = new JerboaRuleNode("n69", 69, JerboaOrbit.orbit(-1,-1,1,-1), 3);
        JerboaRuleNode rn70 = new JerboaRuleNode("n70", 70, JerboaOrbit.orbit(-1,2,-1,-1), 3);
        JerboaRuleNode rn71 = new JerboaRuleNode("n71", 71, JerboaOrbit.orbit(-1,-1,1,-1), 3);
        JerboaRuleNode rn72 = new JerboaRuleNode("n72", 72, JerboaOrbit.orbit(-1,2,1,-1), 3);
        JerboaRuleNode rn73 = new JerboaRuleNode("n73", 73, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn74 = new JerboaRuleNode("n74", 74, JerboaOrbit.orbit(0,-1,-1,-1), 3);
        JerboaRuleNode rn75 = new JerboaRuleNode("n75", 75, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn76 = new JerboaRuleNode("n76", 76, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        JerboaRuleNode rn77 = new JerboaRuleNode("n77", 77, JerboaOrbit.orbit(-1,-1,2,3), 3);
        JerboaRuleNode rn78 = new JerboaRuleNode("n78", 78, JerboaOrbit.orbit(0,1,-1,3), 3, new Menger222ExprRn78color(), new Menger222ExprRn78normal());
        JerboaRuleNode rn79 = new JerboaRuleNode("n79", 79, JerboaOrbit.orbit(-1,-1,2,3), 3);
        JerboaRuleNode rn80 = new JerboaRuleNode("n80", 80, JerboaOrbit.orbit(-1,-1,-1,-1), 3);
        right.add(rn1);
        right.add(rn2);
        right.add(rn3);
        right.add(rn4);
        right.add(rn5);
        right.add(rn6);
        right.add(rn7);
        right.add(rn8);
        right.add(rn9);
        right.add(rn10);
        right.add(rn11);
        right.add(rn12);
        right.add(rn13);
        right.add(rn14);
        right.add(rn15);
        right.add(rn16);
        right.add(rn17);
        right.add(rn18);
        right.add(rn19);
        right.add(rn20);
        right.add(rn21);
        right.add(rn22);
        right.add(rn23);
        right.add(rn24);
        right.add(rn25);
        right.add(rn26);
        right.add(rn27);
        right.add(rn28);
        right.add(rn29);
        right.add(rn30);
        right.add(rn31);
        right.add(rn32);
        right.add(rn33);
        right.add(rn34);
        right.add(rn35);
        right.add(rn36);
        right.add(rn37);
        right.add(rn38);
        right.add(rn39);
        right.add(rn40);
        right.add(rn41);
        right.add(rn42);
        right.add(rn43);
        right.add(rn44);
        right.add(rn45);
        right.add(rn46);
        right.add(rn47);
        right.add(rn48);
        right.add(rn49);
        right.add(rn0);
        right.add(rn50);
        right.add(rn51);
        right.add(rn52);
        right.add(rn53);
        right.add(rn54);
        right.add(rn55);
        right.add(rn56);
        right.add(rn57);
        right.add(rn58);
        right.add(rn59);
        right.add(rn60);
        right.add(rn61);
        right.add(rn62);
        right.add(rn63);
        right.add(rn64);
        right.add(rn65);
        right.add(rn66);
        right.add(rn67);
        right.add(rn68);
        right.add(rn69);
        right.add(rn70);
        right.add(rn71);
        right.add(rn72);
        right.add(rn73);
        right.add(rn74);
        right.add(rn75);
        right.add(rn76);
        right.add(rn77);
        right.add(rn78);
        right.add(rn79);
        right.add(rn80);
        rn2.setAlpha(1, rn79);
        rn2.setAlpha(2, rn71);
        rn3.setAlpha(0, rn80);
        rn3.setAlpha(1, rn1);
        rn3.setAlpha(2, rn50);
        rn4.setAlpha(0, rn2);
        rn4.setAlpha(2, rn73);
        rn5.setAlpha(3, rn5);
        rn6.setAlpha(1, rn4);
        rn6.setAlpha(2, rn75);
        rn7.setAlpha(0, rn5);
        rn7.setAlpha(3, rn7);
        rn8.setAlpha(1, rn5);
        rn8.setAlpha(2, rn66);
        rn8.setAlpha(3, rn8);
        rn9.setAlpha(0, rn6);
        rn9.setAlpha(2, rn76);
        rn10.setAlpha(1, rn9);
        rn11.setAlpha(0, rn10);
        rn11.setAlpha(1, rn77);
        rn12.setAlpha(1, rn7);
        rn12.setAlpha(3, rn12);
        rn13.setAlpha(2, rn59);
        rn14.setAlpha(0, rn13);
        rn14.setAlpha(1, rn70);
        rn14.setAlpha(2, rn61);
        rn15.setAlpha(1, rn76);
        rn15.setAlpha(3, rn15);
        rn16.setAlpha(2, rn7);
        rn16.setAlpha(3, rn16);
        rn17.setAlpha(0, rn15);
        rn17.setAlpha(1, rn68);
        rn17.setAlpha(3, rn17);
        rn18.setAlpha(2, rn54);
        rn19.setAlpha(2, rn12);
        rn19.setAlpha(3, rn1);
        rn20.setAlpha(1, rn18);
        rn20.setAlpha(2, rn65);
        rn21.setAlpha(0, rn20);
        rn21.setAlpha(1, rn66);
        rn21.setAlpha(2, rn67);
        rn22.setAlpha(3, rn22);
        rn23.setAlpha(2, rn52);
        rn23.setAlpha(3, rn74);
        rn24.setAlpha(1, rn23);
        rn24.setAlpha(3, rn80);
        rn25.setAlpha(0, rn24);
        rn25.setAlpha(1, rn19);
        rn25.setAlpha(3, rn3);
        rn26.setAlpha(1, rn22);
        rn26.setAlpha(3, rn26);
        rn27.setAlpha(0, rn16);
        rn27.setAlpha(1, rn67);
        rn27.setAlpha(2, rn5);
        rn27.setAlpha(3, rn27);
        rn28.setAlpha(0, rn26);
        rn28.setAlpha(1, rn51);
        rn28.setAlpha(3, rn28);
        rn29.setAlpha(1, rn64);
        rn29.setAlpha(2, rn24);
        rn29.setAlpha(3, rn29);
        rn30.setAlpha(0, rn29);
        rn30.setAlpha(1, rn16);
        rn30.setAlpha(2, rn25);
        rn30.setAlpha(3, rn30);
        rn32.setAlpha(2, rn31);
        rn32.setAlpha(3, rn66);
        rn33.setAlpha(2, rn78);
        rn33.setAlpha(3, rn18);
        rn34.setAlpha(1, rn33);
        rn34.setAlpha(3, rn20);
        rn35.setAlpha(0, rn34);
        rn35.setAlpha(1, rn32);
        rn35.setAlpha(3, rn21);
        rn37.setAlpha(2, rn11);
        rn37.setAlpha(3, rn13);
        rn38.setAlpha(0, rn37);
        rn38.setAlpha(2, rn10);
        rn38.setAlpha(3, rn14);
        rn39.setAlpha(2, rn74);
        rn40.setAlpha(1, rn38);
        rn40.setAlpha(2, rn15);
        rn40.setAlpha(3, rn70);
        rn41.setAlpha(1, rn39);
        rn42.setAlpha(0, rn40);
        rn42.setAlpha(2, rn17);
        rn42.setAlpha(3, rn72);
        rn43.setAlpha(0, rn41);
        rn43.setAlpha(1, rn36);
        rn44.setAlpha(2, rn51);
        rn44.setAlpha(3, rn31);
        rn46.setAlpha(2, rn22);
        rn46.setAlpha(3, rn45);
        rn47.setAlpha(2, rn43);
        rn47.setAlpha(3, rn71);
        rn48.setAlpha(0, rn47);
        rn48.setAlpha(2, rn41);
        rn48.setAlpha(3, rn73);
        rn49.setAlpha(0, rn50);
        rn49.setAlpha(1, rn48);
        rn49.setAlpha(2, rn80);
        rn49.setAlpha(3, rn62);
        rn51.setAlpha(3, rn51);
        rn53.setAlpha(3, rn53);
        rn55.setAlpha(1, rn54);
        rn56.setAlpha(0, rn55);
        rn56.setAlpha(1, rn52);
        rn57.setAlpha(0, rn0);
        rn58.setAlpha(1, rn53);
        rn58.setAlpha(3, rn58);
        rn59.setAlpha(1, rn57);
        rn60.setAlpha(0, rn58);
        rn60.setAlpha(3, rn60);
        rn61.setAlpha(0, rn59);
        rn62.setAlpha(2, rn60);
        rn63.setAlpha(2, rn55);
        rn63.setAlpha(3, rn63);
        rn64.setAlpha(0, rn63);
        rn64.setAlpha(2, rn56);
        rn64.setAlpha(3, rn64);
        rn65.setAlpha(1, rn63);
        rn65.setAlpha(3, rn65);
        rn67.setAlpha(0, rn65);
        rn67.setAlpha(3, rn67);
        rn68.setAlpha(0, rn53);
        rn68.setAlpha(3, rn68);
        rn69.setAlpha(0, rn62);
        rn69.setAlpha(2, rn58);
        rn69.setAlpha(3, rn50);
        rn72.setAlpha(0, rn70);
        rn73.setAlpha(0, rn71);
        rn73.setAlpha(1, rn62);
        rn75.setAlpha(1, rn60);
        rn75.setAlpha(3, rn75);
        rn76.setAlpha(0, rn75);
        rn76.setAlpha(3, rn76);
        rn79.setAlpha(0, rn77);
        rn80.setAlpha(1, rn74);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return -1;
        case 1: return -1;
        case 2: return -1;
        case 3: return -1;
        case 4: return -1;
        case 5: return -1;
        case 6: return -1;
        case 7: return -1;
        case 8: return -1;
        case 9: return -1;
        case 10: return -1;
        case 11: return -1;
        case 12: return -1;
        case 13: return -1;
        case 14: return -1;
        case 15: return -1;
        case 16: return -1;
        case 17: return -1;
        case 18: return -1;
        case 19: return -1;
        case 20: return -1;
        case 21: return -1;
        case 22: return -1;
        case 23: return -1;
        case 24: return -1;
        case 25: return -1;
        case 26: return -1;
        case 27: return -1;
        case 28: return -1;
        case 29: return -1;
        case 30: return -1;
        case 31: return -1;
        case 32: return -1;
        case 33: return -1;
        case 34: return -1;
        case 35: return -1;
        case 36: return -1;
        case 37: return -1;
        case 38: return -1;
        case 39: return -1;
        case 40: return -1;
        case 41: return -1;
        case 42: return -1;
        case 43: return -1;
        case 44: return -1;
        case 45: return -1;
        case 46: return -1;
        case 47: return -1;
        case 48: return -1;
        case 49: return 0;
        case 50: return -1;
        case 51: return -1;
        case 52: return -1;
        case 53: return -1;
        case 54: return -1;
        case 55: return -1;
        case 56: return -1;
        case 57: return -1;
        case 58: return -1;
        case 59: return -1;
        case 60: return -1;
        case 61: return -1;
        case 62: return -1;
        case 63: return -1;
        case 64: return -1;
        case 65: return -1;
        case 66: return -1;
        case 67: return -1;
        case 68: return -1;
        case 69: return -1;
        case 70: return -1;
        case 71: return -1;
        case 72: return -1;
        case 73: return -1;
        case 74: return -1;
        case 75: return -1;
        case 76: return -1;
        case 77: return -1;
        case 78: return -1;
        case 79: return -1;
        case 80: return -1;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return 0;
        case 2: return 0;
        case 3: return 0;
        case 4: return 0;
        case 5: return 0;
        case 6: return 0;
        case 7: return 0;
        case 8: return 0;
        case 9: return 0;
        case 10: return 0;
        case 11: return 0;
        case 12: return 0;
        case 13: return 0;
        case 14: return 0;
        case 15: return 0;
        case 16: return 0;
        case 17: return 0;
        case 18: return 0;
        case 19: return 0;
        case 20: return 0;
        case 21: return 0;
        case 22: return 0;
        case 23: return 0;
        case 24: return 0;
        case 25: return 0;
        case 26: return 0;
        case 27: return 0;
        case 28: return 0;
        case 29: return 0;
        case 30: return 0;
        case 31: return 0;
        case 32: return 0;
        case 33: return 0;
        case 34: return 0;
        case 35: return 0;
        case 36: return 0;
        case 37: return 0;
        case 38: return 0;
        case 39: return 0;
        case 40: return 0;
        case 41: return 0;
        case 42: return 0;
        case 43: return 0;
        case 44: return 0;
        case 45: return 0;
        case 46: return 0;
        case 47: return 0;
        case 48: return 0;
        case 49: return 0;
        case 50: return 0;
        case 51: return 0;
        case 52: return 0;
        case 53: return 0;
        case 54: return 0;
        case 55: return 0;
        case 56: return 0;
        case 57: return 0;
        case 58: return 0;
        case 59: return 0;
        case 60: return 0;
        case 61: return 0;
        case 62: return 0;
        case 63: return 0;
        case 64: return 0;
        case 65: return 0;
        case 66: return 0;
        case 67: return 0;
        case 68: return 0;
        case 69: return 0;
        case 70: return 0;
        case 71: return 0;
        case 72: return 0;
        case 73: return 0;
        case 74: return 0;
        case 75: return 0;
        case 76: return 0;
        case 77: return 0;
        case 78: return 0;
        case 79: return 0;
        case 80: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

    private class Menger222ExprRn1position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.20000000000000062);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(0.39999999999999947);
res.add(p1);
Point3 p3 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,2),0));
p3.scale(0.4);
res.add(p3);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn1color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn1normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn2position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.20000000000000018);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(0.7999999999999998);
res.add(p1);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn2color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn2normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn4position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.19999999999999984);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(0.40000000000000013);
res.add(p1);
Point3 p2 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,3),0));
p2.scale(0.4);
res.add(p2);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn5position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.2000000000000003);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(( - 2.220446049250313E-16));
res.add(p1);
Point3 p2 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,3),0));
p2.scale(0.4);
res.add(p2);
Point3 p3 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,2),0));
p3.scale(0.4);
res.add(p3);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn5color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn5normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn9position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.5999999999999998);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(2.220446049250313E-16);
res.add(p1);
Point3 p2 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,3),0));
p2.scale(0.4);
res.add(p2);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn11position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.5999999999999996);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(0.40000000000000036);
res.add(p1);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn13color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn13normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn15color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn15normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn16color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn16normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn17position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.6000000000000005);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(( - 4.440892098500626E-16));
res.add(p1);
Point3 p3 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,2),0));
p3.scale(0.4);
res.add(p3);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn18position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.19999999999999996);
res.add(p0);
Point3 p2 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,3),0));
p2.scale(0.8);
res.add(p2);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn18color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn18normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn19color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn19normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn22position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 res = new Point3(0.0,0.0,0.0);
Point3 p0 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2,3),0));
p0.scale(0.2000000000000003);
res.add(p0);
Point3 p1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,2,3),0));
p1.scale(( - 2.220446049250313E-16));
res.add(p1);
Point3 p3 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1,2),0));
p3.scale(0.8);
res.add(p3);
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Menger222ExprRn22color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn22normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn31color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn31normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn32color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn32normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn36color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn36normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn37color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn37normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn44color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn44normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn45color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn45normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn46color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn46normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn47color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn47normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn52color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn52normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn62color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn62normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class Menger222ExprRn78color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class Menger222ExprRn78normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return new Normal3();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class