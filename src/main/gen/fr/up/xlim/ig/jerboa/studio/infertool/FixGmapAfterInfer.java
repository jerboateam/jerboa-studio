package fr.up.xlim.ig.jerboa.studio.infertool;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;

/* Raw Imports : */
import fr.up.xlim.ig.jerboa.studio.util.FixPosition;
import fr.up.xlim.ig.jerboa.studio.util.RegenOrient;
import fr.up.xlim.ig.jerboa.studio.normal.NewellNormalAll;

/* End raw Imports */



/**
 * 
 */



public class FixGmapAfterInfer extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public FixGmapAfterInfer(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "FixGmapAfterInfer", "infertool");

        // -------- LEFT GRAPH

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        JerboaMark mark = gmap.creatFreeMarker();
		for(JerboaDart d : gmap){
		   if(d.isNotMarked(mark)) {
		      gmap.markOrbit(d,JerboaOrbit.orbit(1,2,3), mark);
		      Point3 p = Point3.randomPoint();
		      for(JerboaDart v : gmap.collect(d,JerboaOrbit.orbit(1,2,3),JerboaOrbit.orbit())){
		         JerboaInputHooksGeneric _v_hook1 = new JerboaInputHooksGeneric();
		         _v_hook1.addCol(v);
		         ((FixPosition)modeler.getRule("FixPosition")).setPosition(p);
		         ((FixPosition)modeler.getRule("FixPosition")).applyRule(gmap, _v_hook1);
		      }
		      
		   }
		}
		
		JerboaInputHooksGeneric _v_hook2 = new JerboaInputHooksGeneric();
		((RegenOrient)modeler.getRule("RegenOrient")).applyRule(gmap, _v_hook2);
		JerboaInputHooksGeneric _v_hook3 = new JerboaInputHooksGeneric();
		((NewellNormalAll)modeler.getRule("NewellNormalAll")).applyRule(gmap, _v_hook3);
		return null;
		// END SCRIPT GENERATION

	}
} // end rule Class