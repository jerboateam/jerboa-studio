package fr.up.xlim.ig.jerboa.studio.creat;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;
 // BEGIN HEADER IMPORT


// import up.xlim.ig.jerboa.demo.extrusion.ExtrudeFace;

 // END HEADER IMPORT

/* Raw Imports : */
import fr.up.xlim.ig.jerboa.studio.creat.CreatSquare;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeFace;

/* End raw Imports */



/**
 * 
 */



public class CreatCube extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 

 // BEGIN COPY PASTE OF HEADER



 // END COPY PASTE OF HEADER


    public CreatCube(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "CreatCube", "creat");

        // -------- LEFT GRAPH

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
 // BEGIN COPY PASTE OF HEADER

 // END COPY PASTE OF HEADER
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        JerboaInputHooksGeneric _v_hook0 = new JerboaInputHooksGeneric();
		JerboaRuleResult res = ((CreatSquare)modeler.getRule("CreatSquare")).applyRule(gmap, _v_hook0);
		JerboaInputHooksGeneric _v_hook1 = new JerboaInputHooksGeneric();
		_v_hook1.addCol(res.get(0));
		((ExtrudeFace)modeler.getRule("ExtrudeFace")).applyRule(gmap, _v_hook1);
		return res;
		// END SCRIPT GENERATION

	}
} // end rule Class