package fr.up.xlim.ig.jerboa.studio.color;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;

/* Raw Imports : */
import fr.up.xlim.ig.jerboa.studio.color.SetColorVolume;

/* End raw Imports */



/**
 * 
 */



public class ColorGmapPerConnex extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public ColorGmapPerConnex(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "ColorGmapPerConnex", "color");

        // -------- LEFT GRAPH

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        JerboaMark mark = gmap.creatFreeMarker();
		java.util.List<JerboaDart> connex = new ArrayList<JerboaDart>();
		for(JerboaDart d : gmap){
		   if(d.isNotMarked(mark)) {
		      connex.add(d);
		      gmap.markOrbit(d,JerboaOrbit.orbit(0,1,2,3), mark);
		   }
		}
		
		for(JerboaDart d : connex){
		   ((SetColorVolume)modeler.getRule("SetColorVolume")).setSelect(Color3.randomColor());
		   JerboaInputHooksGeneric _v_hook0 = new JerboaInputHooksGeneric();
		   _v_hook0.addCol(d);
		   ((SetColorVolume)modeler.getRule("SetColorVolume")).applyRule(gmap, _v_hook0);
		}
		
		return null;
		// END SCRIPT GENERATION

	}
} // end rule Class