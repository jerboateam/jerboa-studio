package fr.up.xlim.ig.jerboa.studio.color;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;

/* Raw Imports : */
import fr.up.xlim.ig.jerboa.studio.color.SetColorFace;

/* End raw Imports */



/**
 * 
 */



public class AskColorSelectedFaces extends JerboaRuleScript {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public AskColorSelectedFaces(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "AskColorSelectedFaces", "color");

        // -------- LEFT GRAPH

        // -------- RIGHT GRAPH
;
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        return -1;
    }

    public int attachedNode(int i) {
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        return applyRule(gmap, ____jme_hooks);
	}

@Override
    public JerboaRuleResult apply(final JerboaGMap gmap, final JerboaInputHooks hooks) throws JerboaException {
// BEGIN SCRIPT GENERATION
        Color3 c = Color3.askColor(hooks.dart(0,0).<fr.up.xlim.ig.jerboa.ebds.Color3>ebd(1));
		for(JerboaDart voldart : hooks){
		   ((SetColorFace)modeler.getRule("SetColorFace")).setSelect(c);
		   JerboaInputHooksGeneric _v_hook0 = new JerboaInputHooksGeneric();
		   _v_hook0.addCol(voldart);
		   ((SetColorFace)modeler.getRule("SetColorFace")).applyRule(gmap, _v_hook0);
		}
		
		return null;
		// END SCRIPT GENERATION

	}
} // end rule Class