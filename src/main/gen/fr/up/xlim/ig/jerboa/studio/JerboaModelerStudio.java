package fr.up.xlim.ig.jerboa.studio;

import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.sew.SewA0;
import fr.up.xlim.ig.jerboa.studio.sew.UnSewA0;
import fr.up.xlim.ig.jerboa.studio.sew.SewA1;
import fr.up.xlim.ig.jerboa.studio.sew.UnSewA1;
import fr.up.xlim.ig.jerboa.studio.sew.SewA2;
import fr.up.xlim.ig.jerboa.studio.sew.UnSewA2;
import fr.up.xlim.ig.jerboa.studio.sew.SewA3;
import fr.up.xlim.ig.jerboa.studio.sew.UnSewA3;
import fr.up.xlim.ig.jerboa.studio.util.FlipOrient;
import fr.up.xlim.ig.jerboa.studio.color.ColorChangeUI;
import fr.up.xlim.ig.jerboa.studio.color.ColorRandomConnex;
import fr.up.xlim.ig.jerboa.studio.color.ColorRandomFace;
import fr.up.xlim.ig.jerboa.studio.suppression.DeleteConnex;
import fr.up.xlim.ig.jerboa.studio.creat.CreatSquare;
import fr.up.xlim.ig.jerboa.studio.creat.CreatTriangle;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeA0;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeA1;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeA2;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeA3;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeFace;
import fr.up.xlim.ig.jerboa.studio.duplication.Duplicate;
import fr.up.xlim.ig.jerboa.studio.suppression.IsoAndDeleteVolume;
import fr.up.xlim.ig.jerboa.studio.suppression.IsolateVol;
import fr.up.xlim.ig.jerboa.studio.creat.CreatCube;
import fr.up.xlim.ig.jerboa.studio.color.ColorRandomVol;
import fr.up.xlim.ig.jerboa.studio.color.ColorGmapPerVol;
import fr.up.xlim.ig.jerboa.studio.color.ColorGmapPerVolForConnex;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeCone;
import fr.up.xlim.ig.jerboa.studio.util.SetOrient;
import fr.up.xlim.ig.jerboa.studio.util.RegenOrient;
import fr.up.xlim.ig.jerboa.studio.suppression.IsoAndDeleteFace;
import fr.up.xlim.ig.jerboa.studio.sew.UnSewA2All;
import fr.up.xlim.ig.jerboa.studio.color.ChangeColorFace;
import fr.up.xlim.ig.jerboa.studio.color.RandomColorFace;
import fr.up.xlim.ig.jerboa.studio.color.SetColorVolume;
import fr.up.xlim.ig.jerboa.studio.color.SetColorFace;
import fr.up.xlim.ig.jerboa.studio.color.SetColorConnex;
import fr.up.xlim.ig.jerboa.studio.color.ChangeColorConnex;
import fr.up.xlim.ig.jerboa.studio.normal.NewellNormalFace;
import fr.up.xlim.ig.jerboa.studio.normal.NewellNormalConnex;
import fr.up.xlim.ig.jerboa.studio.normal.NewellNormalAll;
import fr.up.xlim.ig.jerboa.studio.color.AskColorVolume;
import fr.up.xlim.ig.jerboa.studio.color.AskColorFace;
import fr.up.xlim.ig.jerboa.studio.normal.FlipNormalFace;
import fr.up.xlim.ig.jerboa.studio.normal.FlipNormalConnex;
import fr.up.xlim.ig.jerboa.studio.color.ColorGmapPerConnex;
import fr.up.xlim.ig.jerboa.studio.suppression.DeleteFace;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeFaceArg;
import fr.up.xlim.ig.jerboa.studio.extrusion.ExtrudeFaceAxisUI;
import fr.up.xlim.ig.jerboa.studio.infertool.FixGmapAfterInfer;
import fr.up.xlim.ig.jerboa.studio.util.FixPosition;
import fr.up.xlim.ig.jerboa.studio.color.AskColorSelectedVolumes;
import fr.up.xlim.ig.jerboa.studio.color.AskColorSelectedFaces;
import fr.up.xlim.ig.jerboa.studio.creat.CreatDart;
import fr.up.xlim.ig.jerboa.studio.creat.CreatTriangleEq;
import fr.up.xlim.ig.jerboa.studio.creat.CreatHexagon;
import fr.up.xlim.ig.jerboa.studio.subdivision.Menger;
import fr.up.xlim.ig.jerboa.studio.subdivision.Menger222;
import fr.up.xlim.ig.jerboa.studio.subdivision.Chaikin;
import fr.up.xlim.ig.jerboa.studio.subdivision.Butterfly;
import fr.up.xlim.ig.jerboa.studio.subdivision.DooSabin;
import fr.up.xlim.ig.jerboa.studio.subdivision.Loop;
import fr.up.xlim.ig.jerboa.studio.subdivision.Sqrt3;
import fr.up.xlim.ig.jerboa.studio.subdivision.CatmullClark;
import fr.up.xlim.ig.jerboa.studio.subdivision.Quad;
import fr.up.xlim.ig.jerboa.studio.triangulation.TriangulationOneFace;
import fr.up.xlim.ig.jerboa.studio.triangulation.TriangulationAllFaces;



import up.jerboa.core.JerboaModelerState;

/**
 * <h1>JerboaModelerStudio</h1>
 * <p>Modeleur 3D pour JerboaStudio</p>
 */

public class JerboaModelerStudio extends JerboaModelerGeneric {

    // BEGIN LIST OF EMBEDDINGS
    protected JerboaEmbeddingInfo position;
    protected JerboaEmbeddingInfo color;
    protected JerboaEmbeddingInfo orient;
    protected JerboaEmbeddingInfo normal;
    // END LIST OF EMBEDDINGS

    // BEGIN USER DECLARATION
// BEGIN LANGUAGE INPUT CODE

private JerboaModelerState orientOK = new JerboaModelerState(this,"orientOK") {
      public boolean checkInvariant() {
          return true;
      }

};

// END LANGUAGE INPUT CODE
    // END USER DECLARATION

    public JerboaModelerStudio() throws JerboaException {

        super(3);

    // BEGIN USER HEAD CONSTRUCTOR TRANSLATION

    // END USER HEAD CONSTRUCTOR TRANSLATION
        position = new JerboaEmbeddingInfo("position", JerboaOrbit.orbit(1,2,3), fr.up.xlim.ig.jerboa.ebds.Point3.class);
        color = new JerboaEmbeddingInfo("color", JerboaOrbit.orbit(0,1), fr.up.xlim.ig.jerboa.ebds.Color3.class);
        orient = new JerboaEmbeddingInfo("orient", JerboaOrbit.orbit(), java.lang.Boolean.class);
        normal = new JerboaEmbeddingInfo("normal", JerboaOrbit.orbit(0,1), fr.up.xlim.ig.jerboa.ebds.Normal3.class);

        this.registerEbdsAndResetGMAP(position,color,orient,normal);

        this.registerRule(new SewA0(this));
        this.registerRule(new UnSewA0(this));
        this.registerRule(new SewA1(this));
        this.registerRule(new UnSewA1(this));
        this.registerRule(new SewA2(this));
        this.registerRule(new UnSewA2(this));
        this.registerRule(new SewA3(this));
        this.registerRule(new UnSewA3(this));
        this.registerRule(new FlipOrient(this));
        this.registerRule(new ColorChangeUI(this));
        this.registerRule(new ColorRandomConnex(this));
        this.registerRule(new ColorRandomFace(this));
        this.registerRule(new DeleteConnex(this));
        this.registerRule(new CreatSquare(this));
        this.registerRule(new CreatTriangle(this));
        this.registerRule(new ExtrudeA0(this));
        this.registerRule(new ExtrudeA1(this));
        this.registerRule(new ExtrudeA2(this));
        this.registerRule(new ExtrudeA3(this));
        this.registerRule(new ExtrudeFace(this));
        this.registerRule(new Duplicate(this));
        this.registerRule(new IsoAndDeleteVolume(this));
        this.registerRule(new IsolateVol(this));
        this.registerRule(new CreatCube(this));
        this.registerRule(new ColorRandomVol(this));
        this.registerRule(new ColorGmapPerVol(this));
        this.registerRule(new ColorGmapPerVolForConnex(this));
        this.registerRule(new ExtrudeCone(this));
        this.registerRule(new SetOrient(this));
        this.registerRule(new RegenOrient(this));
        this.registerRule(new IsoAndDeleteFace(this));
        this.registerRule(new UnSewA2All(this));
        this.registerRule(new ChangeColorFace(this));
        this.registerRule(new RandomColorFace(this));
        this.registerRule(new SetColorVolume(this));
        this.registerRule(new SetColorFace(this));
        this.registerRule(new SetColorConnex(this));
        this.registerRule(new ChangeColorConnex(this));
        this.registerRule(new NewellNormalFace(this));
        this.registerRule(new NewellNormalConnex(this));
        this.registerRule(new NewellNormalAll(this));
        this.registerRule(new AskColorVolume(this));
        this.registerRule(new AskColorFace(this));
        this.registerRule(new FlipNormalFace(this));
        this.registerRule(new FlipNormalConnex(this));
        this.registerRule(new ColorGmapPerConnex(this));
        this.registerRule(new DeleteFace(this));
        this.registerRule(new ExtrudeFaceArg(this));
        this.registerRule(new ExtrudeFaceAxisUI(this));
        this.registerRule(new FixGmapAfterInfer(this));
        this.registerRule(new FixPosition(this));
        this.registerRule(new AskColorSelectedVolumes(this));
        this.registerRule(new AskColorSelectedFaces(this));
        this.registerRule(new CreatDart(this));
        this.registerRule(new CreatTriangleEq(this));
        this.registerRule(new CreatHexagon(this));
        this.registerRule(new Menger(this));
        this.registerRule(new Menger222(this));
        this.registerRule(new Chaikin(this));
        this.registerRule(new Butterfly(this));
        this.registerRule(new DooSabin(this));
        this.registerRule(new Loop(this));
        this.registerRule(new Sqrt3(this));
        this.registerRule(new CatmullClark(this));
        this.registerRule(new Quad(this));
        this.registerRule(new TriangulationOneFace(this));
        this.registerRule(new TriangulationAllFaces(this));
    }

    public final JerboaEmbeddingInfo getPosition() {
        return position;
    }

    public final JerboaEmbeddingInfo getColor() {
        return color;
    }

    public final JerboaEmbeddingInfo getOrient() {
        return orient;
    }

    public final JerboaEmbeddingInfo getNormal() {
        return normal;
    }

}
