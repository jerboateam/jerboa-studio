package fr.up.xlim.ig.jerboa.studio.subdivision;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;



/**
 * 
 */



public class Sqrt3 extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public Sqrt3(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "Sqrt3", "subdivision");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2), 3);
        left.add(ln0);
        hooks.add(ln0);
        ln0.setAlpha(3, ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(-1,2,1), 3, new Sqrt3ExprRn0position());
        JerboaRuleNode rn1 = new JerboaRuleNode("n1", 1, JerboaOrbit.orbit(-1,2,-1), 3, new Sqrt3ExprRn1orient());
        JerboaRuleNode rn2 = new JerboaRuleNode("n2", 2, JerboaOrbit.orbit(2,-1,0), 3, new Sqrt3ExprRn2position(), new Sqrt3ExprRn2orient(), new Sqrt3ExprRn2color());
        right.add(rn0);
        right.add(rn1);
        right.add(rn2);
        rn0.setAlpha(3, rn0);
        rn1.setAlpha(0, rn0);
        rn1.setAlpha(3, rn1);
        rn2.setAlpha(1, rn1);
        rn2.setAlpha(3, rn2);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return -1;
        case 2: return -1;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return 0;
        case 2: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

    private class Sqrt3ExprRn0position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
java.util.List<fr.up.xlim.ig.jerboa.ebds.Point3> sommetsAdjacents = new ArrayList<fr.up.xlim.ig.jerboa.ebds.Point3>();
for(JerboaDart d : gmap.collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2),JerboaOrbit.orbit(2))){
   sommetsAdjacents.add(d.alpha(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0));
}

return Point3.sqrt3Smooth(curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0),sommetsAdjacents);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Sqrt3ExprRn1orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(2));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class Sqrt3ExprRn2position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1),0));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class Sqrt3ExprRn2orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<java.lang.Boolean>ebd(2);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class Sqrt3ExprRn2color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Color3>ebd(1);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class