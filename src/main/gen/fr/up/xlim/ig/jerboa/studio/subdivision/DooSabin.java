package fr.up.xlim.ig.jerboa.studio.subdivision;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;



/**
 * 
 */



public class DooSabin extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public DooSabin(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "DooSabin", "subdivision");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2), 3);
        left.add(ln0);
        hooks.add(ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,-1), 3, new DooSabinExprRn0position());
        JerboaRuleNode rn1 = new JerboaRuleNode("n1", 1, JerboaOrbit.orbit(0,-1,-1), 3, new DooSabinExprRn1color(), new DooSabinExprRn1orient());
        JerboaRuleNode rn2 = new JerboaRuleNode("n2", 2, JerboaOrbit.orbit(-1,-1,0), 3, new DooSabinExprRn2orient(), new DooSabinExprRn2normal());
        JerboaRuleNode rn3 = new JerboaRuleNode("n3", 3, JerboaOrbit.orbit(-1,1,0), 3, new DooSabinExprRn3color(), new DooSabinExprRn3orient(), new DooSabinExprRn3normal());
        right.add(rn0);
        right.add(rn1);
        right.add(rn2);
        right.add(rn3);
        rn0.setAlpha(2, rn1);
        rn1.setAlpha(1, rn2);
        rn2.setAlpha(2, rn3);
        rn1.setAlpha(3, rn1);
        rn2.setAlpha(3, rn2);
        rn3.setAlpha(3, rn3);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return -1;
        case 2: return -1;
        case 3: return -1;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return 0;
        case 2: return 0;
        case 3: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

    private class DooSabinExprRn0position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
Point3 centerFace = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0,1),0));
Point3 midEdge1 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0),JerboaOrbit.orbit(0),0));
Point3 midEdge2 = Point3.middle(gmap.<fr.up.xlim.ig.jerboa.ebds.Point3>collect(curleftPattern.getNode(0).alpha(1),JerboaOrbit.orbit(0),0));
Point3 res = Point3.middle(centerFace,midEdge1,midEdge2,curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0));
return res;
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class DooSabinExprRn1color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class DooSabinExprRn1orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(2));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class DooSabinExprRn2orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<java.lang.Boolean>ebd(2);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class DooSabinExprRn2normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Normal3>ebd(3);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    private class DooSabinExprRn3color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Color3.randomColor();
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class DooSabinExprRn3orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(2));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class DooSabinExprRn3normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Normal3>ebd(3);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class