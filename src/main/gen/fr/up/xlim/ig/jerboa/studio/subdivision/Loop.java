package fr.up.xlim.ig.jerboa.studio.subdivision;


import java.util.List;
import java.util.ArrayList;
import up.jerboa.core.rule.*;
import up.jerboa.core.util.*;
import up.jerboa.core.*;
import up.jerboa.exception.JerboaException;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import java.lang.Boolean;
import fr.up.xlim.ig.jerboa.ebds.Normal3;



/**
 * 
 */



public class Loop extends JerboaRuleGenerated {

    private transient JerboaRowPattern curleftPattern;


	// BEGIN PARAMETERS Transformed 


	// END PARAMETERS 



    public Loop(JerboaModelerStudio modeler) throws JerboaException {

        super(modeler, "Loop", "subdivision");

        // -------- LEFT GRAPH
        JerboaRuleNode ln0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(0,1,2), 3);
        left.add(ln0);
        hooks.add(ln0);
        ln0.setAlpha(3, ln0);

        // -------- RIGHT GRAPH
        JerboaRuleNode rn0 = new JerboaRuleNode("n0", 0, JerboaOrbit.orbit(-1,1,2), 3, new LoopExprRn0position());
        JerboaRuleNode rn1 = new JerboaRuleNode("n1", 1, JerboaOrbit.orbit(-1,-1,2), 3, new LoopExprRn1position(), new LoopExprRn1orient());
        JerboaRuleNode rn2 = new JerboaRuleNode("n2", 2, JerboaOrbit.orbit(-1,0,-1), 3, new LoopExprRn2orient());
        JerboaRuleNode rn3 = new JerboaRuleNode("n3", 3, JerboaOrbit.orbit(1,0,-1), 3, new LoopExprRn3color(), new LoopExprRn3orient(), new LoopExprRn3normal());
        right.add(rn0);
        right.add(rn1);
        right.add(rn2);
        right.add(rn3);
        rn0.setAlpha(0, rn1);
        rn1.setAlpha(1, rn2);
        rn2.setAlpha(2, rn3);
        rn0.setAlpha(3, rn0);
        rn1.setAlpha(3, rn1);
        rn2.setAlpha(3, rn2);
        rn3.setAlpha(3, rn3);
;
        // ------- SPECIFIED FEATURE
        computeEfficientTopoStructure();
        computeSpreadOperation();
        // ------- USER DECLARATION 
    }

    public int reverseAssoc(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return -1;
        case 2: return -1;
        case 3: return -1;
        }
        return -1;
    }

    public int attachedNode(int i) {
        switch(i) {
        case 0: return 0;
        case 1: return 0;
        case 2: return 0;
        case 3: return 0;
        }
        return -1;
    }

    public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaDart n0) throws JerboaException {
        JerboaInputHooksGeneric ____jme_hooks = new JerboaInputHooksGeneric();
        ____jme_hooks.addCol(n0);
        return applyRule(gmap, ____jme_hooks);
	}

    private class LoopExprRn0position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
int n = 0;
java.util.List<fr.up.xlim.ig.jerboa.ebds.Point3> sommetsAdjacents = new ArrayList<fr.up.xlim.ig.jerboa.ebds.Point3>();
for(JerboaDart d : gmap.collect(curleftPattern.getNode(0),JerboaOrbit.orbit(1,2),JerboaOrbit.orbit(2))){
   sommetsAdjacents.add(d.alpha(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0));
   n = (n + 1);
}

Point3 bary = Point3.middle(sommetsAdjacents);
if((n == 2)) {
   return Point3.barycenter(curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0),(3.0 / 4.0),bary,(1.0 / 4.0));
}
else {
   if((n == 3)) {
      return Point3.barycenter(curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0),(7.0 / 16.0),bary,(9.0 / 16.0));
   }
   else {
      return Point3.barycenter(curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0),(5.0 / 8.0),bary,(3.0 / 8.0));
   }
}
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class LoopExprRn1position implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return Point3.barycenter(curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0),(3.0 / 8.0),curleftPattern.getNode(0).alpha(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0),(3.0 / 8.0),curleftPattern.getNode(0).alpha(1).alpha(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0),(1.0 / 8.0),curleftPattern.getNode(0).alpha(2).alpha(1).alpha(0).<fr.up.xlim.ig.jerboa.ebds.Point3>ebd(0),(1.0 / 8.0));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "position";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getPosition().getID();
        }
    }

    private class LoopExprRn1orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(2));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class LoopExprRn2orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<java.lang.Boolean>ebd(2);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class LoopExprRn3color implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Color3>ebd(1);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "color";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getColor().getID();
        }
    }

    private class LoopExprRn3orient implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return !(curleftPattern.getNode(0).<java.lang.Boolean>ebd(2));
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "orient";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getOrient().getID();
        }
    }

    private class LoopExprRn3normal implements JerboaRuleExpression {

        @Override
        public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,JerboaRowPattern leftPattern, JerboaRuleNode rulenode) throws JerboaException {
            curleftPattern = leftPattern;
// ======== BEGIN CODE TRANSLATION FOR EXPRESSION COMPUTATION
            // ======== SEPARATION CODE TRANSLATION FOR EXPRESSION COMPUTATION
return curleftPattern.getNode(0).<fr.up.xlim.ig.jerboa.ebds.Normal3>ebd(3);
// ======== END CODE TRANSLATION FOR EXPRESSION COMPUTATION
        }

        @Override
        public String getName() {
            return "normal";
        }

        @Override
        public int getEmbedding() {
            return ((JerboaModelerStudio)modeler).getNormal().getID();
        }
    }

    // Facility for accessing to the dart
    private JerboaDart n0() {
        return curleftPattern.getNode(0);
    }

} // end rule Class