package fr.up.xlim.ig.jerboa.demo.bridge.net;

import java.util.ArrayList;
import java.util.List;

import fr.up.xlim.ig.jerboa.ebds.Color3;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.sic.ig.jerboa.viewer.GMapViewer;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerPoint;
import up.jerboa.core.JerboaDart;
import up.jerboa.exception.JerboaRuntimeException;
import up.xlim.ig.jerboa.converter.JMVConverter;
import up.xlim.ig.jerboa.transmitter.object.jerboa.JMVArgGeo;
import up.xlim.ig.jerboa.transmitter.utils.Color;
import up.xlim.ig.jerboa.transmitter.utils.Vec3;

/**
 * Convert embedding to usable classes.
 * 
 * @author Hakim and Romain
 *
 */
public class JerboaStudioConverter extends JMVConverter<JerboaModelerStudio> {

	private GMapViewer viewer;

	public JerboaStudioConverter(JerboaModelerStudio modeler, GMapViewer viewer) {
		super(modeler);
		this.viewer = viewer;
	}

	@Override
	protected List<JMVArgGeo> convertEmbeddings(JerboaDart dart) {
		return new ArrayList<>();
	}

	@Override
	protected Vec3 convertNormal(JerboaDart dart) {
		GMapViewerPoint pts = viewer.eclate(dart);
		return new Vec3(pts.x(), pts.y(), pts.z());
	}

	@Override
	protected Color convertColor(JerboaDart dart) {
		// Color3 color = dart.ebd("color");
		Color3 color = dart.ebd(1);
		return new Color(color.getR(), color.getG(), color.getB(), color.getA());
	}

	@Override
	protected Vec3 convertPoint(JerboaDart dart) {
		// Point3 pos = dart.ebd("position");
		Point3 pos = dart.ebd(0);
		return new Vec3(pos.getX(),pos.getY(),pos.getZ());
	}

	@Override
	protected Boolean convertOrient(JerboaDart dart) {
		// return dart.ebd("orient");
		return dart.ebd(2);
	}

	@Override
	public void loadFile(String filepath) {
		throw new JerboaRuntimeException("not yet implemented");
	}

}
