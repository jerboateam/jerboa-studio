package fr.up.xlim.ig.jerboa.ebds;

/**
 * Interface for embeddings where the underlying space is a vector space.
 * Such embeddings can be infered.
 * 
 * @author Romain
 *
 */
public interface VectorialEbd {
	public static final double EPSILON = 1e-3;
	
	/**
	 * Get the value of the i-th coordinate.
	 * @param i: coordinate
	 * @return value of the i-th coordinate
	 */
	public Number getFromDim(int i);

	/**
	 * Multiply the vector by coef.
	 * @param coef: scaling factor
	 */
	public void scaleVect(Number coef);

	/**
	 * Add another vector.
	 * @param ebdVect: vector to be added
	 */
	public void addVect(VectorialEbd ebdVect);
	
	
	/**
	 * Ensure that the vector has valid coordinates.
	 */
	public void clampVect();
	
	/**
	 * An equality test is required.
	 */
	public boolean equals(Object o);
}
