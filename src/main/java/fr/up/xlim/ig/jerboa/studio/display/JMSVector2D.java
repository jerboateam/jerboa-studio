package fr.up.xlim.ig.jerboa.studio.display;

/**
 * Represent 2D vectors
 * 
 * @author romain
 *
 */
public class JMSVector2D {

	private double dx;
	private double dy;
	private double norm;
	private boolean normUpToDate;
	
	public JMSVector2D(double dx, double dy) {
		this.dx = dx;
		this.dy = dy;
		this.computeNorm();
		this.normUpToDate = true;
	}
	
	public JMSVector2D(double sourceX, double sourceY, double targetX, double targetY) {
		this(targetX-sourceX, targetY-sourceY);
	}

	public double dx() {
		return dx;
	}

	public void setDx(double dx) {
		this.dx = dx;
		this.normUpToDate = false;
	}

	public double dy() {
		return dy;
	}

	public void setDy(double dy) {
		this.dy = dy;
		this.normUpToDate = false;
	}
		
	/**
	 * Norm of the vector (thresholded to 0.005 for rounding errors)
	 * 
	 * @return vector norm as a double
	 */
	public double norm() {
		if (!normUpToDate)
			computeNorm();
		return norm;
	}
	
	private void computeNorm() {
		this.norm = Math.max(Math.sqrt(dx * dx + dy * dy), 0.005);
	}
	
	/**
	 * Normalised x component
	 * @return
	 */
	public double normalisedDx() {
		return dx/norm();
	}
	
	/**
	 * Normalised y component	
	 * @return
	 */
	public double normalisedDy() {
		return dy/norm();
	}
	
	/**
	 * Update vector by changing source and target position
	 * 
	 * @param sourceX
	 * @param sourceY
	 * @param targetX
	 * @param targetY
	 */
	public void updateSourceTarget(double sourceX, double sourceY, double targetX, double targetY) {
		this.setDx(targetX-sourceX);
		this.setDy(targetY-sourceY);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JMSVector2D [dx=");
		builder.append(dx);
		builder.append(", dy=");
		builder.append(dy);
		builder.append("]");
		return builder.toString();
	}
	
}
