/**
 * 
 */
package fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils;

/**
 * Exception used when the obtained folding is not connected.
 * 
 * @author romain
 *
 */
public class ConnectivityException extends Exception {

	private static final long serialVersionUID = -7586034947760516127L;

	public ConnectivityException() {
	}

	public ConnectivityException(String message) {
		super(message);
	}

	public ConnectivityException(Throwable cause) {
		super(cause);
	}

	public ConnectivityException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectivityException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
