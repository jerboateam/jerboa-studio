package fr.up.xlim.ig.jerboa.studio.infer.geometry.inject;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.up.xlim.ig.jerboa.ebds.factories.VectEbdAbstractFactory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments.VectEbdEnrichmentNode;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.system.LinearItem;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaRuleOperation;
import up.jerboa.core.rule.JerboaRowPattern;
import up.jerboa.core.rule.JerboaRuleExpression;
import up.jerboa.core.rule.JerboaRuleNode;
import up.jerboa.exception.JerboaException;

/**
 * Position embedding used to inject the code in the viewer.
 * 
 * @author Romain and Hakim
 *
 */
public abstract class EnrichedVectEbd implements JerboaRuleExpression {

	private int nid;
	private VectEbdEnrichmentNode eNode;
	private VectEbdAbstractFactory ebdFactory;

	public EnrichedVectEbd(int nodeID, VectEbdEnrichmentNode eNode, VectEbdAbstractFactory ebdFactory) {
		this.nid = nodeID;
		this.eNode = eNode;
		this.ebdFactory = ebdFactory;
	}
		
	public VectEbdAbstractFactory getEbdFactory() {
		return ebdFactory;
	}

	@Override
	public Object compute(JerboaGMap gmap, JerboaRuleOperation rule,
			JerboaRowPattern leftfilter, JerboaRuleNode rulenode)
			throws JerboaException {
		return eNode.compute(this, leftfilter);
	}

	@Override
	public String getName() {
		StringBuilder sb = new StringBuilder("Infer: [NodeID: ");
		sb.append(nid).append(", Computation: ");
		
		if (eNode.systemIsUnsolvable())
			sb.append("Default");
		else {
			List<LinearItem> system = eNode.getAbstractSystem();
			sb.append(IntStream.range(0, system.size())
					.mapToObj(i -> " " + system.get(i).getCoef() + " * "
							+ system.get(i).getPoi().toString())
					.collect(Collectors.joining(" +")));
			sb.append(" + ").append(eNode.getTranslation());
		}
		
		sb.append("]");
		return sb.toString();
	}

	@Override
	public abstract int getEmbedding();
}
