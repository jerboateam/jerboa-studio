package fr.up.xlim.ig.jerboa.bridge;


public enum FILEFORMAT {
	AUTO,
	JERBOA,
	JERBOAZIP,
	JERBOACOMPLETE,
	MOKA,
	OBJ
}
