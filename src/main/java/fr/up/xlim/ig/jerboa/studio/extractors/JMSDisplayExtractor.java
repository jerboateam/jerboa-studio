package fr.up.xlim.ig.jerboa.studio.extractors;

import fr.up.xlim.ig.jerboa.studio.display.JMSDisplayable;
import fr.up.xlim.ig.jerboa.studio.display.algorithms.JMSAttractionRepulsionDisplay;
import fr.up.xlim.ig.jerboa.studio.display.algorithms.JMSFruchtermanReingold;
import fr.up.xlim.ig.jerboa.studio.display.algorithms.JMSRandomDisplay;
import fr.up.xlim.ig.jerboa.studio.display.graph.JMSDisplayDart;
import fr.up.xlim.ig.jerboa.studio.display.graph.JMSDisplayGraph;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEArc;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEGraph;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERuleAtomic;

/**
 * Stores and uses the different graph display algorithm.
 * 
 * @author Romain
 *
 */
public class JMSDisplayExtractor {

	private JMSDisplayGraph dLeft;
	private JMSDisplayGraph dRight;

	public JMSDisplayExtractor(JMERuleAtomic rule) {
		this.dLeft = extractDisplayGraph(rule.getLeft());
		this.dRight = extractDisplayGraph(rule.getRight());
	}

	private JMSDisplayGraph extractDisplayGraph(JMEGraph graph) {
		JMSDisplayGraph dGraph = new JMSDisplayGraph(graph);
		for (JMENode node : graph.getNodes()) {
			new JMSDisplayDart(dGraph, node);
		}
		for (JMEArc arc : graph.getArcs()) {
			dGraph.addArc(dGraph.getDartFromJMENode(arc.getSource()),
					dGraph.getDartFromJMENode(arc.getDestination()),
					arc.getDimension());
		}
		return dGraph;
	}

	/**
	 * Try to fix the display of the both the LHS and the RHS of the rule. TODO:
	 * implement better graph display algorithms.
	 * 
	 */
	public void fixDisplay() {
		if (dLeft.size() < 250) {
			JMSDisplayable leftDisplayer = new JMSAttractionRepulsionDisplay(dLeft, 2, 1, 80);
			leftDisplayer.computeDisplay();
		}
		if (dRight.size() < 250) {
			JMSDisplayable rightDisplayer = new JMSAttractionRepulsionDisplay(dRight, 2, 1, 80);
			rightDisplayer.computeDisplay();
		}
	}
	
	public void fixFRDisplay() {
		JMSDisplayable leftDisplayer = new JMSFruchtermanReingold(dLeft);
		leftDisplayer.computeDisplay();
		JMSDisplayable rightDisplayer = new JMSFruchtermanReingold(dRight);
		rightDisplayer.computeDisplay();

	}

	public void randomDisplay() {
		JMSDisplayable leftDisplayer = new JMSRandomDisplay(dLeft);
		leftDisplayer.computeDisplay();
		JMSDisplayable rightDisplayer = new JMSRandomDisplay(dRight);
		rightDisplayer.computeDisplay();
	}

}
