package fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils;

/**
 * Exception used when the LHS or the RHS is not specified in rule inference.
 * 
 * @author romain
 *
 */
public class GmapException extends Exception {

	private static final long serialVersionUID = -6534269100029709332L;
	
	public GmapException() {
	}

	public GmapException(String message) {
		super(message);
	}

	public GmapException(Throwable cause) {
		super(cause);
	}

	public GmapException(String message, Throwable cause) {
		super(message, cause);
	}

	public GmapException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
