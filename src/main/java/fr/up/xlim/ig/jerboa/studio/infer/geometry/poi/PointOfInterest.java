package fr.up.xlim.ig.jerboa.studio.infer.geometry.poi;

import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import up.jerboa.core.JerboaDart;
import up.jerboa.exception.JerboaException;

/**
 * Point of interest, used as a topological abstraction of darts
 * 
 * @author romain
 *
 */
public interface PointOfInterest<E> {
	JMENode poiNode();

	E computeValue(JerboaDart d) throws JerboaException;

	String exportToCode(String name);
	
	String toString();
}
