package fr.up.xlim.ig.jerboa.ebds.factories;

import java.util.List;

import com.google.ortools.linearsolver.MPVariable;

import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import up.jerboa.core.JerboaDart;

/**
 * Factory for the color embeddings
 * 
 * @author Romain
 *
 */
public class Point3Factory implements VectEbdAbstractFactory {
	
	/**
	 * Get the name of Point3 for embedding computations.
	 * 
	 * @return name of Point3
	 */
	@Override
	public String getEbdName() {
		return "position";
	}

	/**
	 * Get the class name of Point3.
	 * 
	 * @return class name of Point3
	 */
	@Override
	public String getEbdClass() {
		return "Point3";
	}

	/**
	 * Build an object of type Point3 with default value.
	 * 
	 * @return Color3 default object
	 */
	public Point3 createEbdDefaultValue(){ return new Point3();}
	
	/**
	 * Build an object of type Color3 with from an array of variables.
	 * 
	 * @param translation: OrTools variables for the translation
	 * @return Color3 object
	 */
	public Point3 createEbdOrToolsVariable(MPVariable[] translation){
		return new Point3(
				(float) translation[0].solutionValue(),
				(float) translation[1].solutionValue(),
				(float) translation[2].solutionValue());
	}	
	
	/**
	 * Build an object of type Point3 as a copy of another Point3 object .
	 * 
	 * @param embedding: Point3 variables for the copy
	 * @return Point3 object
	 */
	@Override
	public Point3 createEbdCopy(VectorialEbd embedding) {
		if (embedding instanceof Point3)
			return new Point3((Point3) embedding);
		else
			return new Point3();
	}
	
	/**
	 * Build an object of type Point3 as a random Point3 object.
	 * 
	 * @return Point3 object
	 */
	@Override
	public Point3 createEbdRandom() {
		return Point3.randomPoint();
	}

	/**
	 * Build an object of type Point3 as the middle of Point3 values of a list of darts.
	 * 
	 * @param darts : list of darts to compute the middle from.
	 * @return instance of the embedding
	 */
	@Override
	public VectorialEbd createEbdMiddle(List<JerboaDart> darts) {
		return Point3.middle(darts,"position");
	}
	
}

