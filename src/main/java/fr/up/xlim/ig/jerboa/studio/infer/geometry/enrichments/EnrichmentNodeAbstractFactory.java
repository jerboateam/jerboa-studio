package fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments;

import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;

/**
 * Abstract Factory for the enrichment of nodes.
 * 
 * @author Romain
 *
 */
public interface EnrichmentNodeAbstractFactory {
	/**
	 * Create an instance of an enrichment node.
	 * 
	 * @return instance of the enrichment node
	 */
	public VectEbdEnrichmentNode createEnrichmentNode(VectEbdEnrichmentRule eRule, JMENode node);
	
}
