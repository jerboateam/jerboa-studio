package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

import java.util.ArrayList;
import java.util.List;

import com.google.ortools.Loader;
import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPObjective;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPVariable;

import fr.up.xlim.ig.jerboa.ebds.Point3;
import up.jerboa.core.util.Pair;

/**
 * OrTools-based implementation of the linear programming solution for the
 * geometric inference.
 * 
 * This was for testing purposes and probably no longer used.
 * 
 * @author Romain and Hakim
 *
 */
public class LPsolverOrTools implements LPsolver {
	
	public Pair<List<Double>,Point3> guessCoefsSystem(List<Pair<List<Point3>, Point3>> equationSystem) throws LPSolverException{
		try {
			System.out.println("Attempt without translation");
			return guessCoefsSystemTranslation(equationSystem, false);
		} catch(LPSolverException lpse) {
			System.out.println("Attempt with translation");
			return guessCoefsSystemTranslation(equationSystem, true);
		}
	}
	
	public Pair<List<Double>,Point3> guessCoefsSystemTranslation(List<Pair<List<Point3>, Point3>> equationSystem, boolean withTranslation) throws LPSolverException{
		Loader.loadNativeLibraries(); 
		final double infinity = MPSolver.infinity();
		MPSolver solver = MPSolver.createSolver("GLOP");
		solver.clear(); // Clears the objective (including the optimization direction), all variables and constraints
		List<Double> res = new ArrayList<>();
		List<MPVariable> vars = new ArrayList<>();
		
		// La translation est commune a tous les points
		MPVariable[] translation = new MPVariable[] { 
			solver.makeNumVar(-infinity, infinity, "tx"),
			solver.makeNumVar(-infinity, infinity, "ty"),
			solver.makeNumVar(-infinity, infinity, "tz")
		};
		
		// Les coeffs sont communs a toutes les variables
		for(int i = 0;i < equationSystem.get(0).l().size(); ++i) {
			MPVariable coef = solver.makeNumVar(-infinity, infinity, "a" + i);
			vars.add(coef);
		}
		
		for (int eqnIndex = 0 ; eqnIndex  < equationSystem.size(); eqnIndex++) {
			Pair<List<Point3>, Point3> eqn = equationSystem.get(eqnIndex);
			List<Point3> pts = eqn.l();
			Point3 d = eqn.r();
			MPConstraint x = solver.makeConstraint(d.getX(), d.getX(), "x" + eqnIndex);
			MPConstraint y = solver.makeConstraint(d.getY(), d.getY(), "y" + eqnIndex);
			MPConstraint z = solver.makeConstraint(d.getZ(), d.getZ(), "z" + eqnIndex);
			MPConstraint[] cs = new MPConstraint[] { x , y, z }; 
			
			for(int dim = 0; dim < 3; ++dim) {
				for(int i = 0;i < pts.size(); ++i) {
					Point3 p = pts.get(i);
					cs[dim].setCoefficient(vars.get(i), p.get(dim));
				}
				if (withTranslation)
					cs[dim].setCoefficient(translation[dim], 1);
				else 
					cs[dim].setCoefficient(translation[dim], 0);
			}
			
		}
		
		System.out.println("SOLVER OrTools: VARS="+solver.numVariables() + " Constraints="+solver.numConstraints());
		MPObjective objective = solver.objective();
		objective.setMinimization();
		
		
		final MPSolver.ResultStatus resultStatus = solver.solve();
		System.out.println("Solved? " + resultStatus);
		if (resultStatus == MPSolver.ResultStatus.OPTIMAL || resultStatus == MPSolver.ResultStatus.FEASIBLE) {
			//System.out.println("Solution:");
			//System.out.println("Objective value = " + objective.value());
			for (int eqnIndex = 0 ; eqnIndex  < equationSystem.size(); eqnIndex++) {
				Pair<List<Point3>, Point3> eqn = equationSystem.get(eqnIndex);
				List<Point3> pts = eqn.l();
				Point3 d = eqn.r();
				for(int dim = 0; dim < 3; ++dim) {
					for(int i = 0;i < pts.size(); ++i) {
						Point3 p = pts.get(i);
						MPVariable a = vars.get(i);
						//System.out.print((i==0? " " : " + ") +a.solutionValue() + " * " + p.get(dim));
					}
					
					//System.out.println(" = " + d.get(dim));
				}
			}
			for(int i = 0;i < equationSystem.get(0).l().size(); ++i) {
				res.add(vars.get(i).solutionValue());
			}
			
		} else {
			System.err.println("The problem does not have an optimal solution!");
			throw new LPSolverException();
		}
		if (withTranslation)
			return new Pair<List<Double>, Point3>(res, new Point3(translation[0].solutionValue(),translation[1].solutionValue(),translation[2].solutionValue()));
		else // We forced the coef in front of translation to be 0, so translation can take any value.
			return new Pair<List<Double>, Point3>(res, new Point3(0,0,0));
	}


	public Pair<List<Double>,Point3> guessCoefs(List<Point3> pts, Point3 d) throws LPSolverException {
		try {
			System.out.println("Attempt translation+");
			return guessCoefsSigne(pts, d, true);
		} catch(LPSolverException lpse) {
			System.out.println("Attempt translation-");
			return guessCoefsSigne(pts, d, false);
		}
	}
	
	public Pair<List<Double>,Point3> guessCoefsSigne(List<Point3> pts, Point3 d, boolean positive) throws LPSolverException {
		
		Loader.loadNativeLibraries(); // pas sur qu'on puisse l'appeler plusieurs fois
		final double infinity = MPSolver.infinity();
		
		MPSolver solver = MPSolver.createSolver("GLOP");
		List<Double> res = new ArrayList<>();
		List<MPVariable> vars = new ArrayList<>();
		MPConstraint x = solver.makeConstraint(d.getX(), d.getX(), "x");
		MPConstraint y = solver.makeConstraint(d.getY(), d.getY(), "y");
		MPConstraint z = solver.makeConstraint(d.getZ(), d.getZ(), "z");
		MPConstraint[] cs = new MPConstraint[] { x , y, z }; 
		
		MPVariable[] translation = new MPVariable[] { 
			solver.makeNumVar(-infinity, infinity, "tx"),
			solver.makeNumVar(-infinity, infinity, "ty"),
			solver.makeNumVar(-infinity, infinity, "tz")
		};
		
		for(int i = 0;i < pts.size(); ++i) {
			MPVariable coef = solver.makeNumVar(-infinity, infinity, "a" + i);
			vars.add(coef);
		}
		
		MPConstraint nondej = null;
		if(positive)
			nondej = solver.makeConstraint(Double.MIN_NORMAL, MPSolver.infinity(), "translation+");
		else
			nondej = solver.makeConstraint(-MPSolver.infinity(),Double.MIN_NORMAL, "translation-");
		System.out.println("CONTRAINTE : " + nondej.getClass().getName());
		nondej.setCoefficient(translation[0], 1);
		nondej.setCoefficient(translation[1], 1);
		nondej.setCoefficient(translation[2], 1);
		
		for(int dim = 0; dim < 3; ++dim) {
			for(int i = 0;i < pts.size(); ++i) {
				Point3 p = pts.get(i);
				cs[dim].setCoefficient(vars.get(i), p.get(dim));
			}
			cs[dim].setCoefficient(translation[dim], 1);
		}
		
		System.out.println("SOLVER OrTools: VARS="+solver.numVariables() + " Constraints="+solver.numConstraints());
		MPObjective objective = solver.objective();
		objective.setMinimization();
		
		final MPSolver.ResultStatus resultStatus = solver.solve();
		System.out.println("Solved? " + resultStatus);
		if (resultStatus == MPSolver.ResultStatus.OPTIMAL || resultStatus == MPSolver.ResultStatus.FEASIBLE) {
			System.out.println("Solution:");
			System.out.println("Objective value = " + objective.value());
			for(int dim = 0; dim < 3; ++dim) {
				for(int i = 0;i < pts.size(); ++i) {
					Point3 p = pts.get(i);
					MPVariable a = vars.get(i);
					System.out.print((i==0? " " : " + ") +a.solutionValue() + " * " + p.get(dim));
				}
				
				System.out.println(" = " + d.get(dim));
			}
			for(int i = 0;i < pts.size(); ++i) {
				res.add(vars.get(i).solutionValue());
			}
			
		} else {
			System.err.println("The problem does not have an optimal solution!");
			throw new LPSolverException();
		}
		return new Pair<List<Double>, Point3>(res, new Point3(translation[0].solutionValue(),translation[1].solutionValue(),translation[2].solutionValue()));
	}
	
}
