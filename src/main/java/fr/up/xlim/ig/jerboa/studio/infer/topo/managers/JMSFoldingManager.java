package fr.up.xlim.ig.jerboa.studio.infer.topo.managers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import fr.up.xlim.ig.jerboa.studio.infer.parameters.JerboaInferRuleParameters;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSFoldGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSFoldSeed;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleNode;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils.JMSFoldingQueueElement;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoDart;
import up.jerboa.core.JerboaOrbit;

/**
 * Store folding info (folding graph, folded graph, queue, maps) to compute
 * foldings.
 * 
 * @author romain
 *
 */
public class JMSFoldingManager {

	private JMSFoldGraph foldGraph;
	private JMSRuleGraph ruleGraph;
	private JMSFoldSeed seed;

	private JMSQueueManager queueManager;
	private HashMap<JMSTopoDart, JMSRuleNode> dartToNode;

	private JerboaInferRuleParameters inferRuleParameters;

	public JMSFoldingManager(JMSFoldGraph graph, JMSTopoDart seedDart,
			JerboaOrbit seedOrbit, String ruleName,
			JerboaInferRuleParameters inferRuleParameters) {
		this.foldGraph = graph;
		this.seed = new JMSFoldSeed(seedDart, seedOrbit);
		this.ruleGraph = new JMSRuleGraph(graph, ruleName);

		this.queueManager = new JMSQueueManager(this);
		this.dartToNode = new HashMap<JMSTopoDart, JMSRuleNode>();

		this.inferRuleParameters = inferRuleParameters;
	}

	public JMSFoldGraph getFoldGraph() {
		return foldGraph;
	}

	public JMSRuleGraph getRuleGraph() {
		return ruleGraph;
	}

	public JMSFoldSeed getSeed() {
		return seed;
	}

	/**
	 * Determine whether a topological dart (from the folding graph) has been
	 * mapped to a node (of the rule graph).
	 * 
	 * @param dart : the dart from the folding graph.
	 * @return true is the dart has been mapped.
	 */
	public boolean hasNode(JMSTopoDart dart) {
		return dartToNode.containsKey(dart);
	}

	/**
	 * Return the node (of the rule graph) associated to a topological dart
	 * (from the folding graph) if one has already been associated.
	 * 
	 * @param dart : the dart from the folding graph.
	 * @return the associated node (null if none have been mapped yet).
	 */
	public JMSRuleNode nodeFromDart(JMSTopoDart dart) {
		return dartToNode.get(dart);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JMSFoldingManager [graph=");
		builder.append(foldGraph);
		builder.append(",\nseed=");
		builder.append(seed);
		builder.append(",\nqueueManager=");
		builder.append(queueManager);
		builder.append("]");
		return builder.toString();
	}

	// ~~ RULE GRAPH ~~
	/**
	 * Create rule node from a queue element.
	 * 
	 * @param queueElement
	 * @return rule node
	 */
	public JMSRuleNode buildSupportNode(JMSFoldingQueueElement queueElement) {
		JMSRuleNode previousNode = queueElement.getNode();
		int arcDim = queueElement.getArcDim();

		JMSRuleNode currNode = new JMSRuleNode(previousNode, arcDim);

		currNode.buildOrbitAsSeedMapping(seed, inferRuleParameters);

		if (currNode.orbitDimsContains(JMSFoldGraph.kappa)) {
			System.err.println("Kappa in the orbit");
		}
		return currNode;
	}

	/**
	 * Add a node to the rule graph. Also builds its loops.
	 * 
	 * @param node : node to be added to the rule graph.
	 */
	private void addNode(JMSRuleNode node) {
		ruleGraph.addDart(node);
		node.extractLoops();
		for (JMSTopoDart dart : node.getOrbitDarts()) {
			dartToNode.put(dart, node);
		}
	}

	/**
	 * Add the seed as a node to the rule graph.
	 */
	public void addSeedAsANode() {
		addNode(seed);
	}

	/**
	 * Extends the rule graph by adding a new node and linking it according to
	 * the traversal of the folding graph.
	 * 
	 * @param node         : node to be added to the rule graph.
	 * @param queueElement : queue element that lead to the construction of the
	 *                     new node.
	 */
	public void addNodeToGraph(JMSRuleNode node,
			JMSFoldingQueueElement queueElement) {
		addNode(node);
		int arcDim = queueElement.getArcDim();
		JMSRuleNode adjacentNode = queueElement.getNode();
		ruleGraph.addArc(adjacentNode, node, arcDim);
	}

	/**
	 * Determine whether all topological darts have been mapped to a rule node.
	 * 
	 * Assuming the absence of bug in the code, this method currently have no
	 * use. In practise, it is really handy to spot out bugs.
	 * 
	 * @return true if all topological darts have been mapped to a rule node.
	 */
	public boolean areAllDartSeen() {
		Set<JMSTopoDart> foldingGraphDartIDs = foldGraph.getDarts().stream()
				.collect(Collectors.toSet());
		Set<JMSTopoDart> ruleGraphMappedDartID = new HashSet<>();

		for (JMSRuleNode node : ruleGraph.getDarts()) {
			ruleGraphMappedDartID.addAll(node.getOrbitDarts());
		}
		return foldingGraphDartIDs.size() == ruleGraphMappedDartID.size()
				&& foldingGraphDartIDs.containsAll(ruleGraphMappedDartID);
	}

	// ~~ QUEUE ~~
	public boolean isQueueEmpty() {
		return queueManager.isQueueEmpty();
	}

	/**
	 * Pops an element from the stack. In other words, removes and returns the
	 * first element of this dequeue.
	 *
	 * @return the element at the front of the queue
	 */
	public JMSFoldingQueueElement popQueue() {
		return queueManager.popQueue();
	}

	/**
	 * Initialize the dequeue (add seed) to run the graph traversal. The dart
	 * corresponding to the seed must have been mapped the the node
	 * preemptively.
	 * 
	 * @param seed : node (mapped from darts) used for the local search.
	 * 
	 * @return true if the queue has properly been initialized.
	 */
	public boolean initQueue(JMSFoldSeed seed) {
		return queueManager.initQueue(seed);
	}

	/**
	 * Update queue (add new elements) after a node has been added to the graph
	 * and update the info.
	 * 
	 * The dart corresponding to the seed must have been mapped the the node
	 * preemptively.
	 * 
	 * @param toCheckNode : node (mapped from darts) used for the local search.
	 * @return true if the queue can be updated (no contradiction with elements
	 *         already in the queue).
	 */
	public boolean updateQueue(JMSRuleNode toCheckNode) {
		return queueManager.updateQueue(toCheckNode);
	}
}
