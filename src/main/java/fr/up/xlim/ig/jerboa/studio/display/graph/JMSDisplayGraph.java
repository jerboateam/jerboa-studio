package fr.up.xlim.ig.jerboa.studio.display.graph;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.stream.Collectors;

import fr.up.xlim.ig.jerboa.studio.graph.JMSDartBasedGraph;
import fr.up.xlim.ig.jerboa.studio.graph.Path;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEGraph;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;

/**
 * Graph structure to auto-generate the display of a rule.
 * 
 * @author Romain
 *
 */
public class JMSDisplayGraph implements
		JMSDartBasedGraph<Integer, JMSDisplayDart>, Iterable<JMSDisplayDart> {


	private JMEGraph jmegraph;
	private HashMap<Integer, JMSDisplayDart> darts;

	public JMSDisplayGraph(JMEGraph jmegraph) {
		this.jmegraph = jmegraph;
		this.darts = new HashMap<>();
	}

	class JMSDisplayGraphIterator implements Iterator<JMSDisplayDart> {
		int pos = 0;
		Iterator<Integer> idIterator = darts.keySet().iterator();

		@Override
		public boolean hasNext() {
			return pos < size();
		}

		@Override
		public JMSDisplayDart next() {
			JMSDisplayDart node = darts.get(idIterator.next());
			pos++;
			return node;
		}

		@Override
		public void remove() {
			// not supported
		}

	}

	@Override
	public Iterator<JMSDisplayDart> iterator() {
		return new JMSDisplayGraphIterator();
	}

	@Override
	public boolean noDart() {
		return darts.size() == 0;
	}

	@Override
	public int size() {
		return darts.size();
	}

	@Override
	public Collection<JMSDisplayDart> getDarts() {
		return darts.values();
	}

	@Override
	public JMSDisplayDart getAnyDart() {
		return this.iterator().next();
	}

	@Override
	public boolean hasDart(JMSDisplayDart dart) {
		return darts.containsValue(dart);
	}

	@Override
	public boolean labelValid(Integer label) {
		return label >= 0
				|| label <= jmegraph.getRule().getModeler().getDimension();
	}

	@Override
	public void addDart(JMSDisplayDart dart) {
		darts.put(dart.getID(), dart);
	}

	@Override
	public void removeDart(JMSDisplayDart dart) {
		darts.remove(dart.getID());
	}

	@Override
	public void addArc(JMSDisplayDart sourceDart, JMSDisplayDart targetDart,
			Integer label) {
		if (!labelValid(label))
			return;
		if (sourceDart.isFree(label) && targetDart.isFree(label)) {
			sourceDart.addArc(targetDart, label);
			targetDart.addArc(sourceDart, label);
		}

	}

	@Override
	public void removeArc(JMSDisplayDart dart, Integer label) {
		JMSDisplayDart target = dart.getNeighbour(label);
		if (target != null) {
			target.removeArc(dart, label);
			dart.removeArc(target, label);
		}
	}

	@Override
	public boolean equalsAllDarts(Collection<JMSDisplayDart> dartCollection) {
		return equalsAllDartIDs(dartCollection.stream().map(d -> d.getID())
				.collect(Collectors.toSet()));
	}

	/**
	 * Compare a collection of dart IDs to see if it matches the IDs of the
	 * graph darts.
	 * 
	 * @param dartIDs : collections of dart IDs (Integers)
	 * @return boolean
	 */
	public boolean equalsAllDartIDs(Collection<Integer> dartIDs) {
		return dartIDs.size() == darts.keySet().size()
				&& dartIDs.containsAll(darts.keySet());
	}

	/**
	 * Compute the shortest path distances from the dart source to all darts of
	 * the graph.
	 * 
	 * @param source : JMSDisplayDart to compute the distance from
	 * @return shortest path distances as a Map(key:= dart, value := distance)
	 */
	public HashMap<JMSDisplayDart, Integer> distFrom(JMSDisplayDart source) {
		HashMap<JMSDisplayDart, Integer> dist = new HashMap<JMSDisplayDart, Integer>();
		ArrayDeque<JMSDisplayDart> queue = new ArrayDeque<JMSDisplayDart>();
		HashSet<JMSDisplayDart> seen = new HashSet<JMSDisplayDart>();

		dist.put(source, 0);
		queue.add(source);
		seen.add(source);

		while (!queue.isEmpty()) {
			JMSDisplayDart nextNode = queue.pop();
			for (JMSDisplayDart neigh : nextNode.getNeighboursUpToLoops()) {
				if (!seen.contains(neigh)) {
					dist.put(neigh, dist.get(nextNode) + 1);
					queue.add(neigh);
					seen.add(neigh);
				}
			}
		}

		return dist;
	}

	/**
	 * Compute the shortest path distances between any two darts of the graph.
	 * 
	 * @return shortest path distances as a Map(key:= path(source, target),
	 *         value := distance)
	 */
	public HashMap<Path<JMSDisplayDart>, Integer> allDist() {
		HashMap<Path<JMSDisplayDart>, Integer> dist = new HashMap<>();
		for (JMSDisplayDart source : darts.values()) {
			HashMap<JMSDisplayDart, Integer> fromSource = distFrom(source);
			for (JMSDisplayDart target : fromSource.keySet()) {
				dist.put(new Path<>(source, target), fromSource.get(target));
			}
		}
		return dist;
	}

	public JMSDisplayDart getDartFromJMENode(JMENode node) {
		return darts.get(node.getID());
	}

}
