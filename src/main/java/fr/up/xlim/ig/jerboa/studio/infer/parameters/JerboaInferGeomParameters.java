package fr.up.xlim.ig.jerboa.studio.infer.parameters;

import fr.up.xlim.sic.ig.jerboa.viewer.tools.ui.UIPrefDialog;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.ui.UIPrefItem;

/**
 * 
 * Inference parameters for the geometric inference
 * 
 * Currently supports :
 * 		- toggle on/off
 * 
 * @author romain
 *
 */
public class JerboaInferGeomParameters {

	@UIPrefItem(name = "Infer positions?", desc = "Toggle the geometric inference on or off (for the position embedding)")
	public boolean inferPositions;
	@UIPrefItem(name = "Infer colors?", desc = "Toggle the geometric inference on or off (for the color embedding)")
	public boolean inferColors;
	@UIPrefItem(name = "Coefficient threshold:", desc = "Threshold to discard the contributions of the points of interest.")
	public double coefThreshold;

	public UIPrefDialog dialog;

	/**
	 * Specify whether to infer the positions.
	 * @return true if the inference should also infer the positions, false otherwise.
	 */
	public boolean getInferPositions() {
		return inferPositions;
	}

	public void setInferPositions(boolean inferPositions) {
		this.inferPositions = inferPositions;
	}
	
	/**
	 * Specify whether to infer the positions.
	 * @return true if the inference should also infer the positions, false otherwise.
	 */
	public boolean getInferColors() {
		return inferColors;
	}
	
	public void setInferColors(boolean inferColors) {
		this.inferColors = inferColors;
	}

	public double getCoefThreshold() {
		return coefThreshold;
	}

	public void setCoefThreshold(double coefThreshold) {
		this.coefThreshold = coefThreshold;
	}

	public UIPrefDialog getDialog() {
		return dialog;
	}
	
	public JerboaInferGeomParameters() {
		this.inferPositions = true;
		this.inferColors = true;
		this.coefThreshold = 0.001;
		this.dialog = new UIPrefDialog(this, "misc");
	}
	
	
	
}
