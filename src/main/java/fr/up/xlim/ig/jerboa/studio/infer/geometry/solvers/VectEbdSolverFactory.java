package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

/**
 * Factory for the vectorial embedding solvers.
 * 
 * @author Romain
 *
 */
public class VectEbdSolverFactory {

	/**
	 * Get an instance of the vectorial embedding solver
	 * 
	 * @return solver
	 */
	public static VectEbdSolver createVectEbdSolver(VectEbdSolverAbstractFactory factory) {
		return factory.createVectEbdSolver();
	};
}
