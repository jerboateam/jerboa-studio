package fr.up.xlim.ig.jerboa.studio.infer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments.ColorEnrichmentFactory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments.PositionEnrichmentFactory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments.VectEbdEnrichmentRule;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.PoIStrategy;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers.VectEbdSolver;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERuleAtomic;
import up.jerboa.core.JerboaDart;

/**
 * 
 * Rule structure with mapping from JMENodes to JerboaDarts and embedding
 * enrichments. At the moment, only the position enrichment is supported.
 * 
 * @author romain
 */
public class JMSExtendedRule {

	private JMERuleAtomic atomic;
	private Map<JMENode, List<JerboaDart>> mappingL;
	private Map<JMENode, List<JerboaDart>> mappingR;
	private VectEbdEnrichmentRule peRule;
	private VectEbdEnrichmentRule ceRule;

	/**
	 * 
	 * @param rule : atomic rule that should be extended.
	 */
	public JMSExtendedRule(JMERuleAtomic rule) {
		this.atomic = rule;
		this.mappingL = new HashMap<JMENode, List<JerboaDart>>();
		this.mappingR = new HashMap<JMENode, List<JerboaDart>>();
		this.peRule = null;
		this.ceRule = null;
	}

	/**
	 * 
	 * @return the underlying atomic rule
	 */
	public JMERuleAtomic getRule() {
		return atomic;
	}

	/**
	 * 
	 * @return the mapping of the left nodes as a map (nodes, list_of_darts)
	 */
	public Map<JMENode, List<JerboaDart>> getMappingL() {
		return mappingL;
	}

	/**
	 * 
	 * @return the mapping of the right nodes as a map (nodes, list_of_darts)
	 */
	public Map<JMENode, List<JerboaDart>> getMappingR() {
		return mappingR;
	}

	/**
	 * Determine whether the atomic rule has been enriched with positions for the missing nodes.
	 * 
	 * @return true if the nodes have been enriched with position embedding, false otherwise.
	 */
	public boolean isPositionEnriched() {
		return peRule != null && peRule.isEmbeddingEnriched();
	}

	/**
	 * 
	 * @return the position enrichment rule (see {@link VectEbdEnrichmentRule})
	 */
	public VectEbdEnrichmentRule getPeRule() {
		return peRule;
	}

	/**
	 * Enrich the position embedding via the position enrichment rule.
	 * 
	 * @param peSolver 	: solver used to find a solution to the linear system
	 * @param poiStrat 	: strategy for the points of interest
	 * @param threshold	: threshold to discard the contributions of the PoI. 
	 */
	public void enrichPositions(VectEbdSolver peSolver,PoIStrategy poiStrat, double threshold) {
		VectEbdEnrichmentRule peRule = new VectEbdEnrichmentRule(this, peSolver,
				poiStrat, atomic.getModeler().getEmbedding("position"), new PositionEnrichmentFactory(), threshold);
		peRule.buildEbdEnrichment();
		this.peRule = peRule;
	}
	
	/**
	 * Enrich the position embedding via the position enrichment rule.
	 * 
	 * For empty LHS
	 */
	public void enrichPositionCreationRule() {
		this.peRule = new VectEbdEnrichmentRule(this, atomic.getModeler().getEmbedding("position"), new PositionEnrichmentFactory());
		this.peRule.solveCreationRule();
	}

	/**
	 * Determine whether the atomic rule has been enriched with colors for the missing nodes.
	 * 
	 * @return true if the nodes have been enriched with color embedding, false otherwise.
	 */
	public boolean isColorEnriched() {
		return ceRule != null && ceRule.isEmbeddingEnriched();
	}

	/**
	 * 
	 * @return the color enrichment rule (see {@link VectEbdEnrichmentRule})
	 */
	public VectEbdEnrichmentRule getCeRule() {
		return ceRule;
	}
	
	/**
	 * Enrich the color embedding via the color enrichment rule.
	 * 
	 * @param ceSolver : solver used to find a solution to the linear system
	 * @param poiStrat : strategy for the points of interest
	 * @param threshold	: threshold to discard the contributions of the PoI. 
	 */
	public void enrichColors(VectEbdSolver ceSolver, PoIStrategy poiStrat, double threshold) {
		VectEbdEnrichmentRule ceRule = new VectEbdEnrichmentRule(this, ceSolver,
				poiStrat, atomic.getModeler().getEmbedding("color"), new ColorEnrichmentFactory(), threshold);
		ceRule.buildEbdEnrichment();
		this.ceRule = ceRule;
		
	}
	
	/**
	 * Enrich the color embedding via the color enrichment rule.
	 * 
	 * For empty LHS
	 */
	public void enrichColorCreationRule() {
		this.ceRule = new VectEbdEnrichmentRule(this, atomic.getModeler().getEmbedding("color"), new ColorEnrichmentFactory());
		this.ceRule.solveCreationRule();
	}
}
