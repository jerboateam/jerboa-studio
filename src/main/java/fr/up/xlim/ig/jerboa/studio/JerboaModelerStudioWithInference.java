package fr.up.xlim.ig.jerboa.studio;

import up.jerboa.core.JerboaRuleOperation;
import up.jerboa.exception.JerboaException;

public class JerboaModelerStudioWithInference extends JerboaModelerStudio {
	
	public JerboaModelerStudioWithInference(JerboaStudio studios) throws JerboaException {
		super();
		        
	}
	
	@Override
	public void registerRule(JerboaRuleOperation jerboaRule) {
		if(rules.contains(jerboaRule))
			rules.remove(jerboaRule);
		rules.add(jerboaRule);
	}

}
