/**
 * 
 */
package fr.up.xlim.ig.jerboa.studio;

 import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.up.xlim.ig.jerboa.bridge.FILEFORMAT;
import fr.up.xlim.ig.jerboa.bridge.ViewerBridge;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.studio.JerboaStudio.AllAssocDarts.AssocDart;
import fr.up.xlim.ig.jerboa.studio.JerboaStudio.ListDartsUI.ListDartUI;
import fr.up.xlim.ig.jerboa.studio.infer.JMSExtendedRule;
import fr.up.xlim.ig.jerboa.studio.infer.JerboaInferRuleGenerator;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments.VectEbdEnrichmentNode;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.inject.EnrichedColor3;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.inject.EnrichedPoint3;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.inject.JerboaRuleGeneratedImpl;
import fr.up.xlim.sic.ig.jerboa.jme.forms.JerboaModelerEditor;
import fr.up.xlim.sic.ig.jerboa.jme.forms.JerboaModelerEditorParent;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEElement;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEModeler;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERule;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERuleAtomic;
import fr.up.xlim.sic.ig.jerboa.jme.model.util.preferences.JMEPreferences;
import fr.up.xlim.sic.ig.jerboa.jme.view.JMEElementView;
import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaMonitorInfo;
import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaMonitorInfoConsole;
import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaOrbitFormatter;
import fr.up.xlim.sic.ig.jerboa.viewer.GMapViewer;
import fr.up.xlim.sic.ig.jerboa.viewer.IJerboaModelerViewer;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaGMapArray;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.core.util.Pair;
import up.jerboa.exception.JerboaException;

/**
 * @author hbelhaou and romain
 *
 */
public class JerboaStudio implements JerboaModelerEditorParent,JMEElementView {

	public static void main(String[] args) {
		
		String hostname = null;
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JerboaStudio studio = new JerboaStudio("JerboaModelerStudio.jme");
		
		JerboaMonitorInfo worker = new JerboaMonitorInfoConsole();
		
		if(args.length > 0) {
			String leftobj = args[0];
			File leftfile = new File(leftobj);
			if(leftfile.exists()) {
				studio.bridgeLeft.loadFile(leftfile, FILEFORMAT.AUTO, worker);
			}
			if(args.length > 1) {
				String rightobj = args[1];
				File rightfile = new File(rightobj);
				if(rightfile.exists()) {
					studio.bridgeRight.loadFile(rightfile, FILEFORMAT.AUTO, worker);
				}
				if(args.length>2) {
					String smapfile = args[2];
					File mapfile = new File(smapfile);
					if(mapfile.exists()) {
						studio.loadMappings(mapfile);
					}
				}	
			}
		}
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				studio.frameview.invalidate();
				studio.frameview.repaint(1000);
				studio.jmvLeft.updateIHM();
				studio.jmvLeft.hideConsole();
				studio.jmvRight.hideConsole();
				studio.jmvRight.hideRuleList();
			}
		});
		
		studio.show();
		
	}
	
	//===========================================================================================================================================================================
	
	
	public JFrame frameview;
	private JMEPreferences preferences;
	private JerboaModelerEditor jme; // current
	protected GMapViewer jmvLeft;
	protected GMapViewer jmvRight;
	private JerboaModelerStudioWithInference modelerLeft;
	private JerboaModelerStudioWithInference modelerRight;
	private JMEModeler modelered;
	protected ViewerBridge bridgeLeft;
	protected ViewerBridge bridgeRight;
	private int historic;
	private AllAssocDarts assocdarts;
	private ListDartsUI leftpattern;
	private ListDartsUI rightpattern;
	
	
	private JerboaInferRuleGenerator rulegenerator;
	
	
	public class JerboaStudioPart {
		protected JerboaModelerStudio modeler;
		protected GMapViewer viewer;
		protected JerboaInferRuleGenerator rulegenerator;
		protected JerboaModelerEditor editor;
		
		public JerboaStudioPart(JerboaModelerStudioWithInference modeler2) {
			this.modeler = modeler2;
		}
		public JerboaModelerStudio getModeler() {
			return modeler;
		}
		public GMapViewer getViewer() {
			return viewer;
		}
		public JerboaInferRuleGenerator getRuleGenerator() {
			return rulegenerator;
		}
		public JerboaModelerEditor getEditor() {
			return editor;
		}
		
		
	}
	

	private static int countRootFrame = 0;
	private static WindowListener winlis = new WindowAdapter() {
		
		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			countRootFrame++;
			System.out.println("COUNTROOTFRAME: "+countRootFrame);
			
		}
		@Override
		public void windowClosed(WindowEvent e) {
			countRootFrame--;
			System.out.println("COUNTROOTFRAME: "+countRootFrame);
			if(countRootFrame <= 0)
				System.exit(0);
		}
	};
	
	public JerboaStudio(String filejme) {
		preferences = new JMEPreferences();
		historic = 0;
		try {
			preferences.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		frameview = new JFrame("JerboaStudio - Viewer");
		rulegenerator = new JerboaInferRuleGenerator(this);
		
		try {
			modelered = JerboaModelerEditor.loadModeler(new File(filejme));
			modelerLeft = new JerboaModelerStudioWithInference(this);
			modelerRight = new JerboaModelerStudioWithInference(this);
			jme = new JerboaModelerEditor(this, modelered);
			modelered = jme.getModeler();
			
			JerboaStudioPart leftstudio = new JerboaStudioPart(modelerLeft);
			JerboaStudioPart rightstudio = new JerboaStudioPart(modelerRight);
			
			bridgeLeft = new ViewerBridge(leftstudio);
			jmvLeft = new GMapViewer(this.getWindowViewer(), modelerLeft, bridgeLeft);
			

			bridgeRight = new ViewerBridge(rightstudio);
			jmvRight = new GMapViewer(this.getWindowViewer(), modelerRight, bridgeRight);
		
			leftstudio.editor = getEditor();
			rightstudio.editor = getEditor();
			
			leftstudio.viewer = jmvLeft;
			rightstudio.viewer = jmvRight;
			
			leftstudio.rulegenerator = getRulegenerator();
			rightstudio.rulegenerator = getRulegenerator();
			
			
			
			assocdarts = new AllAssocDarts();
			
			leftpattern = new ListDartsUI(jmvLeft);
			rightpattern = new ListDartsUI(jmvRight);
			
			// MODIFICATION IHM VIEWER
			//makeIHMAssocOrbit();
			
			// MODIFICATION IHM EDITEUR
			JMenu infer = new JMenu("Inference");
			jme.menuBar.add(infer);
			
			JMenuItem genAllRules = new JMenuItem("Gen. all infered rules");
			JMenuItem clearAllRules = new JMenuItem("Clear all infered rules");
			infer.add(genAllRules);
			infer.add(clearAllRules);
			
			genAllRules.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println("GENERATE infered rules");
					generateAllInferedRules();
				}
			});
			
			clearAllRules.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					System.out.println("CLEAR infered rules");
					clearAllInferedRules();
				}
			});
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
		JTabbedPane maintab = new JTabbedPane();

		frameview.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JSplitPane splitPaneViewer = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, jmvLeft,jmvRight);
		splitPaneViewer.setDividerSize(10);
		splitPaneViewer.setContinuousLayout(true);
		splitPaneViewer.setOneTouchExpandable(true);
//		splitPaneViewer.setDividerLocation(0.5);
		
		//jme.setMinimumSize(new Dimension(10, 10));
		// JSplitPane splitPaneMain = new JSplitPane(JSplitPane.VERTICAL_SPLIT, jme, splitPaneViewer);

		JPanel viewers = new JPanel(new BorderLayout());
		
		viewers.add(splitPaneViewer);
		
		JPanel paraminfer = new JPanel(new BorderLayout());
		
		Box params = Box.createHorizontalBox();
		params.add(leftpattern);
		params.add(assocdarts);
		params.add(rightpattern);
		paraminfer.add(params, BorderLayout.CENTER);
		
		JPanel southparaminfer = new JPanel();
		JButton bntInfer = new JButton("Inference");
		bntInfer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				callInferRules();
				jmvLeft.updateAllRules();
				jmvRight.updateAllRules();
			}
			
		});
		JFormattedTextField orbitinfer = new JFormattedTextField(new JerboaOrbitFormatter());
		JButton bntSetorbitinfer = new JButton("Set");
		JButton bntSaveAllMappings = new JButton("Save mappings");
		JButton bntLoadAllMappings = new JButton("Load mappings");
		
		orbitinfer.setColumns(40);
		orbitinfer.setText("_");
		southparaminfer.add(bntInfer);
		southparaminfer.add(new JLabel(" Target orbit (put '_' for all orbits): "));
		southparaminfer.add(orbitinfer);
		southparaminfer.add(bntSetorbitinfer);
		southparaminfer.add(bntSaveAllMappings);
		southparaminfer.add(bntLoadAllMappings);
		paraminfer.add(southparaminfer, BorderLayout.SOUTH);
		
		viewers.add(paraminfer, BorderLayout.SOUTH);

		maintab.addTab("Viewers", viewers);
		maintab.addTab("Editor", jme);
		
		// frameview.getContentPane().add(jmv);
		frameview.getContentPane().add(maintab);
		frameview.pack();
		frameview.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frameview.addWindowListener(winlis);

		splitPaneViewer.setDividerLocation(0.7);//0.57); // 0.65
		

		bntSetorbitinfer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				getRulegenerator().getTopoParameters().setInferenceOrbit(orbitinfer.getText());
			}
			
		});
		
		bntSaveAllMappings.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				saveMappings();
			}
		});
		bntLoadAllMappings.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				loadMappings();
			}
		});
		
		final ImageIcon img = new ImageIcon(getClass().getResource("/image/logo.png"));
		frameview.setIconImage(img.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH));
		//frameview.setIconImage(img.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH));

		// frameview.setLocationRelativeTo(frame);
		
		rulegenerator.addParameters();
			
	}


	protected void clearAllInferedRules() {
		List<JMERule> rules = new ArrayList<>(modelered.getInferedRules());
		for (JMERule r : rules) {
			System.out.println("DELETE RULE: " + r);
			jme.closeRule(r);
			modelered.removeRule(r);
		}
		
		modelered.update();
		
		// TODO: suppression dans le viewer?
	}


	protected void generateAllInferedRules() {
		List<JMERule> rules = modelered.getInferedRules();
		for (JMERule r : rules) {
			System.out.println("Generate RULE: " + r);
			JerboaRuleGeneratedImpl ruleop = getRulegenerator().convertToView(r);
			modelerLeft.registerRule(ruleop);
			
			JerboaRuleGeneratedImpl ruleopRight = getRulegenerator().convertToView(r);
			modelerRight.registerRule(ruleopRight);
			
			
		}
		jmvLeft.updateAllRules();
		jmvRight.updateAllRules();
	}
	
	/**
	 * Retrieve the information in the IHM to launch the inference mechanism
	 * 
	 * @author romain
	 */
	public void callInferRules() {
		
		JerboaGMap leftGmap = modelerLeft.getGMap();
		JerboaGMap rightGmap = modelerRight.getGMap();
		
		Set<JerboaDart> leftOutOfRule = outOfRule(leftGmap, leftpattern);
		Set<JerboaDart> rightOutOfRule = outOfRule(rightGmap, rightpattern);
		
		Map<Integer, Integer> mapping = buildMapping();

		Random rand = new Random();
		String catname = JOptionPane.showInputDialog(
				"Entrez le nom de la category: ",
				"category" + rand.nextInt(10) + rand.nextInt(10));
		if (catname == null)
			return;
		
		String rulename = JOptionPane
				.showInputDialog("Entrez le prefix des regles: ", "");
		if (rulename == null) 
			return;		
		
		// Fix left and right elements for the inference
		rulegenerator.setLeftPatternRaw(leftGmap);
		rulegenerator.setRightPatternRaw(rightGmap);

		// Infer
		List<JMSExtendedRule> rules = rulegenerator.inferExtendedRules(modelered,
				modelerLeft, rulename, catname, mapping, leftOutOfRule,
				rightOutOfRule);
		System.out.println("End rule generator");

		// Add to JME
		for (JMSExtendedRule rule : rules) {
			JMERuleAtomic atomic = rule.getRule();
			System.out.println(" ADD NEW RULE EDITOR: " + atomic.getFullName());
			modelered.addInferRule(atomic);
		}
		modelered.update();

		// Add to viewer
		boolean inject = true;
		
		if (inject) {
			for (JMSExtendedRule rule : rules) {
				JerboaRuleGeneratedImpl ruleexe = rulegenerator
						.convertToView(rule.getRule());
	
				if (rule.isPositionEnriched()) {
					System.err.println("Position is enriched");
					Map<JMENode, VectEbdEnrichmentNode> posEnrichments = rule
							.getPeRule().getEbdEnrichment();
					System.err.println("posEnrichment size: "+posEnrichments.size());
					for (JMENode node : posEnrichments.keySet()) {
						VectEbdEnrichmentNode peNode = posEnrichments.get(node);
						int nid = ruleexe.getRightIndexRuleNode(node.getName());
						EnrichedPoint3 ebd = new EnrichedPoint3(nid, peNode);
						ruleexe.getRightRuleNode(nid).addExpression(ebd);
					}
				}
				
				if (rule.isColorEnriched()) {
					Map<JMENode, VectEbdEnrichmentNode> colEnrichments = rule
							.getCeRule().getEbdEnrichment();
					for (JMENode node : colEnrichments.keySet()) {
						VectEbdEnrichmentNode ceNode = colEnrichments.get(node);
						int nid = ruleexe.getRightIndexRuleNode(node.getName());
						EnrichedColor3 ebd = new EnrichedColor3(nid, ceNode);
						ruleexe.getRightRuleNode(nid).addExpression(ebd);
					}
				}
	
				getModelerLeft().registerRule(ruleexe);
				getModelerRight().registerRule(ruleexe);
			}
		}

		System.out.println("Mecanism of inference topo/geo done!");
	}
	
	/**
	 * Build left to right mapping based on the association information in the IHM
	 * 
	 * @author romain
	 * @return mapping: Left to Right mapping on Viewer Gmaps.
	 */
	private Map<Integer, Integer> buildMapping() {
		
		Component[] components = assocdarts.list.getComponents();
		Map<Integer, Integer> mapping = new HashMap<Integer, Integer>(); 
		for(Component component : components) {
			AssocDart ad = (AssocDart) component;
			JerboaDart leftDart = ad.left;
			JerboaDart rightDart = ad.right;
			mapping.put(leftDart.getID(), rightDart.getID());
		}
		
		return mapping;
	}
	
	/**
	 * Determine the elements of a Gmap which are out of scope for the operation
	 * that we are inferring.
	 * 
	 * @param gmap : reference Gmap
	 * @param ui : IHM information
	 * @return set of dart in the Gmap out of the scope of the rule.
	 */
	private Set<JerboaDart> outOfRule(JerboaGMap gmap, ListDartsUI ui){
		Set<JerboaDart> outOfRule = gmap.stream().collect(Collectors.toSet());
				
		Component[] uis = ui.list.getComponents();
		for (Component cdui : uis) {
			JerboaDart dui = ((ListDartUI) cdui).dart;
			outOfRule.remove(dui);
		}
				
		return outOfRule;
	}
	
	
	public void reload() {
		if (frameview != null) {
			if (jme != null)
				frameview.setTitle("JerboaStudio: " + jme.getModeler().getName() + (jme.getModeler().isModified()? "*" : ""));
			else
				frameview.setTitle("JerboaStudio");
		}
	}

	@Override
	public void unlink() {
		// TODO Auto-generated method stub
		
	}
	
	public JerboaInferRuleGenerator getRulegenerator() {
		return rulegenerator;
	}

	@Override
	public JMEElement getSourceElement() {
		return jme.getSourceElement();
	}

	
	public Window getWindowViewer() {
		return frameview;
	}
	
	@Override
	public Window getParent() {
		return frameview;
	}

	@Override
	public void change(JerboaModelerEditor jme) {
		System.out.println("FRAME IS NULL IN PARENT");
		frameview = new JFrame("JerboaModelerEditor: " + jme.getModeler().getName());
		if(preferences.getMaximizeWindow())
			frameview.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frameview.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameview.getContentPane().add(jme);
		frameview.pack();
		frameview.addWindowListener(winlis);
		frameview.setVisible(true);
	}

	@Override
	public JerboaModelerEditorParent newWindow(JerboaModelerEditor jme) {
		return this;
	}

	@Override
	public void close() {
		frameview.setVisible(false);
		frameview.dispose();
	}

	@Override
	public JMEPreferences getPreferences() {
		return preferences;
	}


	public JerboaModelerStudioWithInference getModelerLeft() {
		return modelerLeft;
	}
	public JerboaModelerStudioWithInference getModelerRight() {
		return modelerRight;
	}
	
	public IJerboaModelerViewer getViewer() {
		return jmvLeft;
	}
	public JerboaModelerEditor getEditor() {
		return jme;
	}

	protected void show() {
		frameview.setVisible(true);
		frameview.setVisible(true);
	}
	
	
	// bon c moche, je melange model et vue+controleur
	protected class AllAssocDarts extends JPanel {
		private static final long serialVersionUID = -339719005937738710L;


		protected class AssocDart extends JPanel {
			private static final long serialVersionUID = 490674705478627206L;
			private JerboaDart left;
			private JerboaDart right;
			
			private JButton del; 
			private JButton sel;
			
			
			public AssocDart(JerboaDart l, JerboaDart r) {
				setBorder(BorderFactory.createLineBorder(Color.black));
				this.left = l;
				this.right = r;
				del = new JButton("Delete");
				sel = new JButton("Select");
				
				sel.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						List<JerboaDart> ll = new ArrayList<>();
						ll.add(left);
						jmvLeft.addDartSelection(ll);
						
						List<JerboaDart> lr = new ArrayList<>();
						lr.add(right);
						jmvRight.addDartSelection(lr);
						
					}
				});
				
				del.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						AllAssocDarts.this.removeAssoc(AssocDart.this);
					}
				});
				
				JLabel label = new JLabel("" + left.getID() + " =====>>> " + right.getID());
				this.add(label);
				this.add(sel);
				this.add(del);
			}
			
			
		}// end assoc dart
	
		private JPanel topbar;
		private JLabel lassocsize;
		private JButton fixAssoc;
		private JButton guessAssoc;
		private JButton selAssoc;
		private JButton deselAssoc;
		private JButton vertAssoc;
		private JButton faceAssoc;
		private JButton orbAssoc;
		private JButton clearAssoc;
		private int assocsize;
		private JScrollPane scrollpane;
		private Box list;
		
		public AllAssocDarts() {
			topbar = new JPanel();
			lassocsize = new JLabel("0");
			fixAssoc = new JButton("Add assoc");
			guessAssoc = new JButton("Guess by geometry");
			selAssoc = new JButton("Select all assocs");
			deselAssoc = new JButton("Deselect all assocs");
			vertAssoc = new JButton("Assoc vertex"); // <1,2,3>
			faceAssoc = new JButton("Assoc face"); // 
			orbAssoc = new JButton("Assoc orbit");
			clearAssoc = new JButton("Clear assoc");
			
			
			topbar.add(new JLabel("Size: ")); topbar.add(lassocsize);
			topbar.add(fixAssoc);
			//topbar.add(guessAssoc);
			topbar.add(selAssoc);
			topbar.add(deselAssoc);
			topbar.add(vertAssoc);
			topbar.add(faceAssoc);
			topbar.add(orbAssoc);
			topbar.add(clearAssoc);
			
			assocsize = 0;
			
			list = Box.createVerticalBox();
			scrollpane = new JScrollPane(list);
			
			this.setLayout(new BorderLayout());
			this.add(topbar, BorderLayout.NORTH);
			this.add(scrollpane, BorderLayout.CENTER);
			scrollpane.setPreferredSize(new Dimension(100,150));
			
			
			fixAssoc.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					List<JerboaDart> lefts = jmvLeft.getSelectedJerboaNodes();
					List<JerboaDart> rights = jmvRight.getSelectedJerboaNodes();
					
					if(lefts.size() != rights.size()) {
						JOptionPane.showMessageDialog(frameview, "Unmatched assoc size between left and right gmap", "Error: assoc!", JOptionPane.ERROR_MESSAGE);
					}
					else {
						for(int i = 0;i < lefts.size(); ++i) {
							JerboaDart l = lefts.get(i);
							JerboaDart r = rights.get(i);
							
							addAssoc(l, r);
						}
					}
				}
			});
			
			guessAssoc.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					lookUpAssocByGeometry();
				}
			});

		
			selAssoc.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Component[] childs = list.getComponents();
					List<JerboaDart> leftdarts = new ArrayList<>();
					List<JerboaDart> rightdarts = new ArrayList<>();
					for (Component component : childs) {
						AssocDart ad = (AssocDart)component;
						// addSelectAssoc(ad);
						leftdarts.add(ad.left);
						rightdarts.add(ad.right);
					}
					jmvLeft.addDartSelection(leftdarts);
					jmvRight.addDartSelection(rightdarts);
				}
				
			});
			
			deselAssoc.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Component[] childs = list.getComponents();
					List<JerboaDart> leftdarts = new ArrayList<>();
					List<JerboaDart> rightdarts = new ArrayList<>();
					for (Component component : childs) {
						AssocDart ad = (AssocDart)component;
						// deselSelectAssoc(ad);
						leftdarts.add(ad.left);
						rightdarts.add(ad.right);
					}
					jmvLeft.delDartSelection(leftdarts);
					jmvRight.delDartSelection(rightdarts);
				}
				
			});
			
			faceAssoc.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					List<JerboaDart> lefts = jmvLeft.getSelectedJerboaNodes();
					List<JerboaDart> rights = jmvRight.getSelectedJerboaNodes();
					JerboaOrbit orbFace = JerboaOrbit.orbit(0,1,3);
					
					if(lefts.size() != rights.size()) {
						JOptionPane.showMessageDialog(frameview, "Unmatched assoc size between left and right gmap", "Error: assoc!", JOptionPane.ERROR_MESSAGE);
					}
					else {
						for(int i = 0;i < lefts.size(); ++i) {
							JerboaDart l = lefts.get(i);
							JerboaDart r = rights.get(i);
					
							try {
								List<JerboaDart> leftorb = jmvLeft.getGMap().orbit(l, orbFace);
								List<JerboaDart> rightorb = jmvRight.getGMap().orbit(r, orbFace);
								if(leftorb.size() != rightorb.size()) {
									JOptionPane.showMessageDialog(frameview, "Unmatched assoc size between left and right orbits", "Error: assoc!", JOptionPane.ERROR_MESSAGE);
								}
								else {
									for(int j = 0;j < leftorb.size(); ++j) {
										JerboaDart dl = leftorb.get(j);
										JerboaDart dr = rightorb.get(j);
										
										addAssoc(dl, dr);										
									}
								}
								
								
							} catch (JerboaException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
				
			});
			
			
			vertAssoc.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					List<JerboaDart> lefts = jmvLeft.getSelectedJerboaNodes();
					List<JerboaDart> rights = jmvRight.getSelectedJerboaNodes();
					JerboaOrbit orbFace = JerboaOrbit.orbit(1,2,3);
					
					if(lefts.size() != rights.size()) {
						JOptionPane.showMessageDialog(frameview, "Unmatched assoc size between left and right gmap", "Error: assoc!", JOptionPane.ERROR_MESSAGE);
					}
					else {
						for(int i = 0;i < lefts.size(); ++i) {
							JerboaDart l = lefts.get(i);
							JerboaDart r = rights.get(i);
					
							try {
								List<JerboaDart> leftorb = jmvLeft.getGMap().orbit(l, orbFace);
								List<JerboaDart> rightorb = jmvRight.getGMap().orbit(r, orbFace);
								if(leftorb.size() != rightorb.size()) {
									JOptionPane.showMessageDialog(frameview, "Unmatched assoc size between left and right orbits", "Error: assoc!", JOptionPane.ERROR_MESSAGE);
								}
								else {
									for(int j = 0;j < leftorb.size(); ++j) {
										JerboaDart dl = leftorb.get(j);
										JerboaDart dr = rightorb.get(j);
										
										addAssoc(dl, dr);										
									}
								}
								
								
							} catch (JerboaException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
				
			});
			
			orbAssoc.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					List<JerboaDart> lefts = jmvLeft.getSelectedJerboaNodes();
					List<JerboaDart> rights = jmvRight.getSelectedJerboaNodes();
					
					JerboaOrbit specifiedOrbit = JerboaOrbit.orbit(0,1);
					String stringOrbit = JOptionPane.showInputDialog("Input orbit: ", "<0 , 1>");
					JerboaOrbitFormatter jof = new JerboaOrbitFormatter();
					try {
						specifiedOrbit = (JerboaOrbit) jof.stringToValue(stringOrbit);
					} catch (ParseException e2) {
						e2.printStackTrace();
					}
					
					if(lefts.size() != rights.size()) {
						JOptionPane.showMessageDialog(frameview, "Unmatched assoc size between left and right gmap", "Error: assoc!", JOptionPane.ERROR_MESSAGE);
					}
					else {
						for(int i = 0;i < lefts.size(); ++i) {
							JerboaDart l = lefts.get(i);
							JerboaDart r = rights.get(i);
					
							try {
								List<JerboaDart> leftorb = jmvLeft.getGMap().orbit(l, specifiedOrbit);
								List<JerboaDart> rightorb = jmvRight.getGMap().orbit(r, specifiedOrbit);
								if(leftorb.size() != rightorb.size()) {
									JOptionPane.showMessageDialog(frameview, "Unmatched assoc size between left and right orbits", "Error: assoc!", JOptionPane.ERROR_MESSAGE);
								}
								else {
									for(int j = 0;j < leftorb.size(); ++j) {
										JerboaDart dl = leftorb.get(j);
										JerboaDart dr = rightorb.get(j);
										
										addAssoc(dl, dr);										
									}
								}
								
								
							} catch (JerboaException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
				
			});
			
			clearAssoc.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					list.removeAll();
					list.repaint();
					list.invalidate();
					assocsize = list.getComponentCount();
					lassocsize.setText(""+ assocsize);
				}
			});

		}
		
		
		protected void lookUpAssocByGeometry() {
			JerboaGMap gmapLeft = jmvLeft.getGMap();
			JerboaGMap gmapRight = jmvRight.getGMap();
			
			JerboaOrbit orbSommet = JerboaOrbit.orbit(1,2,3);
			
			Set<JerboaDart> doneLeft = new TreeSet<>();
			Set<JerboaDart> doneRight = new TreeSet<>();
			
			for (JerboaDart dleft : gmapLeft) {
				if(!doneLeft.contains(dleft)) {
					doneLeft.add(dleft);
					Point3 lcur = dleft.ebd("position");
					Set<JerboaDart> rightDarts = gmapRight.stream().filter(d -> {
						// System.out.println("\tDART: " + d+ " -> isdel:" + d.isDeleted()+ " already: "+ !doneRight.contains(d));
						// System.out.println("\t\t coordinates: " + d.ebd("position"));
						if(d.ebd("position") != null)
							return !d.isDeleted() && !doneRight.contains(d) && d.ebd("position").equals(lcur);
						else
							return false;
					}).collect(Collectors.toSet());

					Set<JerboaDart> rightDartCur = rightDarts.stream().filter(d -> d.ebd("position").equals(lcur)).collect(Collectors.toSet());

					if(rightDartCur.size() == 1) {
						JerboaDart rdart = rightDartCur.iterator().next();
						doneRight.add(rdart);
						// TODO entry right
					}
					else {
						Set<JerboaDart> rightDartCurA0 = rightDartCur.stream().filter(d -> d.alpha(0).ebd("position").equals(dleft.alpha(0).ebd("position"))).collect(Collectors.toSet());
						if(rightDartCurA0.size() == 1) {
							JerboaDart rdart = rightDartCur.iterator().next();
							doneRight.add(rdart);
							// TODO entry right
						}
						else {
							Set<JerboaDart> rightDartCurA1A0 = rightDartCurA0.stream().filter(d -> d.alpha(1).alpha(0).ebd("position").equals(dleft.alpha(1).alpha(0).ebd("position"))).collect(Collectors.toSet());
							if(rightDartCurA1A0.size() == 1) {
								JerboaDart rdart = rightDartCur.iterator().next();
								doneRight.add(rdart);
								// TODO entry right
							}
							else {
								Set<JerboaDart> rightDartCurA2A1A0 = rightDartCurA1A0.stream().filter(d -> d.alpha(2).alpha(1).alpha(0).ebd("position").equals(dleft.alpha(2).alpha(1).alpha(0).ebd("position"))).collect(Collectors.toSet());
								if(rightDartCurA0.size() == 1) {
									JerboaDart rdart = rightDartCur.iterator().next();
									doneRight.add(rdart);
									// TODO entry right
								}
								else {
									Set<JerboaDart> rightDartCurA3A2A1A0 = rightDartCurA2A1A0.stream().filter(d -> d.alpha(3).alpha(2).alpha(1).alpha(0).ebd("position").equals(dleft.alpha(3).alpha(2).alpha(1).alpha(0).ebd("position"))).collect(Collectors.toSet());
									if(rightDartCurA3A2A1A0.size() == 1) {
										JerboaDart rdart = rightDartCur.iterator().next();
										doneRight.add(rdart);
										// TODO entry right
									}
									else {
										System.out.println(" NO ASSOCIATION FOUND FOR " + dleft);
									}	
								}	
							}	
						}
					}
				}
			}
			
			// System.out.println("FOUND " + reversefoundAssoc.size()+ " associations");
			/*for (Entry<JerboaDart, JerboaDart> entry : reversefoundAssoc.entrySet()) {
				addAssoc(entry.getValue(), entry.getKey());
			}*/
		}


		public void addAssoc(JerboaDart left, JerboaDart right) {
			AssocDart ad = new AssocDart(left, right);
			list.add(ad);
			assocsize = list.getComponentCount();
			lassocsize.setText(""+ assocsize);
		}
		
		protected void removeAssoc(AssocDart assocDart) {
			list.remove(assocDart);
			assocsize = list.getComponentCount();
			lassocsize.setText(""+ assocsize);
		}


		private void addSelectAssoc(AssocDart ad) {
			List<JerboaDart> lassoc = new ArrayList<>();
			lassoc.add(ad.left);
			jmvLeft.addDartSelection(lassoc);
			
			List<JerboaDart> rassoc = new ArrayList<>();
			rassoc.add(ad.right);
			jmvRight.addDartSelection(rassoc);
		}


		private void deselSelectAssoc(AssocDart ad) {
			List<JerboaDart> lassoc = new ArrayList<>();
			lassoc.add(ad.left);
			jmvLeft.delDartSelection(lassoc);
			
			List<JerboaDart> rassoc = new ArrayList<>();
			rassoc.add(ad.right);
			jmvRight.delDartSelection(rassoc);
		}
	}
	
	protected class ListDartsUI extends JPanel {
		private static final long serialVersionUID = -7452631012156313770L;


		protected class ListDartUI extends JPanel {
			private static final long serialVersionUID = 490674705478627206L;
			private JerboaDart dart;
			
			private JButton del; 
			private JButton sel;
			
			public ListDartUI(JerboaDart l) {
				this.dart = l;
				del = new JButton("Delete");
				sel = new JButton("Select");
				
				
				sel.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						List<JerboaDart> ll = new ArrayList<>();
						ll.add(dart);
						ListDartsUI.this.viewer.addDartSelection(ll);
					}
				});
				
				del.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						ListDartsUI.this.removeDart(ListDartUI.this);
					}
				});
				
				JLabel label = new JLabel("Dart: " + dart.getID());
				this.add(label);
				this.add(sel);
				this.add(del);
			}
			
			
		}// end assoc dart
	
		private JPanel topbar;
		private JLabel lassocsize;
		private JButton fixListBnt;
		private JButton addall ;
		private JButton clearall;
		private JButton selAssoc;
		private JButton deselAssoc;
		private int assocsize;
		private JScrollPane scrollpane;
		private Box list;
		private GMapViewer viewer;
		
		public ListDartsUI(GMapViewer viewer) {
			setBorder(BorderFactory.createLineBorder(Color.black));
			
			this.viewer = viewer;
			topbar = new JPanel();
			lassocsize = new JLabel("0");
			addall = new JButton("Add All");
			fixListBnt = new JButton("Fix");
			selAssoc = new JButton("Select all");
			deselAssoc = new JButton("Deselect all");
			clearall = new JButton("Clear All");
			
			topbar.add(new JLabel("Size: ")); topbar.add(lassocsize);
			topbar.add(addall);
			topbar.add(fixListBnt);
			topbar.add(selAssoc);
			topbar.add(deselAssoc);
			topbar.add(clearall);
			
			assocsize = 0;
			
			list = Box.createVerticalBox();
			scrollpane = new JScrollPane(list);
			
			this.setLayout(new BorderLayout());
			this.add(topbar, BorderLayout.NORTH);
			this.add(scrollpane, BorderLayout.CENTER);
			scrollpane.setPreferredSize(new Dimension(100,150));
			
			
			addall.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					JerboaGMap gmap = viewer.getGMap();
					for (JerboaDart jerboaDart : gmap) {
						addDart(jerboaDart);
					}
				}
				
			});
			
			fixListBnt.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					List<JerboaDart> lefts = viewer.getSelectedJerboaNodes();
						for(int i = 0;i < lefts.size(); ++i) {
							JerboaDart l = lefts.get(i);
							
							addDart(l);
						}					
				}
			});

		
			selAssoc.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Component[] childs = list.getComponents();
					List<JerboaDart> darts = new ArrayList<>();
					for (Component component : childs) {
						ListDartUI ad = (ListDartUI)component;
						// selectListDartUI(ad);
						darts.add(ad.dart);
					}
					viewer.addDartSelection(darts);
				}
				
			});
			
			deselAssoc.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Component[] childs = list.getComponents();
					List<JerboaDart> darts = new ArrayList<>();
					
					for (Component component : childs) {
						ListDartUI ad = (ListDartUI)component;
						// deselectListDartUI(ad);
						darts.add(ad.dart);
					}
					viewer.delDartSelection(darts);
				}
				
			});

			clearall.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// list.removAll();
					Component[] childs = list.getComponents();
					for (Component component : childs) {
						ListDartUI ad = (ListDartUI)component;						
						removeDart(ad);
					}
				}
				
			});

		}
		
		
		public void addDart(JerboaDart left) {
			ListDartUI ad = new ListDartUI(left);
			list.add(ad);
			assocsize = list.getComponentCount();
			lassocsize.setText(""+ assocsize);
		}
		
		protected void removeDart(ListDartUI assocDart) {
			list.remove(assocDart);
			assocsize = list.getComponentCount();
			lassocsize.setText(""+ assocsize);
		}


		private void selectListDartUI(ListDartUI ad) {
			List<JerboaDart> lassoc = new ArrayList<>();
			lassoc.add(ad.dart);
			viewer.addDartSelection(lassoc);
		}


		private void deselectListDartUI(ListDartUI ad) {
			List<JerboaDart> lassoc = new ArrayList<>();
			lassoc.add(ad.dart);
			viewer.delDartSelection(lassoc);
		}
	}
	
	protected void saveMappings() {
		JFileChooser chooser = new JFileChooser(".");
		chooser.setFileFilter(new FileNameExtensionFilter("JerboaStudio Mappings", "jsmap"));
		chooser.setSelectedFile(new File("jerboa_studio_mappings.jsmap"));
		if(chooser.showSaveDialog(frameview) == JFileChooser.APPROVE_OPTION) {
			try {
				// PrintStream ps = new PrintStream(chooser.getSelectedFile());
				PrintStream ps = new PrintStream(chooser.getSelectedFile());
				// sauvegarder les mappings
				Component[] components = assocdarts.list.getComponents();
				ps.println("M " + components.length);
				for(Component component : components) {
					AssocDart ad = (AssocDart) component;
					JerboaDart oldleft = ad.left;
					JerboaDart oldright = ad.right;
					
					ps.println(oldleft.getID() + " " + oldright.getID());	
				}
				ps.println();
				
				ps.println("L " + leftpattern.list.getComponentCount());
				for (Component component : leftpattern.list.getComponents()) {
					ListDartUI ldartui = (ListDartUI) component;
					ps.print(ldartui.dart.getID());
					ps.print(" ");
				}
				ps.println();
				
				ps.println("R " + rightpattern.list.getComponentCount());
				for (Component component : rightpattern.list.getComponents()) {
					ListDartUI ldartui = (ListDartUI) component;
					ps.print(ldartui.dart.getID());
					ps.print(" ");
				}
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected void loadMappings() {
		JFileChooser chooser = new JFileChooser(".");
		chooser.setSelectedFile(new File("jerboa_studio_mappings.jsmap"));
		chooser.setFileFilter(new FileNameExtensionFilter("JerboaStudio Mappings", "jsmap"));
		if(chooser.showOpenDialog(frameview) == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			loadMappings(file);
		}
	}


	protected void loadMappings(File file) {
		try (Scanner sc = new Scanner(file)) {
			String code = sc.next(); 
			int count = sc.nextInt();
			for(int i = 0; i < count ; ++i) {
				int l = sc.nextInt();
				int r = sc.nextInt();

				JerboaDart ldart = jmvLeft.getModeler().getGMap().getNode(l);
				JerboaDart rdart = jmvRight.getModeler().getGMap().getNode(r);

				assocdarts.addAssoc(ldart, rdart);

			}

			code = sc.next();
			count = sc.nextInt();
			for(int i = 0; i < count ; ++i) {
				int id = sc.nextInt();
				JerboaDart ldart = jmvLeft.getModeler().getGMap().getNode(id);
				leftpattern.addDart(ldart);
			}

			code = sc.next();
			count = sc.nextInt();
			for(int i = 0; i < count ; ++i) {
				int id = sc.nextInt();
				JerboaDart dart = jmvRight.getModeler().getGMap().getNode(id);
				rightpattern.addDart(dart);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
