package fr.up.xlim.ig.jerboa.studio.infer.geometry.inject;

import fr.up.xlim.ig.jerboa.ebds.factories.Color3Factory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments.VectEbdEnrichmentNode;

/**
 * Color embedding used to inject the code in the viewer.
 * 
 * @author Romain
 *
 */
public class EnrichedColor3 extends EnrichedVectEbd {

	public EnrichedColor3(int nodeID, VectEbdEnrichmentNode ceNode) {
		super(nodeID, ceNode, new Color3Factory());
	}

	@Override
	public int getEmbedding() {
		return 1;
	}
}
