package fr.up.xlim.ig.jerboa.studio.infer.parameters;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import fr.up.xlim.ig.jerboa.studio.utils.IntegerSetParser;
import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaOrbitFormatter;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.ui.UIPrefDialog;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.ui.UIPrefItem;
import up.jerboa.core.JerboaOrbit;

/**
 * 
 * Inference parameters for the folding algorithm.
 * 
 * Currently supports :
 * 		- specification of an inference orbit
 * 		- removal of loops on preserved nodes of the rule
 * 		- exclusion of loops from the orbit variables
 * 		- exclusion of the maximal dimension to obtain an isolated object
 * 		- choose between direct and reverse mapping
 * 		- call to use a graph display algorithm.
 * 
 * @author Romain
 *
 */

public class JerboaInferRuleParameters {

	@UIPrefItem(name = "InferenceOrbit :", desc = "Restrict inference to a specific orbit (leave empty to consider all possibilities).")
	public String inferenceOrbit;
	
	@UIPrefItem(name = "Prior-cut loops :", desc = "Loops to cut before running the folding algorithm, i.e., on the G-maps.")
	public String priorCutLoops;
	
	@UIPrefItem(name = "Posterior-cut loops :", desc = "Loops to cut after running the folding algorithm, i.e., on the rule. Please note that prior-cut should be preferred as posterior-cut may break the topological consistency.")
	public String posteriorCutLoops;
	
	@UIPrefItem(name = "Excluded loops :", desc = "Loops to force as explicit arcs, i.e. they will not appear in the node variables (be careful of conflicts witht the orbit type used for inference).")
	public String excludedLoops;
	
	@UIPrefItem(name = "Direct Mapping", desc = "Run direct mapping from the hook to the current node rather than reverse mapping from the node to the hook (reverse by default).")
	public boolean directMapping;
	
	@UIPrefItem(name = "Try drawing the graph", desc = "Run Fruchterman & Reingold to display the rule schemes.")
	public boolean display;
	
	public UIPrefDialog dialog;
	
	public boolean hasInferenceOrbit() {
		return !inferenceOrbit.isBlank();
	}
	
	/**
	 * Specific orbit used to restrict the inference (when empty all possibilities are considered)
	 * @return parsed string corresponding to the orbit
	 */
	public String getInferenceOrbit() {
		return inferenceOrbit;
	}

	public void setInferenceOrbit(String inferenceOrbit) {
		if(inferenceOrbit == null || inferenceOrbit.isBlank() || inferenceOrbit.contains("_"))
			this.inferenceOrbit = "";
		else
			this.inferenceOrbit = inferenceOrbit;
	}
	
	/**
	 * Specific loop dimensions to be removed before running the folding algorithm.
	 * @return string parsed as an orbit
	 */
	public String getPriorCutLoops() {
		return priorCutLoops;
	}

	public void setPriorCutLoops(String priorCutLoops) {
		if(priorCutLoops == null || priorCutLoops.isBlank() || priorCutLoops.contains("_"))
			this.priorCutLoops = "";
		else
			this.priorCutLoops = priorCutLoops;
	}
	
	/**
	 * Specific loop dimensions to be removed after running the folding algorithm.
	 * @return string parsed as an orbit
	 */
	public String getPosteriorCutLoops() {
		return posteriorCutLoops;
	}

	public void setPosteriorCutLoops(String posteriorCutLoops) {
		if(posteriorCutLoops == null || posteriorCutLoops.isBlank() || posteriorCutLoops.contains("_"))
			this.posteriorCutLoops = "";
		else
			this.posteriorCutLoops = posteriorCutLoops;
	}
	
	/**
	 * Specific loop dimensions to be excluded from node variables.
	 * @return string parsed as an orbit
	 */
	public String getExcludedLoops() {
		return excludedLoops;
	}

	public void setExcludedLoops(String excludedLoops) {
		if(excludedLoops == null || excludedLoops.isBlank() || excludedLoops.contains("_"))
			this.excludedLoops = "";
		else
			this.excludedLoops = excludedLoops;
	}

	/**
	 * Which loops are deleted from the preserved nodes ?
	 * @return parsed string that contains the dimensions
	 */
	// public String getRemovedLoops()

	
	/**
	 * Is the exclusion of loops forced in the inference ?
	 * @return true if loops are excluded
	 */
	// public boolean getExcludeLoops()
	

	/**
	 * Is the maximal dimension force be open (i.e., object is considered isolated) ?
	 * @return true if the dimension is open
	 */
	// public boolean getOpenMaxDim()
	
	/**
	 * Is the mapping direct (hook -&gt; node) or reverse (node -&gt; hook) ?
	 * @return true if the mapping is direct
	 */
	public boolean getDirectMapping() {
		return directMapping;
	}

	public void setDirectMapping(boolean directMapping) {
		this.directMapping = directMapping;
	}

	/**
	 * Is the graph drawing algorithm used ?
	 * @return true if the graph are not randomly displayed
	 */
	public boolean getDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	public UIPrefDialog getDialog() {
		return dialog;
	}

	public JerboaInferRuleParameters() {
		this.inferenceOrbit = "";
		this.priorCutLoops = "";
		this.priorCutLoops = "";
		this.excludedLoops = "";
		this.directMapping = false;
		this.display = true;
		this.dialog = new UIPrefDialog(this, "inferenceparams");
	}
	
	/**
	 * Convert the string corresponding to the inference orbit to an actual JerboaOrbit.
	 * @return specificOrbit : the converted JerboaOrbit.
	 */
	public JerboaOrbit convertInferenceOrbit() {
		JerboaOrbit specificOrbit = new JerboaOrbit();
		if (!inferenceOrbit.isBlank()) {
			JerboaOrbitFormatter jof = new JerboaOrbitFormatter();
			try {
				specificOrbit = (JerboaOrbit) jof.stringToValue(inferenceOrbit);
				// System.err.println("Infering with orbit :" + specificOrbit);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return specificOrbit;	
	}
	
	/**
	 * Parse an orbit to extract the set of dimensions
	 * @return set of dimensions.
	 */
	@SuppressWarnings("unchecked")
	public Set<Integer> parseOrbit(String loops) {
		HashSet<Integer> loopsToRemove = new HashSet<>();
		if (loops!=null && !loops.isBlank()) {
			IntegerSetParser isp = new IntegerSetParser();
			try {
				loopsToRemove = (HashSet<Integer>) isp.stringToValue(loops);
				// System.err.println("Removing Loops:" + loops);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return loopsToRemove;
	}
	
	public Set<Integer> parsePriorCut() {return parseOrbit(priorCutLoops);}
	public Set<Integer> parsePosteriorCut() {return parseOrbit(posteriorCutLoops);}
	public Set<Integer> parseExcluded() {return parseOrbit(excludedLoops);}
}
