package fr.up.xlim.ig.jerboa.studio.display.algorithms;

import java.util.Random;

import fr.up.xlim.ig.jerboa.studio.display.JMSDisplayable;
import fr.up.xlim.ig.jerboa.studio.display.graph.JMSDisplayDart;
import fr.up.xlim.ig.jerboa.studio.display.graph.JMSDisplayGraph;

/**
 * Randomly choose position for the nodes. Can be used for initialisation of
 * other graph display algorithm.
 * 
 * @author Romain
 *
 */
public class JMSRandomDisplay implements JMSDisplayable {
	final private Random random = new Random();

	private JMSDisplayGraph dGraph;

	/**
	 * @param dGraph
	 */
	public JMSRandomDisplay(JMSDisplayGraph dGraph) {
		this.dGraph = dGraph;
	}

	/**
	 * @return dGraph
	 */
	public JMSDisplayGraph getdGraph() {
		return dGraph;
	}

	@Override
	public void computeDisplay() {
		
		// 2/sqrt(2)* 70 ~ 100
		
		int side = (int) (100 * Math.sqrt(dGraph.size()));
		
		for (JMSDisplayDart ddart : dGraph.getDarts()) {
			int posX = random.nextInt(2*side);
			int posY = random.nextInt(side);
			ddart.setPosition(posX, posY);
		}
	}

}
