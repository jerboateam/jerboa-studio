package fr.up.xlim.ig.jerboa.studio.infer.geometry.poi;

import fr.up.xlim.ig.jerboa.ebds.Color3;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import up.jerboa.core.JerboaDart;
import up.jerboa.exception.JerboaException;

/**
 * Dart value for points of interests.
 * 
 * @author romain
 *
 */
public class ValuePoI<Embedding> implements PointOfInterest<Embedding> {

	private JMENode node;
	private JerboaDart assocDart;
	private String ebdName;
	
	public ValuePoI(JMENode node, JerboaDart assocDart, String ebd) {
		this.node = node;
		this.assocDart = assocDart;
		this.ebdName = ebd;
	}
	
	@Override
	public JMENode poiNode() {
		return node;
	}

	@Override
	public Embedding computeValue(JerboaDart d) throws JerboaException {
		return assocDart.<Embedding>ebd(this.ebdName);
	}

	@Override
	public String exportToCode(String name) {
		StringBuilder sb = new StringBuilder();
		sb.append("return new ");
		sb.append(toString());
		sb.append(";");
		
		return sb.toString();
	}
	
	@Override
	public String toString() {
		Embedding ebdg = assocDart.<Embedding>ebd(this.ebdName);

		if (this.ebdName.equals("position")) {
			StringBuilder sb = new StringBuilder();
			sb.append("Point3(");
			boolean first = true;
			for (int i = 0; i < 3; i++) {
				if (first)
					first = false;
				else
					sb.append(", ");
				sb.append(((Point3) ebdg).getFromDim(i));
			}
			sb.append(")");
			return sb.toString();
		}

		else if (this.ebdName.equals("color")) {
			StringBuilder sb = new StringBuilder();
			sb.append("Color3(");
			boolean first = true;
			for (int i = 0; i < 4; i++) {
				if (first)
					first = false;
				else
					sb.append("f, ");
				sb.append(((Color3) ebdg).getFromDim(i));
			}
			sb.append("f)");
			return sb.toString();
		}

		else {
			return "";
		}
	}

}
