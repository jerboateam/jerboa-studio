package fr.up.xlim.ig.jerboa.ebds.factories;

import java.util.List;

import com.google.ortools.linearsolver.MPVariable;

import fr.up.xlim.ig.jerboa.ebds.Color3;
import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import up.jerboa.core.JerboaDart;

/**
 * Factory for the color embeddings
 * 
 * @author Romain
 *
 */
public class Color3Factory implements VectEbdAbstractFactory {

	/**
	 * Get the name of Color3 for embedding computations.
	 * 
	 * @return name of Color3
	 */
	@Override
	public String getEbdName() {
		return "color";
	}

	/**
	 * Get the class name of Color3.
	 * 
	 * @return class name of Color3
	 */
	@Override
	public String getEbdClass() {
		return "Color3";
	}

	/**
	 * Build an object of type Color3 with default value.
	 * 
	 * @return Color3 default object
	 */
	public Color3 createEbdDefaultValue(){ return new Color3();}
	
	/**
	 * Build an object of type Color3 with from an array of variables.
	 * 
	 * @param translation: OrTools variables for the translation
	 * @return Color3 object
	 */
	public Color3 createEbdOrToolsVariable(MPVariable[] translation){
		return new Color3(
			(float) translation[0].solutionValue(),
			(float) translation[1].solutionValue(),
			(float) translation[2].solutionValue(),
			(float) translation[3].solutionValue());
	}

	/**
	 * Build an object of type Color3 as a copy of another Color3 object .
	 * 
	 * @param embedding: Color3 variables for the copy
	 * @return Color3 object
	 */
	@Override
	public Color3 createEbdCopy(VectorialEbd embedding) {
		if (embedding instanceof Color3)
			return new Color3((Color3) embedding);
		else
			return new Color3();
	}

	/**
	 * Build an object of type Color3 as a random Color3 object.
	 * 
	 * @return Color3 object
	 */
	@Override
	public Color3 createEbdRandom() {
		return Color3.randomColor();
	}
	
	/**
	 * Build an object of type Color3 as the middle of Color3 values of a list of darts.
	 * 
	 * @param darts : list of darts to compute the middle from.
	 * @return instance of the embedding
	 */
	@Override
	public VectorialEbd createEbdMiddle(List<JerboaDart> darts) {
		return Color3.middle(darts, "color");
	}
	
}

