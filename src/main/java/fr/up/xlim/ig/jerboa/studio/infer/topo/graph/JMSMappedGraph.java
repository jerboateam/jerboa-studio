package fr.up.xlim.ig.jerboa.studio.infer.topo.graph;

import java.util.HashMap;

import fr.up.xlim.ig.jerboa.studio.graph.JMSSideMark;
import up.jerboa.core.JerboaDart;

/**
 * Extension of TopoGraph to encode mapping from the left and right instances.
 * 
 * @author romain
 *
 */
public class JMSMappedGraph extends JMSTopoGraph {

	private HashMap<JMSTopoDart, JerboaDart> mappingL;
	private HashMap<JMSTopoDart, JerboaDart> mappingR;

	public JMSMappedGraph(int minDim, int maxDim) {
		super(minDim, maxDim);
		this.mappingL = new HashMap<JMSTopoDart, JerboaDart>();
		this.mappingR = new HashMap<JMSTopoDart, JerboaDart>();
	}

	/**
	 * getter for the mapping from the folded darts to the dart from the left
	 * instance
	 * 
	 * @return mappingL
	 */
	public HashMap<JMSTopoDart, JerboaDart> getMappingL() {
		return mappingL;
	}

	/**
	 * 
	 * getter for the mapping from the folded darts to the dart from the right
	 * instance
	 * 
	 * @return mappingR
	 */
	public HashMap<JMSTopoDart, JerboaDart> getMappingR() {
		return mappingR;
	}

	/**
	 * Create a new dart in the underlying graph and adds to the mapping.
	 * 
	 * This method should be the standard method for adding new darts to the
	 * graph when the mapping is needed.
	 * 
	 * @param d    : the corresponding dart (in the G-map)
	 * @param ID   : the ID of the JMSTopodart to create
	 * @param mark : JMSSideMark (i.e. left or right)
	 * @return the newly created dart.
	 */
	public JMSTopoDart createDart(JerboaDart d, int ID, JMSSideMark mark) {
		JMSTopoDart dart = super.createDart(ID, mark);
		if (mark == JMSSideMark.Left) {
			mappingL.put(dart, d);
		} else if (mark == JMSSideMark.Right) {
			mappingR.put(dart, d);
		}
		return dart;
	}

}
