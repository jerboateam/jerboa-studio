package fr.up.xlim.ig.jerboa.studio.display.algorithms;

import java.util.Collections;
import java.util.HashMap;

import fr.up.xlim.ig.jerboa.studio.display.JMSDisplayable;
import fr.up.xlim.ig.jerboa.studio.display.graph.JMSDisplayDart;
import fr.up.xlim.ig.jerboa.studio.display.graph.JMSDisplayGraph;

/**
 * Implementation of Fruchterman Reingold algorithm for graph display.
 * 
 * The hard part is to set the "good" constants.
 * Still a work in progress.
 * 
 * @author Romain
 *
 */
public class JMSFruchtermanReingold implements JMSDisplayable {

	private JMSDisplayGraph dGraph;
	private double optimalEdgeLength;
	private double minAcceptablePos;
	private int maxIter;
	private double initTemp;
	private double stoppingDisp;

	/**
	 * @param dGraph
	 */
	public JMSFruchtermanReingold(JMSDisplayGraph dGraph) {
		this.dGraph = dGraph;
		this.optimalEdgeLength = 80d;
		this.minAcceptablePos = 30d;
		this.maxIter = 200;
		this.initTemp = optimalEdgeLength * (double) dGraph.size();
		this.stoppingDisp = optimalEdgeLength / 25d;
		
	}

	/**
	 * @return the dGraph
	 */
	public JMSDisplayGraph getdGraph() {
		return dGraph;
	}
	
	/**
	 * Method to use to draw the graph as a line to empirically determine the 
	 * "good" edge length.
	 */
	public void tryEdgeLength() {
		if (dGraph.getDarts().isEmpty())
			return;
				
		int nbDart = 0;
		int scale = 80;
		
		for (JMSDisplayDart dart : dGraph.getDarts()) {
			dart.setPosition(nbDart*scale, nbDart*scale);
			nbDart++;
		}
	}

	/**
	 * Compute graph display using Fruchterman Reingold algorithm.
	 */
	public void computeDisplay() {

		if (dGraph.getDarts().isEmpty())
			return;
		
		if (dGraph.size() == 1) {
			JMSDisplayDart dart = dGraph.getDarts().iterator().next();
			dart.setPosition((int) minAcceptablePos, (int) minAcceptablePos);
			return;
		}
				
		double temperature = this.initTemp;
		double maxDisp = this.stoppingDisp;
		int iter = 0;

		HashMap<JMSDisplayDart, Double> posX = new HashMap<>();
		HashMap<JMSDisplayDart, Double> posY = new HashMap<>();
		HashMap<JMSDisplayDart, Double> dispX = new HashMap<>();
		HashMap<JMSDisplayDart, Double> dispY = new HashMap<>();

		for (JMSDisplayDart dart : dGraph.getDarts()) {
			posX.put(dart, (double) dart.getX());
			posY.put(dart, (double) dart.getY());
		}

		while (iter < this.maxIter && maxDisp >= this.stoppingDisp){
			iter++;

			// repulsive forces
			for (JMSDisplayDart v : dGraph.getDarts()) {
				dispX.put(v, 0d);
				dispY.put(v, 0d);

				for (JMSDisplayDart u : dGraph.getDarts()) {
					if (!u.equals(v)) {
						double deltaX = posX.get(v) - posX.get(u);
						double deltaY = posY.get(v) - posY.get(u);
						double normDelta = norm(deltaX, deltaY);
						dispX.compute(v, (key, value) -> value
								+ (deltaX / normDelta) * forceR(normDelta));
						dispY.compute(v, (key, value) -> value
								+ (deltaY / normDelta) * forceR(normDelta));
					}
				}
			}
			
			// attractive force
			for (JMSDisplayDart v : dGraph.getDarts()) {
				for (JMSDisplayDart u : v.getNeighboursUpToLoops()) {
					double deltaX = posX.get(v) - posX.get(u);
					double deltaY = posY.get(v) - posY.get(u);
					double normDelta = norm(deltaX, deltaY);
					dispX.compute(v, (key, value) -> value
							- (deltaX / normDelta) * forceA(normDelta));
					dispY.compute(v, (key, value) -> value
							- (deltaY / normDelta) * forceA(normDelta));
					dispX.compute(u, (key, value) -> value
							+ (deltaX / normDelta) * forceA(normDelta));
					dispY.compute(u, (key, value) -> value
							+ (deltaY / normDelta) * forceA(normDelta));
				}
			}

			// update positions
			maxDisp = 0d;
			for (JMSDisplayDart v : dGraph.getDarts()) {
				double displacementX = Math.signum(dispX.get(v))
						* Math.min(Math.abs(dispX.get(v)), temperature);
				double displacementY = Math.signum(dispY.get(v))
						* Math.min(Math.abs(dispY.get(v)), temperature);
				maxDisp = Math.max(maxDisp, norm(displacementX,displacementY));
				posX.compute(v, (key, value) -> value + displacementX);
				posY.compute(v, (key, value) -> value + displacementY);
			}

			// translate to have min = minAcceptablePos
			double minX = Collections.min(posX.values());
			for (JMSDisplayDart v : dGraph.getDarts()) {
				posX.compute(v, (key, value) -> value - minX + this.minAcceptablePos);		
			}
			double minY = Collections.min(posY.values());
			for (JMSDisplayDart v : dGraph.getDarts()) {
				posY.compute(v, (key, value) -> value - minY + this.minAcceptablePos);			
			}

			// cool
			temperature = cool(temperature);
		}
		
		System.err.println("Stopped display algorithm after " + iter + " iterations. The last maximal displacement was " + maxDisp + ".");
		
		for (JMSDisplayDart v : dGraph.getDarts()) {
			v.setPosition((int) Math.round(posX.get(v)),
					(int) Math.round(posY.get(v)));
		}
		
	}

	private double force(double num, double denom) {
		return num * num / denom;
	}

	private double forceA(double x) {
		return force(x, this.optimalEdgeLength);
	}

	private double forceR(double x) {
		return force(this.optimalEdgeLength, x);
	}

	private double norm(double x, double y) {
		return Math.max(Math.sqrt(x * x + y * y), 0.005);
	}
	
	private double cool(double temperature) {
		return 0.95 * temperature;
	}
}
