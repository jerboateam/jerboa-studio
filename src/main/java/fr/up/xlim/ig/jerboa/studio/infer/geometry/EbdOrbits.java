package fr.up.xlim.ig.jerboa.studio.infer.geometry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import fr.up.xlim.ig.jerboa.studio.orbitgenerator.JerboaOrbitGenerator;
import up.jerboa.core.JerboaOrbit;

/**
 * Used to computed the orbits relative to an embedding for the geometric
 * inference
 *  
 * @author Romain and Hakim
 *
 */
public class EbdOrbits {

	/**
	 * Return equivalence class representative from the relation induced by the 
	 * ebdOrbit.
	 * 
	 * @param dim : maximal dimension to consider
	 * @param ebdOrbit : orbit used to quotient the set of all orbits
	 * @return the orbit representatives.
	 */
	private static Collection<JerboaOrbit> getOrbits(int dim, JerboaOrbit ebdOrbit){
		HashSet<JerboaOrbit> res = new HashSet<JerboaOrbit>();
		
		for (JerboaOrbit orbit : new JerboaOrbitGenerator(dim)) {
			res.add(minInEquivClass(orbit, ebdOrbit));
		}
		
		return res;
	}
	
	/**
	 * Return the minimal orbit (ordered by length + lex) equivalent to the orbit
	 * parameter. The equivalence relation is derived from ebdOrbit.
	 * 
	 * ATTENTION: dimensions in orbits are supposed to be ordered.
	 * 
	 * @param orbit : orbit for which we are building the minimal orbit
	 * @param ebdOrbit : orbit used to determine the equivalence relation
	 * @return : minimal orbit in the equivalence class
	 */
	private static JerboaOrbit minInEquivClass(JerboaOrbit orbit, JerboaOrbit ebdOrbit){
		int dim = 0;
		int maxDim = orbit.getMaxDim();
		ArrayList<Integer> orbitDim = new ArrayList<Integer>();
		while (dim <= maxDim) {
			if (!orbit.contains(dim)) {
				dim++;
				continue;
			}
			int minConsecutive = dim;
			int maxConsecutive = dim;
			boolean intersectMin = ebdOrbit.contains(minConsecutive);
			boolean intertect = intersectMin;
			while (orbit.contains(maxConsecutive)) {
				if (ebdOrbit.contains(maxConsecutive))
					intertect = true;
				maxConsecutive++;
			}
			dim = maxConsecutive;
			boolean intersectMax = ebdOrbit.contains(maxConsecutive-1);
			if (!intertect || !intersectMin || !intersectMax) {
				for (int d=minConsecutive; d<maxConsecutive; d++)
					orbitDim.add(d);
			}
		}
		return new JerboaOrbit(orbitDim);
	}
	
	/**
	 * @return all orbits
	 */
	public static Collection<JerboaOrbit>  getAllOrbits(int dim) {
		return JerboaOrbitGenerator.getAllOrbits(dim);
	}

	/**
	 * Return equivalence class representative from the relation induced by the 
	 * position embedding.
	 * 
	 * Position orbit is &lt;1,2,3&gt;.
	 * 
	 * @param dim : maximal dimension to consider
	 * @return the position orbit representatives.
	 */
	public static Collection<JerboaOrbit> getPosOrbits(int dim) {
		return getOrbits(dim, new JerboaOrbit(1,2,3));
	}

	/**
	 * Return equivalence class representative from the relation induced by the 
	 * color embedding.
	 * 
	 * Color orbit is &lt;0,1&gt;.
	 * 
	 * @param dim : maximal dimension to consider
	 * @return the color orbit representatives.
	 */
	public static Collection<JerboaOrbit> getColOrbits(int dim) {
		return getOrbits(dim, new JerboaOrbit(0,1));
	}
	
	public static void main(String[] args) {
		
		
		System.out.println("All orbits");
		for (JerboaOrbit orbit : getAllOrbits(3)) {
			System.out.println(orbit);
		}
		System.out.println();
		System.out.println("Position orbits");
		for (JerboaOrbit orbit : getPosOrbits(3)) {
			System.out.println(orbit);
		}
		System.out.println();
		System.out.println("Color orbits");
		for (JerboaOrbit orbit : getColOrbits(3)) {
			System.out.println(orbit);
		}
	}

}