package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

import java.util.List;

import fr.up.xlim.ig.jerboa.ebds.Point3;
import up.jerboa.core.util.Pair;

public interface LPsolver {
	public Pair<List<Double>,Point3> guessCoefs(List<Point3> pts, Point3 d) throws LPSolverException;
	public Pair<List<Double>,Point3> guessCoefsSystem(List<Pair<List<Point3>, Point3>> equationSystem) throws LPSolverException;
}
