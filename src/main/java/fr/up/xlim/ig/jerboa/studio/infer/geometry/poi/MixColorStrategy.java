package fr.up.xlim.ig.jerboa.studio.infer.geometry.poi;

import java.util.List;
import java.util.stream.Collectors;

import fr.up.xlim.ig.jerboa.ebds.factories.Color3Factory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.EbdOrbits;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.system.LinearItem;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;

/**
 * Barycentric approach for colors
 * 
 * @author romain
 *
 */
public class MixColorStrategy implements PoIStrategy {

	@Override
	public List<LinearItem> getItems(JMENode node) {
		return EbdOrbits.getColOrbits(3).stream().map(o -> {
			return new LinearItem(0d, new MiddlePoI(o, node, new Color3Factory()));
		}).collect(Collectors.toList());
	}

}
