package fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import fr.up.xlim.ig.jerboa.ebds.factories.VectEbdAbstractFactory;
import fr.up.xlim.ig.jerboa.ebds.factories.VectEbdFactory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.inject.EnrichedVectEbd;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.PoIStrategy;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers.LPSolverException;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.system.LinearItem;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENodeExpression;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.rule.JerboaRowPattern;
import up.jerboa.core.util.Pair;
import up.jerboa.exception.JerboaException;

/**
 * Used to store and compute the system to infer the missing embedding
 * of a node.
 * 
 * Assumes vectorial representation of the embedding.
 * 
 * @author Romain and Hakim
 *
 */
public abstract class VectEbdEnrichmentNode {
	protected VectEbdEnrichmentRule enrichRule;
	private JMENode node;
	private boolean built;
	private List<LinearItem> abstractSystem;
	private ArrayList<Pair<List<VectorialEbd>, VectorialEbd>> concreteSystem;
	protected boolean solved;
	private VectorialEbd translation;
	protected boolean systemIsUnsolvable;

	public VectEbdEnrichmentNode(VectEbdEnrichmentRule enrichRule, JMENode node) {
		this.enrichRule = enrichRule;
		this.node = node;
		this.built = false;
		this.abstractSystem = new ArrayList<>();
		this.concreteSystem = new ArrayList<>();
		this.solved = false;
		this.translation = null;
		this.systemIsUnsolvable = true;
	}
	
	protected abstract String debug(Pair<List<Double>, VectorialEbd> result);
	protected abstract String getEbdExpression(Pair<List<Double>, VectorialEbd> result, double threshold);
	protected abstract String getDefaultExpresion();

	public JMENode getNode() {
		return node;
	}
	
	/**
	 * @return whether the system has been solved
	 */
	public boolean isSolved() {
		return solved;
	}

	/**
	 * Inherent translation of the operation
	 * 
	 * @return translation vector as a Color3
	 */
	public VectorialEbd getTranslation() {
		return translation;
	}

	/**
	 * System with the coefficients and the PoIs
	 * 
	 * @return list of item containing the coefficients and the PoIs
	 */
	public List<LinearItem> getAbstractSystem() {
		return abstractSystem;
	}
	
	/**
	 * @return true if no solution has been found and a default/random value should be used.
	 */
	public boolean systemIsUnsolvable() {
		return systemIsUnsolvable;
	}

	public void buildSystem() {
		if (built)
			return;

		int index = 0;
		List<JerboaDart> assocDarts = enrichRule.getExtRule().getMappingR()
				.get(node);
		PoIStrategy poiStrat = enrichRule.getPoiStrat();
		List<JMENode> refNodes = enrichRule.getEmbeddingRefNodes();
		Map<JMENode, List<JerboaDart>> mappingL = enrichRule.getExtRule()
				.getMappingL();
		abstractSystem = refNodes.stream()
				.flatMap(refNode -> poiStrat.getItems(refNode).stream())
				.collect(Collectors.toList());

		for (JerboaDart d : assocDarts) {
			final int currIdx = index;
			VectorialEbd targetValue = d.ebd(enrichRule.getEbdinfo().getName());
			List<VectorialEbd> ebdValues = abstractSystem.stream().map(ref -> {
				JerboaDart refDart = mappingL.get(ref.getPoi().poiNode())
						.get(currIdx);
				try {
					return (VectorialEbd) ref.getPoi().computeValue(refDart);
				} catch (JerboaException e) {
					e.printStackTrace();
					return null;
				}
			}).filter(Objects::nonNull).collect(Collectors.toList());

			String input = IntStream.range(0, ebdValues.size()).mapToObj(i -> {
				return "a" + i + " * " + ebdValues.get(i);
			}).collect(Collectors.joining(" + "));

			/*
			System.err.println("EQ (new) for <d" + currIdx + "; "
					+ node.getName() + "> : " + input + " = " + targetValue);
			*/		

			this.concreteSystem.add(new Pair<List<VectorialEbd>, VectorialEbd>(ebdValues, targetValue));
			index++;
		}

		built = true;
	}
	
	public void solveSystem() {
		if (isSolved())
			return;
		String expr = "";
		systemIsUnsolvable = true;
		try {
			Pair<List<Double>, VectorialEbd> result = enrichRule.getSolver()
					.guessCoefsSystem(concreteSystem);
			this.translation = result.r();
		
			if (VectEbdEnrichmentRule.debug)
				System.out.println("node: " + node + " -> " + debug(result));
			
			expr = getEbdExpression(result, enrichRule.getThreshold());
			
		} catch (LPSolverException e) {
			expr = getDefaultExpresion();
		}
		
		node.addExplicitExpression(new JMENodeExpression(node, enrichRule.getEbdinfo(), expr));

		solved = true;
		systemIsUnsolvable = false;
	}
	
	public Object compute(EnrichedVectEbd enrichEmbedding,
			JerboaRowPattern leftfilter) throws JerboaException {
		VectorialEbd res;
		VectEbdAbstractFactory ebdFactory = enrichEmbedding.getEbdFactory();

		if (systemIsUnsolvable) {
			res = VectEbdFactory.getEbdRandom(ebdFactory);
		}

		else {
			res = VectEbdFactory.getEbdCopy(ebdFactory, getTranslation());

			for (LinearItem item : getAbstractSystem()) {
				if (Math.abs(item.getCoef()) > enrichRule.getThreshold()) {
					JerboaDart dart = leftfilter
							.get(item.getPoi().poiNode().getID());
					VectorialEbd p = (VectorialEbd) item.getPoi()
							.computeValue(dart);
					p.scaleVect(item.getCoef());
					res.addVect(p);
				}
			}
			res.clampVect();
		}
		return res;
	}
	
}
