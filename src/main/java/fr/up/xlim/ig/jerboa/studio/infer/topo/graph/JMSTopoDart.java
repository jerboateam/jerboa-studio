package fr.up.xlim.ig.jerboa.studio.infer.topo.graph;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.up.xlim.ig.jerboa.studio.graph.JMSDart;
import fr.up.xlim.ig.jerboa.studio.graph.JMSSideMark;

/**
 * 
 * Darts in the graphs structure used to build foldings.
 * 
 * @author romain
 */
public class JMSTopoDart implements JMSDart<Integer>, Comparable<JMSTopoDart> {

	protected HashMap<Integer, JMSTopoDart> neighbours;
	protected int ID;
	protected JMSTopoGraph graph;
	protected JMSSideMark mark;

	/**
	 * The constructor adds the dart to the graph.
	 * 
	 * @param graph
	 * @param ID
	 * @param mark
	 */
	public JMSTopoDart(JMSTopoGraph graph, int ID, JMSSideMark mark) {
		this.ID = ID;
		this.mark = mark;
		this.graph = graph;
		this.graph.addDart(this);
		this.neighbours = new HashMap<Integer, JMSTopoDart>();
	}

	/**
	 * The constructor adds the dart to the graph.
	 * 
	 * @param graph
	 * @param ID
	 */
	public JMSTopoDart(JMSTopoGraph graph, int ID) {
		this(graph, ID, JMSSideMark.Left);
	}

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public JMSTopoGraph getGraph() {
		return graph;
	}

	public JMSSideMark getMark() {
		return mark;
	}

	public void setMark(JMSSideMark mark) {
		this.mark = mark;
	}

	public boolean isInLeftGraph() {
		return mark == JMSSideMark.Left;
	}
	
	public boolean isInRightGraph() {
		return mark == JMSSideMark.Right;
	}

	@Override
	public void addArc(JMSDart<Integer> dart, Integer label) {
		if (!(dart instanceof JMSTopoDart))
			return;
		neighbours.put(label, (JMSTopoDart) dart);
	}

	@Override
	public boolean removeArc(Integer label) {
		return neighbours.remove(label) != null;
	}

	@Override
	public boolean removeArc(JMSDart<Integer> dart, Integer label) {
		return neighbours.remove(label, (JMSTopoDart) dart);
	}

	@Override
	public boolean hasNeighbour(Integer dim) {
		return neighbours.containsKey(dim);
	}

	@Override
	public JMSTopoDart getNeighbour(Integer dim) {
		return neighbours.get(dim);
	}

	@Override
	public int hashCode() {
		return ID;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JMSTopoDart other = (JMSTopoDart) obj;
		if (ID != other.ID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{Node" + ID);
		sb.append("\tm=" + mark);
		for (int dim : neighbours.keySet())
			sb.append("\td=" + dim + ":" + getNeighbour(dim).getID());
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Check if the dart is free for a given dimension.
	 * 
	 * @param label : dimension
	 * @return boolean
	 */
	public boolean isFree(Integer label) {
		return !hasNeighbour(label);
	}

	/**
	 * Check if the dart has a loop for a given dimension.
	 * 
	 * @param label : dimension
	 * @return boolean
	 */
	public boolean isLoop(Integer label) {
		return (hasNeighbour(label) && getNeighbour(label) == this);
	}

	/**
	 * Get the set of incident dimensions.
	 * 
	 * @return integer set.
	 */
	public Set<Integer> getIncidentDims() {
		return neighbours.keySet();
	}

	/**
	 * Build a copy of the neighbourhood of the node.
	 * 
	 * @return Map (key=dimension, value=adjacentNode) describing the
	 *         neighbourhood.
	 */
	public Map<Integer, JMSTopoDart> getNeighbourhood() {
		return new HashMap<>(neighbours);
	}

	/**
	 * Get the dimensions of incident loops.
	 * 
	 * @return integer set.
	 */
	public Set<Integer> getLoops() {
		HashSet<Integer> loops = new HashSet<Integer>();
		for (int dim : getIncidentDims()) {
			if (getNeighbour(dim).getID() == ID)
				loops.add(dim);
		}
		return loops;
	}
	
	/**
	 * Build the neighbourhood of the node but exclude the loops labelled by
	 * some dimensions
	 * 
	 * @param excludedLoops : loop dimensions to be excluded.
	 * @return Map (key=dimension, value=adjacentNode) describing the
	 *         neighbourhood.
	 */
	public Map<Integer, JMSTopoDart> getNeighbourhoodUpToLoops(Collection<Integer> excludedLoops) {
		HashMap<Integer, JMSTopoDart> neighboursUpTo = new HashMap<>();
		for (int dim : getIncidentDims()) {
			if (!(excludedLoops.contains(dim)) || getNeighbour(dim).getID() != ID)
				neighboursUpTo.put(dim, getNeighbour(dim));
		}
		return neighboursUpTo;
	}
	
	/**
	 * Build the neighbourhood of the node but exclude the maximal dimension if it is a loop.
	 * 
	 * @return Map (key=dimension, value=adjacentNode) describing the
	 *         neighbourhood.
	 */
	public Map<Integer, JMSTopoDart> getNeighbourhoodUpToOpenedMaxDim() {
		HashMap<Integer, JMSTopoDart> neighboursUpTo = new HashMap<>();
		for (int dim : getIncidentDims()) {
			if (dim != this.getGraph().getMaxDim() || getNeighbour(dim).getID() != ID)
				neighboursUpTo.put(dim, getNeighbour(dim));
		}
		return neighboursUpTo;
	}

	/**
	 * Compare using IDs
	 */
	@Override
	public int compareTo(JMSTopoDart d) {
		return this.ID - d.getID();
	}
	
}
