package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

import java.util.List;

import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import up.jerboa.core.util.Pair;

/**
 * Abstract solver for vectorial embedding
 * 
 * @author Romain
 *
 */
public interface VectEbdSolver {
	public Pair<List<Double>,VectorialEbd> guessCoefsSystem(List<Pair<List<VectorialEbd>, VectorialEbd>> equationSystem) throws LPSolverException;
}
