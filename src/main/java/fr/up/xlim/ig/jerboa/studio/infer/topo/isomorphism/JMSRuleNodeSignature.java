package fr.up.xlim.ig.jerboa.studio.infer.topo.isomorphism;

import java.util.HashSet;

import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleNode;
import up.jerboa.core.JerboaOrbit;

/**
 * Node signatures (see "Efficient Methods to Perform Isomorphism Testing of
 * Labeled Graph" Hsieh et al. 2006)
 *
 * @author romain
 *
 */
public class JMSRuleNodeSignature {

	private JerboaOrbit dartOrbit;
	private HashSet<Integer> loops;
	private HashSet<Integer> arcs;

	public JMSRuleNodeSignature(JMSRuleNode node) {
		dartOrbit = node.getOrbit();
		loops = new HashSet<Integer>();
		arcs = new HashSet<Integer>();
		for (int dim : node.getGraph().getDims()) {
			if (node.hasNeighbour(dim)) {
				if (node.getNeighbour(dim).equals(node))
					loops.add(dim);
				else
					arcs.add(dim);
			}
		}
	}

	/**
	 * Dimensions of the explicit incident arcs that are not loops.
	 * 
	 * @return HashSet
	 */
	public HashSet<Integer> getArcs() {
		return arcs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arcs == null) ? 0 : arcs.hashCode());
		result = prime * result
				+ ((dartOrbit == null) ? 0 : dartOrbit.hashCode());
		result = prime * result + ((loops == null) ? 0 : loops.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		JMSRuleNodeSignature other = (JMSRuleNodeSignature) obj;

		if (dartOrbit == null) {
			if (other.dartOrbit != null)
				return false;
		} else if (!dartOrbit.equals(other.dartOrbit))
			return false;

		if (arcs == null) {
			if (other.arcs != null)
				return false;
		} else if (!arcs.equals(other.arcs))
			return false;

		if (loops == null) {
			if (other.loops != null)
				return false;
		} else if (!loops.equals(other.loops))
			return false;

		return true;
	}

}
