package fr.up.xlim.ig.jerboa.studio.infer.geometry.poi;

import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import fr.up.xlim.ig.jerboa.ebds.factories.VectEbdAbstractFactory;
import fr.up.xlim.ig.jerboa.ebds.factories.VectEbdFactory;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.exception.JerboaException;

/**
 * Orbit middle for points of interests.
 * 
 * @author romain
 *
 */
public class MiddlePoI implements PointOfInterest<VectorialEbd> {

	private JerboaOrbit orbit;
	private JMENode node;
	private VectEbdAbstractFactory ebdFactory;

	public MiddlePoI(JerboaOrbit orbit, JMENode node, VectEbdAbstractFactory ebdFactory) {
		this.orbit = orbit;
		this.node = node;
		this.ebdFactory = ebdFactory;
	}

	public JerboaOrbit getOrbit() {
		return orbit;
	}

	@Override
	public JMENode poiNode() {
		return node;
	}

	@Override
	public VectorialEbd computeValue(JerboaDart d) throws JerboaException {
		return VectEbdFactory.getEbdMiddle(ebdFactory, d.getOwner().orbit(d, orbit));
	}

	@Override
	public String exportToCode(String name) {
		StringBuilder sb = new StringBuilder();
		sb.append(VectEbdFactory.getEbdClass(ebdFactory));
		sb.append(" ");
		sb.append(name);
		sb.append(" = ");
		sb.append(VectEbdFactory.getEbdClass(ebdFactory));
		sb.append("::middle(");
		sb.append(toString());
		sb.append(");\n");

		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<");
		boolean first = true;
		for (Integer dim : orbit) {
			if (first)
				first = false;
			else
				sb.append(", ");
			if (dim == -1)
				sb.append("_");
			else
				sb.append(dim);
		}
		sb.append(">_");
		sb.append(VectEbdFactory.getEbdName(ebdFactory));
		sb.append("(");;
		sb.append(node.getName());
		sb.append(")");

		return sb.toString();
	}
	
	
}
