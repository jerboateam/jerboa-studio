package fr.up.xlim.ig.jerboa.studio.display.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.up.xlim.ig.jerboa.studio.graph.JMSDart;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;

/**
 * Darts in the graphs structure used to compute rule display.
 * 
 * @author Romain
 *
 */
public class JMSDisplayDart implements JMSDart<Integer> {

	protected HashMap<Integer, JMSDisplayDart> neighbours;
	private JMENode displayedNode;

	/**
	 * The constructor adds the dart to the graph.
	 * 
	 * @param graph : display graph
	 * @param displayedNode : node
	 */
	public JMSDisplayDart(JMSDisplayGraph graph, JMENode displayedNode) {
		this.displayedNode = displayedNode;
		graph.addDart(this);
		this.neighbours = new HashMap<Integer, JMSDisplayDart>();
	}

	public int getID() {
		return displayedNode.getID();
	}

	public int getX() {
		return displayedNode.getX();
	}

	public int getY() {
		return displayedNode.getY();
	}

	public void setPosition(int x, int y) {
		displayedNode.setPosition(x, y);
	}

	@Override
	public void addArc(JMSDart<Integer> dart, Integer label) {
		if (!(dart instanceof JMSDisplayDart))
			return;
		neighbours.put(label, (JMSDisplayDart) dart);
	}

	@Override
	public boolean removeArc(Integer label) {
		return neighbours.remove(label) != null;
	}

	@Override
	public boolean removeArc(JMSDart<Integer> dart, Integer label) {
		return neighbours.remove(label, (JMSDisplayDart) dart);
	}

	@Override
	public boolean hasNeighbour(Integer dim) {
		return neighbours.containsKey(dim);
	}

	@Override
	public JMSDisplayDart getNeighbour(Integer dim) {
		return neighbours.get(dim);
	}

	@Override
	public int hashCode() {
		return getID();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JMSDisplayDart other = (JMSDisplayDart) obj;
		if (getID() != other.getID())
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{Node" + getID());
		for (int dim : neighbours.keySet())
			sb.append("\td=" + dim + ":" + getNeighbour(dim).getID());
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Check if the dart is free for a given dimension.
	 * 
	 * @param label : dimension
	 * @return boolean
	 */
	public boolean isFree(Integer label) {
		return !hasNeighbour(label);
	}

	/**
	 * Check if the dart has a loop for a given dimension.
	 * 
	 * @param label : dimension
	 * @return boolean
	 */
	public boolean isLoop(Integer label) {
		return (hasNeighbour(label) && getNeighbour(label) == this);
	}

	/**
	 * Get the set of incident dimensions.
	 * 
	 * @return integer set.
	 */
	public Set<Integer> getIncidentDims() {
		return neighbours.keySet();
	}

	/**
	 * Build a copy of the neighbourhood of the node.
	 * 
	 * @return Map (key=dimension, value=adjacentNode) describing the
	 *         neighbourhood.
	 */
	public Map<Integer, JMSDisplayDart> getNeighbourhood() {
		return new HashMap<>(neighbours);
	}

	/**
	 * Build a copy of the set of neighbours of the node.
	 * 
	 * @return Set containing the neighbours.
	 */
	public Set<JMSDisplayDart> getNeighbours() {
		return new HashSet<JMSDisplayDart>(neighbours.values());
	}

	/**
	 * Build the neighbourhood of the node but exclude the loops.
	 * 
	 * @return Map (key=dimension, value=adjacentNode) describing the
	 *         neighbourhood.
	 */
	public Map<Integer, JMSDisplayDart> getNeighbourhoodUpToLoops() {
		HashMap<Integer, JMSDisplayDart> neighboursUpTo = new HashMap<>();
		for (int dim : getIncidentDims()) {
			if (getNeighbour(dim).getID() != this.getID())
				neighboursUpTo.put(dim, getNeighbour(dim));
		}
		return neighboursUpTo;
	}

	/**
	 * Build the set of neighbours of the node but exclude the loops.
	 * 
	 * @return Set containing the neighbours.
	 */
	public Set<JMSDisplayDart> getNeighboursUpToLoops() {
		HashSet<JMSDisplayDart> neighboursUpTo = new HashSet<>();
		for (int dim : getIncidentDims()) {
			if (getNeighbour(dim).getID() != this.getID())
				neighboursUpTo.add(getNeighbour(dim));
		}
		return neighboursUpTo;
	}

}
