package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

/**
 * Abstract factory for the vectorial embedding solvers.
 * 
 * @author Romain
 *
 */
public interface VectEbdSolverAbstractFactory {

	/**
	 * Creates an instance of the vectorial embedding solver
	 * 
	 * @return solver
	 */
	public VectEbdSolver createVectEbdSolver();
}
