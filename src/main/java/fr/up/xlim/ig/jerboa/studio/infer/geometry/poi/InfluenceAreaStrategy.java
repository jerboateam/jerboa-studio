package fr.up.xlim.ig.jerboa.studio.infer.geometry.poi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.up.xlim.ig.jerboa.studio.infer.geometry.EbdOrbits;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.system.LinearItem;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import up.jerboa.core.JerboaOrbit;

/**
 * 
 * Area of Influence strategy
 * 
 * @author romain
 *
 */
public class InfluenceAreaStrategy implements PoIStrategy {
	
	// TODO: get the values from somewhere
	JerboaOrbit posOrbit = new JerboaOrbit(1,2,3);
	int pathLength = 4;

	@Override
	public List<LinearItem> getItems(JMENode node) {
		
		return EbdOrbits.getAllOrbits(3).stream().flatMap(o -> {
			ArrayList<List<Integer>> paths =
					buildPaths(o, posOrbit, pathLength);
			Stream<LinearItem> stream = paths.stream().map(p -> { 
				return new LinearItem(0d, new InfluenceAreaPoI(o, p, node));			
			});
			return stream;
		}).collect(Collectors.toList());
	}
	
	private ArrayList<List<Integer>> buildPaths(
			JerboaOrbit suppOrbit,
			JerboaOrbit ebdOrbit,
			int maxLength)
	{
		ArrayList<List<Integer>> paths = new ArrayList<List<Integer>>();
		/*
		 * TODO: generate all relevant paths, given the embedding orbit, the 
		 * support orbit and the maximal path length.
		 */
		
		for (int pos=0; pos<maxLength; pos++) {
			int[] path = new int[maxLength];
			for (int dim=0; dim<3; dim++) {
				path[pos] = dim;
			}
		}
		
		
		return paths;
	}
	
}
