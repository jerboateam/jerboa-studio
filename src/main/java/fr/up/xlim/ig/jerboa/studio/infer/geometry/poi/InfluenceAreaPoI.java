package fr.up.xlim.ig.jerboa.studio.infer.geometry.poi;

import java.util.List;

import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.exception.JerboaException;

/**
 * Influence area for points of interests.
 * 
 * @author romain
 *
 */
public class InfluenceAreaPoI implements PointOfInterest<Point3> {
	
	private JerboaOrbit supportOrbit;
	private List<Integer> path;
	private JMENode node;
	
	public InfluenceAreaPoI(JerboaOrbit supportOrbit, List<Integer> path,
			JMENode node) {
		this.supportOrbit = supportOrbit;
		this.path = path;
		this.node = node;
	}

	public JerboaOrbit getSupportOrbit() {
		return supportOrbit;
	}

	/**
	 * A path is represented as a sequence of dimensions.
	 * 
	 * @return path as a List of Integers
	 */
	public List<Integer> getPath() {
		return path;
	}

	@Override
	public JMENode poiNode() {
		return node;
	}

	@Override
	public Point3 computeValue(JerboaDart d) throws JerboaException {
		Point3 point = new Point3();
		int n = 0;
		for(JerboaDart dart : d.getOwner().orbit(d, supportOrbit)){
		   JerboaDart target = dart;
		   for (int dim : path)
			   target = target.alpha(dim);
			point.addVect(target.ebd(0));
			n++;
		}
		point.scaleVect(1.0/n);
		return point;
	}

	@Override
	public String exportToCode(String name) {
		StringBuilder sb = new StringBuilder();
		sb.append("Point3 ");
		sb.append(name);
		sb.append(" = new Point3();\n");
		sb.append("int n");
		sb.append(name);
		sb.append(" = 0;\n");
		sb.append("for (JerboaDart d : <");
		boolean first = true;
		for (int dim : supportOrbit) {
			if (first)
				first = false;
			else
				sb.append(", ");
			if (dim == -1)
				sb.append("_");
			else
				sb.append(dim);
		}
		sb.append("(");;
		sb.append(node.getName());
		sb.append(")){\n\t");
		sb.append(name);
		sb.append(".add(d");
		for (int arc : path) {
			sb.append("@").append(arc);
		}
		sb.append(".position);\n");
		sb.append("n");
		sb.append(name);
		sb.append(" = ");
		sb.append("n");
		sb.append(name);
		sb.append(" +1;\n");
		sb.append("}");
		
		return sb.toString();
	}

}
