package fr.up.xlim.ig.jerboa.studio.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Aux functions for hash-based collections.
 * 
 * @author Romain
 *
 */
public class HashCollectionsAuxFunctions {

	public static Random random = new Random();

	/**
	 * Get a random element from a Set&lt;E&gt; collection. The element is not
	 * removed from the set.
	 * 
	 * &lt;E&gt; : type of objects in the set.
	 * 
	 * @param set       : set to get an element from.
	 * @return : element from the set.
	 */
	public static <E> E getRandomSetElement(Set<E> set) {
		return set.stream().skip(random.nextInt(set.size())).findFirst()
				.orElse(null);
	}

	/**
	 * Remove an element from a set which is the value corresponding to a key of
	 * a map. If the set only contained this element, the key is removed from
	 * the map.
	 * 
	 * &lt;K&gt; : type of objects used as keys. &lt;V&gt; : type of objects
	 * stored in the value sets.
	 * 
	 * @param map   : map having sets of &lt;V&gt; as values.
	 * @param key   : key of the set to remove an element from.
	 * @param value : value to be removed.
	 */
	public static <K, V> void removeFromSetMap(HashMap<K, Set<V>> map, K key,
			V value) {
		map.get(key).remove(value);
		map.computeIfPresent(key, (k, v) -> !v.isEmpty() ? v : null);
	}

	/**
	 * Add an element to a set which is the value corresponding to a key of a
	 * map. If the key was not present in the map, a value set is created and
	 * initialised with the value.
	 * 
	 * &lt;K&gt; : type of objects used as keys. &lt;V&gt; : type of objects
	 * stored in the value sets.
	 * 
	 * @param map   : map having sets of &lt;V&gt; as values.
	 * @param key   : key of the set to add an element to.
	 * @param value : value to be added.
	 */
	public static <K, V> void addToSetMap(HashMap<K, Set<V>> map, K key,
			V value) {
		map.computeIfAbsent(key, k -> new HashSet<V>()).add(value);
	}

	/**
	 * Determine whether the set mapped to a key contains an element.
	 * 
	 * &lt;K&gt; : type of objects used as keys. &lt;V&gt; : type of objects
	 * stored in the value sets.
	 * 
	 * @param map   : map having sets of &lt;V&gt; as values.
	 * @param key   : key of the set to check if it contains an element.
	 * @param value : value to be checked.
	 * @return true if the pair set mapped to the key contains the value.
	 */
	public static <K, V> boolean containsSetMap(HashMap<K, Set<V>> map, K key,
			V value) {
		return map.getOrDefault(key, new HashSet<V>()).contains(value);
	}

	/**
	 * Determine whether a mapping is surjective (i.e. all values are distinct).
	 * 
	 * @param mapping : Map
	 * 
	 * @return true if the mapping is surjective.
	 */
	@SuppressWarnings("rawtypes")
	public static boolean mappingIsOneToOne(Map mapping) {
		return mapping.size() == mapping.values().stream().distinct().count();
	}

}
