package fr.up.xlim.ig.jerboa.studio.infer.geometry.inject;

import fr.up.xlim.ig.jerboa.ebds.factories.Point3Factory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments.VectEbdEnrichmentNode;

/**
 * Position embedding used to inject the code in the viewer.
 * 
 * @author Romain and Hakim
 *
 */
public class EnrichedPoint3 extends EnrichedVectEbd {

	public EnrichedPoint3(int nodeID, VectEbdEnrichmentNode peNode) {
		super(nodeID, peNode, new Point3Factory());
	}

	@Override
	public int getEmbedding() {
		return 0;
	}
}
