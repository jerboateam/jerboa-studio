package fr.up.xlim.ig.jerboa.studio.display;

/**
 * 
 * @author Romain
 *
 */
public interface JMSDisplayable {
	
	void computeDisplay();

}
