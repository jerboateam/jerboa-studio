package fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.PointOfInterest;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.system.LinearItem;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import up.jerboa.core.util.Pair;

/**
 * Used to store and compute the system to infer the missing embedding
 * of a node.
 * 
 * Assumes vectorial representation of the embedding.
 * 
 * @author Romain and Hakim
 *
 */
public class PositionEnrichmentNode extends VectEbdEnrichmentNode {

	
	public PositionEnrichmentNode(VectEbdEnrichmentRule positionEnrichmentRule,
			JMENode node) {
		super(positionEnrichmentRule, node);
	}

	@Override
	protected String debug(Pair<List<Double>, VectorialEbd> result) {
		List<Double> coefs = result.l();
		Point3 translation = (Point3) result.r();
		
		String expr = IntStream.range(0, coefs.size())
				.mapToObj(i -> " " + coefs.get(i) + " * "
						+ getAbstractSystem().get(i).getPoi().toString())
				.collect(Collectors.joining(" +"));
		expr += " + " + translation;
		return expr;
		
	}

	@Override
	@SuppressWarnings("unchecked")
	protected String getEbdExpression(Pair<List<Double>, VectorialEbd> result, double threshold) {
		List<Double> coefs = result.l();
		Point3 translation = (Point3) result.r();
		
		StringBuilder allbody = new StringBuilder(
				"Point3 res = new Point3(");
		allbody.append(translation.getX()).append(",")
				.append(translation.getY()).append(",")
				.append(translation.getZ()).append(");\n");

		allbody.append(IntStream.range(0, coefs.size()).mapToObj(i -> {
			LinearItem item = getAbstractSystem().get(i);
			double coef = coefs.get(i);
			item.setCoef(coef);
			PointOfInterest<Point3> poi = (PointOfInterest<Point3>) item.getPoi();
			if (Math.abs(coef) > threshold) {
				StringBuilder sb = new StringBuilder();
				String name = "p"+i;
				sb.append(poi.exportToCode(name));
				sb.append(name).append(".scaleVect(").append(coef).append(");\n");
				sb.append("res.addVect(").append(name).append(");\n");
				return sb.toString();
			} else
				return "";
		}).collect(Collectors.joining()));
		allbody.append("res.clampVect();\n");
		allbody.append("return res;");
		
		return allbody.toString();
	}

	@Override
	protected String getDefaultExpresion() {
		return " // Error!! \n return new Point3();";
	}

}
