package fr.up.xlim.ig.jerboa.ebds.factories;

import java.util.List;

import com.google.ortools.linearsolver.MPVariable;

import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import up.jerboa.core.JerboaDart;

public interface VectEbdAbstractFactory {
	
	/**
	 * Get the name of the embedding
	 * 
	 * @return name of the embedding
	 */
	public String getEbdName();


	/**
	 * Get the class name of the embedding
	 * 
	 * @return class name of the embedding
	 */
	public String getEbdClass();
	
	/**
	 * Create an instance of an embedding with default values.
	 * 
	 * @return instance of the embedding
	 */
	public VectorialEbd createEbdDefaultValue();
	
	/**
	 * Create an instance of an embedding with from an OrTools variable.
	 * 
	 * @param translation: OrTools variables for the translation
	 * @return instance of the embedding
	 */
	public VectorialEbd createEbdOrToolsVariable(MPVariable[] translation);
	
	/**
	 * Create an instance of an embedding as a copy.
	 * 
	 * @param embedding: value used for copy.
	 * @return instance of the embedding
	 */
	public VectorialEbd createEbdCopy(VectorialEbd embedding);
	
	/**
	 * Create an instance of an embedding with random values.
	 * 
	 * @return instance of the embedding
	 */
	public VectorialEbd createEbdRandom();
	
	/**
	 * Create an instance of an embedding as the middle of embedding values of a list of darts.
	 * 
	 * @param darts : list of darts to compute the middle from.
	 * @return instance of the embedding
	 */
	public VectorialEbd createEbdMiddle(List<JerboaDart> darts);
}
