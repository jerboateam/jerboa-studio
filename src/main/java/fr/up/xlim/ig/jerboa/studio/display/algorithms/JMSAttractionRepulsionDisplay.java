package fr.up.xlim.ig.jerboa.studio.display.algorithms;

import java.util.Collections;
import java.util.HashMap;

import fr.up.xlim.ig.jerboa.studio.display.JMSDisplayable;
import fr.up.xlim.ig.jerboa.studio.display.JMSVector2D;
import fr.up.xlim.ig.jerboa.studio.display.graph.JMSDisplayDart;
import fr.up.xlim.ig.jerboa.studio.display.graph.JMSDisplayGraph;

/**
 * Attraction Repulsion algorithm for graph display.
 * 
 * see : M. Jacomy, T. Venturini, S. Heymann, and M. Bastian, 
 * ‘ForceAtlas2, a Continuous Graph Layout Algorithm for Handy Network
 * Visualization Designed for the Gephi Software’, PLOS ONE, vol. 9, no. 6,
 * p. e98679, Jun. 2014, doi: 10.1371/journal.pone.0098679.
 * 
 * Remarks 1: 
 * - distances are less dependent on densities for large (a-r), 
 * 		and less dependent on path lengths for small (a).
 * - it seems that the constant should be updated depending on a and r
 * 
 * Remarks 2:
 * - (2,1) ~ Fruchterman Reingold: results look good (springConstant=80)
 * - (1,1) used in Force Atlas: result doesn't look too good (springConstant=20)
 * - (3,2) : possibly best results, longer times to converge (springConstant=500)
 * 
 * @author Romain
 *
 */
public class JMSAttractionRepulsionDisplay implements JMSDisplayable {

	private JMSDisplayGraph dGraph;
	private int attraction;
	private int repulsion;
	private double springConstant;
	private double minAcceptablePos;
	private int maxIter;
	private double initTemp;
	private double stoppingDisp;

	/**
	 * 
	 * @param dGraph : display graph
	 * @param attraction : attraction strength
	 * @param repulsion : repulsion strength
	 * @param springConstant  : spring constant
	 */
	public JMSAttractionRepulsionDisplay(JMSDisplayGraph dGraph, int attraction, int repulsion, double springConstant) {
		this.dGraph = dGraph;
		this.attraction = attraction;
		this.repulsion = repulsion;
		this.springConstant = springConstant;
		this.minAcceptablePos = 30d;
		this.maxIter = 200;
		this.initTemp = springConstant * (double) dGraph.size();
		this.stoppingDisp = 3d;
		
	}

	/**
	 * @return the dGraph
	 */
	public JMSDisplayGraph getdGraph() {
		return dGraph;
	}
	
	/**
	 * Compute graph display using attraction-repulsion algorithm.
	 */
	public void computeDisplay() {

		if (dGraph.getDarts().isEmpty())
			return;
		
		if (dGraph.size() == 1) {
			JMSDisplayDart dart = dGraph.getDarts().iterator().next();
			dart.setPosition((int) minAcceptablePos, (int) minAcceptablePos);
			return;
		}
				
		double temperature = this.initTemp;
		double sqdStoppingDisp = this.stoppingDisp * this.stoppingDisp;
		double sqdMaxDisp = sqdStoppingDisp;
		int iter = 0;

		HashMap<JMSDisplayDart, Double> posX = new HashMap<>();
		HashMap<JMSDisplayDart, Double> posY = new HashMap<>();
		HashMap<JMSDisplayDart, Double> dispX = new HashMap<>();
		HashMap<JMSDisplayDart, Double> dispY = new HashMap<>();
		
		HashMap<JMSDisplayDart,HashMap<JMSDisplayDart, JMSVector2D>> vert2vert = new HashMap<>();

		// initialise positions
		for (JMSDisplayDart dart : dGraph.getDarts()) {
			posX.put(dart, (double) dart.getX());
			posY.put(dart, (double) dart.getY());
		}
		
		// initialise vectors
		for (JMSDisplayDart source : dGraph.getDarts()) {
			vert2vert.put(source, new HashMap<>());
			for (JMSDisplayDart target : dGraph.getDarts()) {
				if (source.getID() < target.getID()) {
					JMSVector2D source2target = new JMSVector2D(
							posX.get(source), posY.get(source),
							posX.get(target), posY.get(target));
					vert2vert.get(source).put(target, source2target);
				}
			}
		}

		while (iter < this.maxIter && sqdMaxDisp >= sqdStoppingDisp){
			iter++;

			// repulsive forces
			for (JMSDisplayDart v : dGraph.getDarts()) {
				dispX.put(v, 0d);
				dispY.put(v, 0d);

				for (JMSDisplayDart u : dGraph.getDarts()) {
					if (u.getID() < v.getID()) {
						JMSVector2D u2v = vert2vert.get(u).get(v);
						dispX.compute(v, (key, value) -> value
								+ (u2v.normalisedDx()) * forceR(u2v.norm()));
						dispY.compute(v, (key, value) -> value
								+ (u2v.normalisedDy()) * forceR(u2v.norm()));
					} else if (v.getID() < u.getID()) {
						JMSVector2D v2u = vert2vert.get(v).get(u);
						dispX.compute(v, (key, value) -> value
								- (v2u.normalisedDx()) * forceR(v2u.norm()));
						dispY.compute(v, (key, value) -> value
								- (v2u.normalisedDy()) * forceR(v2u.norm()));
					}
				}
			}
			
			// attractive force
			for (JMSDisplayDart v : dGraph.getDarts()) {
				for (JMSDisplayDart u : v.getNeighboursUpToLoops()) {
					if (u.getID() < v.getID()) {
						JMSVector2D u2v = vert2vert.get(u).get(v);
						dispX.compute(v, (key, value) -> value
								- (u2v.normalisedDx()) * forceA(u2v.norm()));
						dispY.compute(v, (key, value) -> value
								- (u2v.normalisedDy()) * forceA(u2v.norm()));
						dispX.compute(u, (key, value) -> value
								+ (u2v.normalisedDx()) * forceA(u2v.norm()));
						dispY.compute(u, (key, value) -> value
								+ (u2v.normalisedDy()) * forceA(u2v.norm()));
					}
				}
			}

			// update positions
			sqdMaxDisp = 0d;
			for (JMSDisplayDart v : dGraph.getDarts()) {
				double displacementX = Math.signum(dispX.get(v))
						* Math.min(Math.abs(dispX.get(v)), temperature);
				double displacementY = Math.signum(dispY.get(v))
						* Math.min(Math.abs(dispY.get(v)), temperature);
				double sqdDisp = displacementX*displacementX + displacementY*displacementY;
				sqdMaxDisp = Math.max(sqdMaxDisp, sqdDisp);
				posX.compute(v, (key, value) -> value + displacementX);
				posY.compute(v, (key, value) -> value + displacementY);
			}

			// translate to have min = minAcceptablePos
			double minX = Collections.min(posX.values());
			for (JMSDisplayDart v : dGraph.getDarts()) {
				posX.compute(v, (key, value) -> value - minX + this.minAcceptablePos);		
			}
			double minY = Collections.min(posY.values());
			for (JMSDisplayDart v : dGraph.getDarts()) {
				posY.compute(v, (key, value) -> value - minY + this.minAcceptablePos);			
			}
			
			// update vectors
			for (JMSDisplayDart source : dGraph.getDarts()) {
				for (JMSDisplayDart target : dGraph.getDarts()) {
					if (source.getID() < target.getID()) {
						JMSVector2D source2target = vert2vert.get(source).get(target);
						source2target.updateSourceTarget(
								posX.get(source), posY.get(source),
								posX.get(target), posY.get(target));
					}
				}
			}

			// cool
			temperature = cool(temperature);
		}
		
		System.err.println("Stopped display algorithm after " + iter + " iterations. The last maximal displacement was " + Math.sqrt(sqdMaxDisp) + ".");
		
		for (JMSDisplayDart v : dGraph.getDarts()) {
			v.setPosition((int) Math.round(posX.get(v)),
					(int) Math.round(posY.get(v)));
		}
		
	}

	/**
	 * Attractive force of the spring, the value should be positive.
	 * 
	 * @param distance between the points used to compute the force
	 * @return value of the attractive force
	 */
	private double forceA(double distance) {
		return Math.pow(distance, this.attraction) / this.springConstant;
	}

	/**
	 * Repulsive force of the points, the value should be positive.
	 * 
	 * @param distance between the points used to compute the force
	 * @return value of the repulsive force
	 */
	private double forceR(double distance) {
		return Math.pow(this.springConstant,2) / Math.pow(distance, this.repulsion);
	}
	
	private double cool(double temperature) {
		return 0.95 * temperature;
	}
}
