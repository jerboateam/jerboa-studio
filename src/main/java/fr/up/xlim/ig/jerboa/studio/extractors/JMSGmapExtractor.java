/**
 * 
 */
package fr.up.xlim.ig.jerboa.studio.extractors;

import java.util.Map;
import java.util.Set;

import fr.up.xlim.ig.jerboa.studio.graph.JMSSideMark;
import fr.up.xlim.ig.jerboa.studio.infer.parameters.JerboaInferRuleParameters;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSFoldGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils.ConnectivityException;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils.GmapException;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoDart;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoGraph;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;

/**
 * Extract information from a Gmap to build a fold graph.
 * 
 * @author romain
 *
 */
public class JMSGmapExtractor {

	/**
	 * Extract darts from a Gmap.
	 * 
	 * Darts with negative ID are discarded. Darts in outOfRule are discarded.
	 * 
	 * @param gmap
	 * @param graph           : graph to which the darts are added
	 * @param leftRightOffset : offset for dart IDs when folding a rule
	 * @param mark            : mark for darts
	 * @param outOfRule       : darts to be discarded
	 */
	private static void extractDartsGmap(JerboaGMap gmap, JMSFoldGraph graph,
			int leftRightOffset, JMSSideMark mark, Set<JerboaDart> outOfRule) {
		for (JerboaDart d : gmap) {
			if (d.getID() >= 0 && !outOfRule.contains(d))
				graph.createDart(d, d.getID() + leftRightOffset, mark);
		}
	}

	/**
	 * Extract arcs from a Gmap. The offset is used to properly match left vs
	 * right nodes.
	 * 
	 * Darts with negative ID are discarded. Darts in outOfRule are discarded.
	 * 
	 * @param gmap
	 * @param graph           : graph to which the arcs are added
	 * @param leftRightOffset : offset for dart IDs when folding a rule
	 * @param mapping         : mapping from the LHS (key) to the RHS (value)
	 *                        darts. Only preserved nodes are provided. Used to
	 *                        extract preserved darts.
	 * @param loopsToRemove   : loops to delete on preserved nodes (might be
	 *                        empty but should not be null)
	 * @param leftOutOfRule   : darts not to consider
	 */
	private static void extractArcsGmap(JerboaGMap gmap, JMSFoldGraph graph,
			int leftRightOffset, Map<Integer, Integer> mapping,
			Set<Integer> priorCut, Set<JerboaDart> outOfRule) {
		for (JerboaDart d : gmap) {
			if (d.getID() >= 0 && !outOfRule.contains(d)) {
				int sourceID = leftRightOffset + d.getID();
				JMSTopoDart source = graph.getDart(sourceID);
				for (int dim = 0; dim <= gmap.getDimension(); dim++) {
					if (d.alpha(dim) != null && d.alpha(dim).getID() >= 0
							&& !outOfRule.contains(d.alpha(dim))) {
						int targetID = leftRightOffset + d.alpha(dim).getID();
						JMSTopoDart target = graph.getDart(targetID);

						if (source.isInLeftGraph()) {
							if (sourceID != targetID
									|| !priorCut.contains(dim)
									|| !mapping.containsKey(d.getID()))
								graph.addArc(source, target, dim);
						} else {
							if (sourceID != targetID
									|| !priorCut.contains(dim)
									|| !mapping.containsValue(d.getID()))
								graph.addArc(source, target, dim);
						}
					}
				}
			}
		}
	}

	/**
	 * Add the kappa-arcs that link darts that correspond to a preserved copy
	 * between LHS and RHS
	 * 
	 * @param rightGmap       : RHS
	 * @param graph           : graph to which the arcs are added
	 * @param leftRightOffset : offset for dart IDs when folding a rule
	 * @param mapping         : mapping from the LHS (key) to the RHS (value)
	 *                        darts. Only preserved nodes are provided. Empty as
	 *                        input
	 */
	private static void buildIdentityForKappa(JerboaGMap rightGmap,
			JMSFoldGraph graph, int leftRightOffset,
			Map<Integer, Integer> mapping) {
		for (JerboaDart d : rightGmap) {
			int gmapDartID = d.getID();
			JMSTopoDart leftSideNode = graph.getDart(gmapDartID);
			if (graph.hasDart(gmapDartID) && leftSideNode.isInLeftGraph())
				mapping.put(gmapDartID, gmapDartID);
		}
	}

	/**
	 * Add the kappa-arcs that link darts that correspond to a preserved copy
	 * between LHS and RHS
	 * 
	 * @param graph           : graph to which the arcs are added
	 * @param leftRightOffset : offset for dart IDs when folding a rule
	 * @param mapping         : mapping from the LHS (key) to the RHS (value)
	 *                        darts. Only preserved nodes are provided.
	 */
	private static void addKappaArcs(JMSFoldGraph graph, int leftRightOffset,
			Map<Integer, Integer> mapping) {
		for (int leftSideID : mapping.keySet()) {
			int rightSideID = leftRightOffset + mapping.get(leftSideID);
			if (graph.hasDart(leftSideID) && graph.hasDart(rightSideID)) {
				JMSTopoDart leftSideNode = graph.getDart(leftSideID);
				JMSTopoDart rightSideNode = graph.getDart(rightSideID);
				if (leftSideNode.isInLeftGraph()
						&& rightSideNode.isInRightGraph()) {
					graph.addArc(leftSideNode, rightSideNode,
							JMSTopoGraph.kappa);
				}
			}
		}
	}

	/**
	 * Builder of JMSFoldGraph from a rule expressed as two Gmaps.
	 * 
	 * NB @romain : we exploit the uniqueness of IDs in the Gmap and the
	 * incident arcs constraint. We also assume that a dart in the left Gmap is
	 * preserved and correspond to a dart in the right Gmap if they have the
	 * same ID.
	 * 
	 * @param left                : left Gmap
	 * @param right               : right Gmap
	 * @param inferenceParameters : inference parameters (handled by an instance
	 *                            of JerboaInferRuleParameters)
	 * @param leftOutOfRule       : left darts not to be considered as part of
	 *                            the rule
	 * @param rightOutOfRule      : right darts not to be considered as part of
	 *                            the rule
	 * 
	 * @return JMSFoldGraph graph to be used to try foldings on the rule.
	 * 
	 * @throws ConnectivityException if the graph is not connex.
	 */
	public static JMSFoldGraph buildFoldGraphFromRule(JerboaGMap left,
			JerboaGMap right, JerboaInferRuleParameters inferenceParameters,
			Map<Integer, Integer> mapping, Set<JerboaDart> leftOutOfRule,
			Set<JerboaDart> rightOutOfRule)
			throws ConnectivityException, GmapException {

		int dim = 0;

		try {
			dim = left.getDimension();
		} catch (NullPointerException lhse) {
			throw new GmapException("LHS is not defined.");
		}
		try {
			if (dim != right.getDimension()) {
				throw new GmapException(
						"LHS and RHS have different dimensions.");
			}
		} catch (NullPointerException rhse) {
			throw new GmapException("RHS is not defined.");
		}

		JMSFoldGraph graph = new JMSFoldGraph(dim, inferenceParameters);

		Set<Integer> priorCut = inferenceParameters.parsePriorCut();

		// nodes
		final int lhsOffset = 0;
		extractDartsGmap(left, graph, lhsOffset, JMSSideMark.Left,
				leftOutOfRule);
		final int leftRightOffset = graph.setAsFoldRule();
		extractDartsGmap(right, graph, leftRightOffset, JMSSideMark.Right,
				rightOutOfRule);

		// kappa arcs (build mapping if none is provided)
		if (mapping.isEmpty())
			buildIdentityForKappa(right, graph, leftRightOffset, mapping);
		addKappaArcs(graph, leftRightOffset, mapping);

		// add arcs (discard loops according to priorCut)
		extractArcsGmap(left, graph, lhsOffset, mapping, priorCut,
				leftOutOfRule);
		extractArcsGmap(right, graph, leftRightOffset, mapping, priorCut,
				rightOutOfRule);

		if (!graph.checkConnectivity())
			throw new ConnectivityException();
		return graph;
	}

}
