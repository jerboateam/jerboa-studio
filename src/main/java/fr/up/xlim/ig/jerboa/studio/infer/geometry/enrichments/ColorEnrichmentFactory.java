package fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments;

import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;

/**
 * 
 * @author Romain
 *
 */
public class ColorEnrichmentFactory
		implements EnrichmentNodeAbstractFactory {

	@Override
	public VectEbdEnrichmentNode createEnrichmentNode(
			VectEbdEnrichmentRule eRule, JMENode node) {
		return new ColorEnrichmentNode(eRule, node);
	}

}
