package fr.up.xlim.ig.jerboa.demo.serializer;

import java.util.ArrayList;
import java.util.List;

import fr.up.xlim.ig.jerboa.ebds.Normal3;

public class Face extends ArrayList<FacePart> {
	private static final long serialVersionUID = 4402090402860201215L;

	public Normal3 geonorm;
	protected OBJImporter owner;
	protected ArrayList<Edge> res;
	
	public Face(OBJImporter owner, List<FacePart> parts) {
		super(parts);
		this.owner = owner;
		calcEdges();
	}
	
	
	void calcNormal() {
		geonorm = Normal3.computeNewellMethod(get(0).edge0);
	}
	
	public Normal3 getGeoNorm() {
		return geonorm;
	}
	
	
	public List<Edge> getEdges() 
	{
			return res;
	}

	private void calcEdges() {
		res = new ArrayList<Edge>();
		final int size = size();
		for(int i = 0;i < size; i++) {
			FacePart fp0 = get(i);
			FacePart fp1 = get((i+1)%size);
			Edge e = new Edge(this,fp0.vindex, fp1.vindex,fp0.edge0,fp0.edge1);
			res.add(e);
		}
		
	
	}

	public boolean include(Edge edge) {
		return res.contains(edge);
	}
	
	public Edge search(Edge e) {
		if(res.contains(e)) {
			int idx = res.indexOf(e);
			if(idx != -1) {
				return res.get(idx);
			}
		}
		return null;
	}
	
	
	
}
