/**
 * 
 */
package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

/**
 * @author hbelhaou
 *
 */
public class LPSolverException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4725627012960588271L;

	/**
	 * 
	 */
	public LPSolverException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public LPSolverException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public LPSolverException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public LPSolverException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public LPSolverException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
