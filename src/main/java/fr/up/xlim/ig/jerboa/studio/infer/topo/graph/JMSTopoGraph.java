package fr.up.xlim.ig.jerboa.studio.infer.topo.graph;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.up.xlim.ig.jerboa.studio.graph.JMSDart;
import fr.up.xlim.ig.jerboa.studio.graph.JMSDartBasedGraph;
import fr.up.xlim.ig.jerboa.studio.graph.JMSSideMark;

/**
 * 
 * Gmap-like graphs structure (integer-labeled arcs).
 * 
 * @author romain
 */
public class JMSTopoGraph implements JMSDartBasedGraph<Integer, JMSTopoDart>,
		Iterable<JMSTopoDart> {

	public final static int kappa = -1;

	protected HashMap<Integer, JMSTopoDart> darts;
	protected int minDim;
	protected int maxDim;
	protected int nextAvailableNodeID;

	public JMSTopoGraph(int minDim, int maxDim) {
		this.darts = new HashMap<Integer, JMSTopoDart>();
		this.minDim = minDim;
		this.maxDim = maxDim;
		this.nextAvailableNodeID = 0;
	}

	public int getMinDim() {
		return minDim;
	}

	public int getMaxDim() {
		return maxDim;
	}

	public int getNextAvailableNodeID() {
		return nextAvailableNodeID;
	}

	/**
	 * Get the dart corresponding to the given ID, if exists.
	 * 
	 * @param id : dart ID
	 * @return dart or NULL.
	 */
	public JMSTopoDart getDart(int id) {
		return darts.get(id);
	}

	/**
	 * Get all dart IDs in the graph
	 * 
	 * @return Collection of dart IDs
	 */
	public Collection<Integer> getDartIDs() {
		return darts.keySet();
	}

	/**
	 * Get an ID from any existing dart.
	 * 
	 * @return dartID
	 */
	public int getAnyDartID() {
		return darts.keySet().iterator().next();
	}

	/**
	 * Create a new dart and adds it to the graph with a specific ID.
	 * 
	 * This method should be the standard method for adding new darts to the
	 * graph when a specific ID is needed.
	 * 
	 * @param ID : dart ID (is available)
	 * @return the newly created dart.
	 */
	public JMSTopoDart createDart(int ID) {
		return new JMSTopoDart(this, ID);
	}

	/**
	 * Create a new dart and adds it to the graph with a specific ID and a
	 * specific mark.
	 * 
	 * This method should be the standard method for adding new darts to the
	 * graph when specific ID and mark are needed.
	 * 
	 * @param mark : JMSSideMark (i.e. left or right)
	 * @return the newly created dart.
	 */
	public JMSTopoDart createDart(int ID, JMSSideMark mark) {
		JMSTopoDart dart = createDart(ID);
		dart.setMark(mark);
		return dart;
	}

	/**
	 * Determine if there exists a dart for a given ID.
	 * 
	 * @param ID : ID to be checked.
	 * @return true if there is such a dart.
	 */
	public boolean hasDart(int ID) {
		return darts.containsKey(ID);
	}

	/**
	 * Remove a dart from the graph (the dart is actually not deleted) but all
	 * arcs pointing towards it are.
	 * 
	 * @param ID : id of the dart to delete.
	 */
	public void removeDart(int ID) {
		JMSTopoDart dart = getDart(ID);
		for (int dim : dart.getIncidentDims()) {
			JMSTopoDart target = dart.getNeighbour(dim);
			target.removeArc(dart, dim);
		}
		darts.remove(ID);
	}

	/**
	 * Compare a collection of dart IDs to see if it matches the IDs of the
	 * graph darts.
	 * 
	 * @param dartIDs : collections of dart IDs (Integers)
	 * @return boolean
	 */
	public boolean equalsAllDartIDs(Collection<Integer> dartIDs) {
		return dartIDs.size() == darts.keySet().size()
				&& dartIDs.containsAll(darts.keySet());
	}

	@Override
	public boolean noDart() {
		return darts.size() == 0;
	}

	@Override
	public int size() {
		return darts.size();
	}

	@Override
	public Collection<JMSTopoDart> getDarts() {
		return darts.values();
	}

	@Override
	public JMSTopoDart getAnyDart() {
		return darts.get(getAnyDartID());
	}

	public boolean hasDart(JMSTopoDart dart) {
		return hasDart(dart.getID());
	}

	@Override
	public boolean labelValid(Integer dim) {
		return dim >= minDim || dim <= maxDim;
	}

	/**
	 * Add a dart to the list of darts. If the affected ID if higher than the
	 * stored next possible value, update it.
	 * 
	 * We make sure that the stored next possible value is always higher than
	 * all stored ID. We assume that the number of darts will not be too
	 * important.
	 * 
	 * This method should only be called by the dart, on its creation.
	 * 
	 * @param d : dart
	 */
	@Override
	public void addDart(JMSTopoDart d) {
		if (hasDart(d))
			d.setID(nextAvailableNodeID);
		if (d.getID() >= nextAvailableNodeID)
			nextAvailableNodeID = d.getID() + 1;
		darts.put(d.getID(), d);
	}

	@Override
	public void removeDart(JMSTopoDart d) {
		removeDart(d.getID());
	}

	@Override
	public void addArc(JMSTopoDart sourceDart, JMSTopoDart targetDart,
			Integer dim) {
		if (!labelValid(dim))
			return;
		if (sourceDart.isFree(dim) && targetDart.isFree(dim)) {
			sourceDart.addArc(targetDart, dim);
			targetDart.addArc(sourceDart, dim);
		}
	}

	@Override
	public void removeArc(JMSTopoDart dart, Integer label) {
		JMSTopoDart target = dart.getNeighbour(label);
		if (target != null) {
			target.removeArc(dart, label);
			dart.removeArc(target, label);
		}
	}

	@Override
	public boolean equalsAllDarts(Collection<JMSTopoDart> dartCollection) {
		return equalsAllDartIDs(dartCollection.stream().map(d -> d.getID())
				.collect(Collectors.toSet()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((darts == null) ? 0 : darts.hashCode());
		result = prime * result + maxDim;
		result = prime * result + minDim;
		result = prime * result + nextAvailableNodeID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JMSTopoGraph other = (JMSTopoGraph) obj;
		if (darts == null) {
			if (other.darts != null)
				return false;
		} else if (!darts.equals(other.darts))
			return false;
		if (maxDim != other.maxDim)
			return false;
		if (minDim != other.minDim)
			return false;
		if (nextAvailableNodeID != other.nextAvailableNodeID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Graph\n");
		sb.append("Nodes :\n");
		for (JMSDart<Integer> d : darts.values())
			sb.append("\n" + d);
		return sb.toString();
	}

	class JMSTopoGraphIterator implements Iterator<JMSTopoDart> {
		int pos = 0;
		Iterator<Integer> idIterator = darts.keySet().iterator();

		@Override
		public boolean hasNext() {
			return pos < size();
		}

		@Override
		public JMSTopoDart next() {
			JMSTopoDart node = darts.get(idIterator.next());
			pos++;
			return node;
		}

		@Override
		public void remove() {
			// not supported
		}

	}

	public Iterator<JMSTopoDart> iterator() {
		return new JMSTopoGraphIterator();
	}

	/**
	 * Build maximal subgraph given a set of dimensions. See the inclusion
	 * construction with the pattern functor in the article about topology.
	 * 
	 * Most of the code is reused from JMEGraph.
	 * 
	 * @param dID   : dart ID
	 * @param orbit : collection of dimensions
	 * @return Set of dart ID in the maximal subgraph.
	 */
	public Set<Integer> orbit(int dID, Collection<Integer> orbit) {

		HashSet<Integer> visited = new HashSet<Integer>();
		Stack<Integer> stack = new Stack<Integer>();

		stack.push(dID);

		while (!stack.isEmpty()) {
			Integer curID = stack.pop();
			if (!visited.contains(curID)) {
				visited.add(curID);
				for (int arcDim : getDart(curID).getIncidentDims()) {
					if (orbit.contains(arcDim)) {
						int otherNodeID = getDart(curID).getNeighbour(arcDim)
								.getID();
						if (!visited.contains(otherNodeID))
							stack.push(otherNodeID);
					}
				}
			}
		}

		return visited;
	}

	/**
	 * Check if the graph is connected
	 * 
	 * @return true if the graph is connected
	 */
	public boolean checkConnectivity() {
		if (noDart())
			return true;
		List<Integer> allDims = IntStream.rangeClosed(minDim, maxDim).boxed()
				.collect(Collectors.toList());
		return equalsAllDartIDs(orbit(getAnyDartID(), allDims));
	}
}