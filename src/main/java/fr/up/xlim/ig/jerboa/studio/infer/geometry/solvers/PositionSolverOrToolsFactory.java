package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

import com.google.ortools.Loader;
import com.google.ortools.linearsolver.MPSolver;

import fr.up.xlim.ig.jerboa.ebds.factories.Point3Factory;

/**
 * Creation of an OrTool based solver for the position embedding.
 * 
 * @author Romain
 *
 */
public class PositionSolverOrToolsFactory
		implements VectEbdSolverAbstractFactory {

	@Override
	public VectEbdSolverOrTools createVectEbdSolver() {
		Loader.loadNativeLibraries(); 
		final double infinity = MPSolver.infinity();
		return new VectEbdSolverOrTools(
				new Point3Factory(),
				3,
				new double[] { -infinity, -infinity, -infinity },
				new double[] { infinity, infinity, infinity }) {
		};
	}

}
