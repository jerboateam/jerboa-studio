/**
 * 
 */
package fr.up.xlim.ig.jerboa.studio.infer.geometry.inject;

import java.util.HashMap;
import java.util.Stack;

import up.jerboa.core.JerboaModeler;
import up.jerboa.core.rule.JerboaRowPattern;
import up.jerboa.core.rule.JerboaRuleNode;
import up.jerboa.core.util.JerboaRuleGenerated;
import up.jerboa.exception.JerboaException;

/**
 * @author Romain and Hakim
 *
 */
public class JerboaRuleGeneratedImpl extends JerboaRuleGenerated {

	private HashMap<Integer, Integer> rassoc;
	private HashMap<Integer, Integer> attnode;
	protected transient JerboaRowPattern curleftPattern;

	/**
	 * @param modeler
	 * @param name
	 * @param category
	 */
	public JerboaRuleGeneratedImpl(JerboaModeler modeler, String name,
			String category) {
		super(modeler, name, category);
		rassoc = new HashMap<>();
		attnode = new HashMap<>();
	}

	@Override
	public int reverseAssoc(int i) {
		if (rassoc.containsKey(i))
			return rassoc.get(i);
		return -1;
	}

	@Override
	public int attachedNode(int i) {
		if (attnode.containsKey(i))
			return attnode.get(i);
		return -1;
	}

	public void prepareGen() {
		// on cherche les noeuds gardes
		for (JerboaRuleNode rnode : right) {
			int rid = rnode.getID();
			for (JerboaRuleNode lnode : left) {
				String lname = lnode.getName();
				String rname = rnode.getName();
				if (lname.equals(rname)) {
					rassoc.put(rid, lnode.getID());
					if (hooks.contains(lnode))
						attnode.put(rid, lnode.getID());
				}
			}
		}

		// on doit chercher le premier hook attached au noeud
		for (JerboaRuleNode rnode : right) {
			// = right.get(cid);
			int rid = rnode.getID();
			if (!rassoc.containsKey(rid)) {
				// if(!hooks.contains(rnode)) {
				JerboaRuleNode hnode = searchAttHook(rnode);
				if (hnode != null) {
					rassoc.put(rid, hnode.getID());
					attnode.put(rid, hnode.getID());
				}
			}
		}

	}

	private JerboaRuleNode searchAttHook(JerboaRuleNode node) {
		for (JerboaRuleNode n : right) {
			n.setMark(false);
		}

		Stack<JerboaRuleNode> pile = new Stack<>();
		pile.push(node);
		while (!pile.isEmpty()) {
			JerboaRuleNode n = pile.pop();
			if (rassoc.containsKey(n.getID())) {
				Integer i = rassoc.get(n.getID());
				JerboaRuleNode lnode = left.get(i);
				if (hooks.contains(lnode))
					return lnode;
			}
			for (int d = 0; d <= modeler.getDimension(); ++d) {
				JerboaRuleNode v = n.alpha(d);
				if (v != null && v.isNotMarked()) {
					v.setMark(true);
					pile.push(v);
				}
			}

		}

		return null;
	}

	public void compile() throws JerboaException {
		computeEfficientTopoStructure();
		computeSpreadOperation();
		prepareGen();

	}

}
