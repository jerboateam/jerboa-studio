package fr.up.xlim.ig.jerboa.studio.graph;

import java.util.Collection;

/**
 * Interface for a graph (built exploiting the class {@link JMSDart}.
 * 
 * @author romain
 *
 * @param <ArcLabelType> : type of arc labels.
 * @param <DartType>     : implementation of JMSDart.
 */
public interface JMSDartBasedGraph<ArcLabelType, DartType extends JMSDart<ArcLabelType>> {

	/**
	 * Check whether the graph has a least one dart.
	 * 
	 * @return true if the graph has no dart.
	 */
	boolean noDart();

	/**
	 * Get the size of the graph (i.e. the number of darts)
	 * 
	 * @return the number of darts in the graph
	 */
	int size();

	/**
	 * Get all darts in the graph.
	 * 
	 * @return Collection of darts.
	 */
	Collection<DartType> getDarts();

	/**
	 * Get any existing dart
	 * 
	 * @return dart
	 */
	DartType getAnyDart();

	/**
	 * Determine if the graph contains a dart
	 * 
	 * @param dart : dart to be checked.
	 * @return true if there is such a dart.
	 */
	boolean hasDart(DartType dart);

	/**
	 * Check if an arc label is valid.
	 * 
	 * @param label : label to be checked
	 * @return true if the label is valid
	 */
	boolean labelValid(ArcLabelType label);

	/**
	 * Add a dart to the graph
	 * 
	 * @param dart : dart
	 */
	void addDart(DartType dart);

	/**
	 * Remove a dart from the graph (the dart is actually not deleted) but all
	 * arcs pointing towards it are.
	 * 
	 * @param dart : dart to delete.
	 */
	void removeDart(DartType dart);

	/**
	 * Add an arc to the graph given both end points and the arc label.
	 * 
	 * If one dart already has a neighbour for the given label, no arc should be
	 * added.
	 * 
	 * @param sourceDart : first end point
	 * @param targetDart : second end point
	 * @param label      : arc label
	 */
	void addArc(DartType sourceDart, DartType targetDart, ArcLabelType label);

	/**
	 * Remove any arc from a dart given a label
	 * 
	 * @param dart  : dart
	 * @param label : label
	 */
	void removeArc(DartType dart, ArcLabelType label);

	/**
	 * Compare a collection of dart to see if it matches the graph darts.
	 * 
	 * @param dartCollection : collections of darts
	 * @return boolean
	 */
	boolean equalsAllDarts(Collection<DartType> dartCollection);

}