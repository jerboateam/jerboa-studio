package fr.up.xlim.ig.jerboa.studio.infer.topo.isomorphism;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleNode;
import fr.up.xlim.ig.jerboa.studio.utils.HashCollectionsAuxFunctions;

/**
 * Build a mapping between two rules expressed as JMSRuleGraph objects.
 * 
 * Used for isomorphism computations
 * 
 * @author romain
 *
 */
public class JMSRuleIsomorphism {

	JMSRuleGraph sourceGraph;
	HashMap<JMSRuleNodeSignature, Integer> sourceGraphSignatures;
	HashMap<JMSRuleNodeSignature, Set<JMSRuleNode>> sourceGraphSignatureToNode;

	JMSRuleGraph targetGraph;
	HashMap<JMSRuleNodeSignature, Integer> targetGraphSignatures;
	HashMap<JMSRuleNodeSignature, Set<JMSRuleNode>> targetGraphSignatureToNode;

	HashMap<JMSRuleNode, JMSRuleNode> dartMapping;

	public JMSRuleIsomorphism(JMSRuleGraph sourceGraph,
			JMSRuleGraph targetGraph) {
		this.sourceGraph = sourceGraph;
		this.sourceGraphSignatures = new HashMap<>(
				sourceGraph.getSignatureOccurrences());
		this.sourceGraphSignatureToNode = new HashMap<>(
				sourceGraph.getSignatureToNode());

		this.targetGraph = targetGraph;
		this.targetGraphSignatures = new HashMap<>(
				targetGraph.getSignatureOccurrences());
		this.targetGraphSignatureToNode = new HashMap<>(
				targetGraph.getSignatureToNode());

		this.dartMapping = new HashMap<>();
	}

	/**
	 * Verification of vertex invariants (i.e. equality of graph signatures).
	 * Note that the equality of graph signatures is not necessarily enough to
	 * conclude that the graphs are isomorphic.
	 * 
	 * @return true if the vertex invariants are equal.
	 */
	public boolean checkVertexInvariants() {
		return sourceGraphSignatures.equals(targetGraphSignatures);
	}

	/**
	 * Return a mapping from the source graph to the target graph. If no mapping
	 * have be built, build it first. The mapping describes an isomorphism
	 * between the graphs.
	 * 
	 * @return hashMap describing a mapping of the nodes. NULL if the graphs are
	 *         not isomorphic.
	 */
	public HashMap<JMSRuleNode, JMSRuleNode> getIsomorphism() {
		if (dartMapping.isEmpty())
			if (!testIsomorphism()) {
				this.dartMapping = new HashMap<>();
				return null;
			}
		return dartMapping;
	}

	/**
	 * Build mapping between the two graphs.
	 * 
	 * The mapping is obtained by running joint DFS starting from nodes with the
	 * same signature. Each joint DFS is run using the smallest available class
	 * signature.
	 * 
	 * @return true if a mapping can be built.
	 */
	public boolean testIsomorphism() {
		while (sourceGraphSignatures.isEmpty()
				&& targetGraphSignatures.isEmpty()) {
			JMSRuleNodeSignature smallestClass = Collections
					.min(sourceGraphSignatures.entrySet(),
							Map.Entry.comparingByValue())
					.getKey();

			JMSRuleNode sourceGraphDart = sourceGraphSignatureToNode
					.get(smallestClass).iterator().next();

			for (JMSRuleNode targetGraphDart : targetGraphSignatureToNode
					.get(smallestClass)) {
				if (jointDFS(sourceGraphDart, targetGraphDart))
					break;
			}
			return false;
		}
		return true;
	}

	/**
	 * Run a joint DFS on two graphs from two darts (one for each graph).
	 * 
	 * @param sourceGraphDart : the reference dart in the source graph.
	 * @param targetGraphDart : the reference dart in the target graph.
	 * 
	 * @return true if the DFSs are coherent.
	 */
	private boolean jointDFS(JMSRuleNode sourceGraphDart,
			JMSRuleNode targetGraphDart) {

		HashSet<JMSRuleNode> visited = new HashSet<>();
		Stack<JMSRuleNode> stack = new Stack<>();

		HashMap<JMSRuleNode, JMSRuleNode> mapping = new HashMap<>();

		mapping.put(sourceGraphDart, targetGraphDart);
		stack.push(sourceGraphDart);

		while (!stack.isEmpty()) {
			JMSRuleNode currentDart = stack.pop();

			if (visited.contains(currentDart))
				continue;

			visited.add(currentDart);
			JMSRuleNode mappedDart = mapping.get(currentDart);

			for (int arcDim : currentDart.getSignature().getArcs()) {
				// get darts
				JMSRuleNode neighborOfCurrentDart = currentDart
						.getNeighbour(arcDim);
				JMSRuleNode neighborOfMappedDart = mappedDart
						.getNeighbour(arcDim);

				// check mapping (i.e. arcs are coherent)
				if (mapping.containsKey(neighborOfCurrentDart)) {
					JMSRuleNode mappingOfNeighbourDart = mapping
							.get(neighborOfCurrentDart);
					if (!mappingOfNeighbourDart.equals(neighborOfMappedDart))
						return false;
				}

				if (!neighborOfCurrentDart.getSignature()
						.equals(neighborOfMappedDart.getSignature()))
					return false;

				mapping.put(neighborOfCurrentDart, neighborOfMappedDart);
				stack.push(neighborOfCurrentDart);
			}

		} // end while

		if (!HashCollectionsAuxFunctions.mappingIsOneToOne(mapping))
			return false;

		updateDartMapping(mapping);
		return true;
	}

	/**
	 * Used to update the global mapping after running a joint DFS.
	 * 
	 * @param mapping : local mapping found by the joint DFS.
	 */
	private void updateDartMapping(HashMap<JMSRuleNode, JMSRuleNode> mapping) {
		for (JMSRuleNode sourceDart : mapping.keySet()) {
			JMSRuleNodeSignature sourceSignature = sourceDart.getSignature();
			sourceGraphSignatures.merge(sourceSignature, -1, Integer::sum);
			HashCollectionsAuxFunctions.removeFromSetMap(
					sourceGraphSignatureToNode, sourceSignature, sourceDart);

			JMSRuleNode mappedDart = mapping.get(sourceDart);
			JMSRuleNodeSignature targetSignature = mappedDart.getSignature();
			targetGraphSignatures.merge(targetSignature, -1, Integer::sum);
			HashCollectionsAuxFunctions.removeFromSetMap(
					targetGraphSignatureToNode, targetSignature, mappedDart);

			dartMapping.put(sourceDart, mappedDart);
		}

	}

}
