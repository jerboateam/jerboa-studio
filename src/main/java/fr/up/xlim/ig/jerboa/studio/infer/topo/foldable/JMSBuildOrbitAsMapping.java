package fr.up.xlim.ig.jerboa.studio.infer.topo.foldable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import fr.up.xlim.ig.jerboa.studio.infer.parameters.JerboaInferRuleParameters;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoDart;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoGraph;
import up.jerboa.core.JerboaOrbit;

/**
 * Built the orbit of the node by computing a dimension map (i.e. the
 * relabelling function) from the seed.
 * 
 * @author Romain
 *
 */
public class JMSBuildOrbitAsMapping {

	private JMSRuleNode ruleNode;
	private JerboaInferRuleParameters inferParams;

	public JMSBuildOrbitAsMapping(JMSRuleNode ruleNode,
			JerboaInferRuleParameters inferParams) {
		this.ruleNode = ruleNode;
		this.inferParams = inferParams;
	}

	/**
	 * Get neighbourhood based on the inference parameters (i.e., exclude more
	 * or less elements). The neighbourhood contains values that should be
	 * mapped in the node variable.
	 * 
	 * @return Map<Integer, JMSTopoDart> describing the neighbourhood that can
	 *         be use for remapping.
	 */
	private Map<Integer, JMSTopoDart> buildOrbitNeighborhood() {
		Collection<Integer> loopsExcluded = inferParams.parseExcluded();
		loopsExcluded.add(JMSTopoGraph.kappa);
		return ruleNode.getAssocDart().getNeighbourhoodUpToLoops(loopsExcluded);

	}

	/**
	 * Built the orbit of the node by computing a dimension map (i.e. the
	 * relabelling function) from the seed.
	 * 
	 * @param seed : seed used as reference
	 */
	public JerboaOrbit buildOrbitAsSeedMapping(JMSFoldSeed seed) {

		Map<Integer, JMSTopoDart> neighbourhood = buildOrbitNeighborhood();

		Set<Integer> seedOrbitDims = seed.getOrbitDims();
		HashMap<Integer, Integer> dimensionMap = new HashMap<Integer, Integer>();

		if (inferParams.getDirectMapping()) {
			for (int seedOrbitDim : seed.getOrbitDims()) {
				// we try the identity mapping first
				if (neighbourhood.containsKey(seedOrbitDim)
						&& !dimensionMap.containsValue(seedOrbitDim)
						&& checkMapping(seedOrbitDim, seedOrbitDim, seed,
								neighbourhood)) {
					dimensionMap.put(seedOrbitDim, seedOrbitDim);
					continue;
				}

				for (int mappingToDim : neighbourhood.keySet()) {
					if (!dimensionMap.containsValue(mappingToDim)
							&& !dimensionMap.containsKey(seedOrbitDim)
							&& checkMapping(seedOrbitDim, mappingToDim, seed,
									neighbourhood)) {
						dimensionMap.put(seedOrbitDim, mappingToDim);
						break;
					}
				}
			}
		}

		else {
			for (int mappingToDim : neighbourhood.keySet()) {
				// we try the identity mapping first
				if (seedOrbitDims.contains(mappingToDim)
						&& !dimensionMap.containsKey(mappingToDim)
						&& checkMapping(mappingToDim, mappingToDim, seed,
								neighbourhood)) {
					dimensionMap.put(mappingToDim, mappingToDim);
					continue;
				}

				for (int seedOrbitDim : seedOrbitDims) {
					if (!dimensionMap.containsValue(mappingToDim)
							&& !dimensionMap.containsKey(seedOrbitDim)
							&& checkMapping(seedOrbitDim, mappingToDim, seed,
									neighbourhood)) {
						dimensionMap.put(seedOrbitDim, mappingToDim);
						break;
					}
				}
			}
		}

		return new JerboaOrbit(seed.getOrbit(), dimensionMap);
	}

	/**
	 * Check if the mapping sourceDim -> targetDim is valid for the orbit.
	 * 
	 * We check that we are not trying to map to kappa arcs.
	 * 
	 * @param sourceDim     : dimension to map from
	 * @param targetDim     : dimension to map to
	 * @param seed          : seed used as reference
	 * @param neighbourhood : neighbourhood of the reference dart corresponding
	 *                      to the node.
	 * @return true if the mapping is correct
	 */
	private boolean checkMapping(int sourceDim, int targetDim, JMSFoldSeed seed,
			Map<Integer, JMSTopoDart> neighbourhood) {

		for (JMSTopoDart seedDart : seed.getOrbitDarts()) {
			if (!ruleNode.getSeedEquivalent(seedDart.getNeighbour(sourceDim))
					.equals(ruleNode.getSeedEquivalent(seedDart)
							.getNeighbour(targetDim)))
				return false;
		}
		return true;
	}

}
