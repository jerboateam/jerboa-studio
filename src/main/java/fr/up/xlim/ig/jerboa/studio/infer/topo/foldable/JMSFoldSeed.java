package fr.up.xlim.ig.jerboa.studio.infer.topo.foldable;

import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoDart;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoGraph;
import up.jerboa.core.JerboaOrbit;

/**
 * Specific JMSRuleNode that corresponds to the seed used in the folding
 * algorithm.
 * 
 * @author romain
 *
 */
public class JMSFoldSeed extends JMSRuleNode {

	/**
	 * The constructor for a seed uses a topological dart and an orbit. The set
	 * of darts associated to the node (of the rule graph) are obtained with a
	 * traversal of the folding graph for the specified orbit.
	 * 
	 * @param topoDart : reference dart for the rule node.
	 * @param orbit    : orbit associated to the rule node.
	 */
	public JMSFoldSeed(JMSTopoDart topoDart, JerboaOrbit orbit) {
		super(topoDart, orbit);
		JMSTopoGraph topoGraph = topoDart.getGraph();

		for (int seedDartID : topoGraph.orbit(topoDart.getID(),
				getOrbitDims())) {
			JMSTopoDart seedDart = topoGraph.getDart(seedDartID);
			this.seedMap.put(seedDart, seedDart);
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JMSFoldSeed [assocDart=");
		builder.append(assocDart);
		builder.append(", orbit=");
		builder.append(orbit);
		builder.append("]");
		return builder.toString();
	}

}
