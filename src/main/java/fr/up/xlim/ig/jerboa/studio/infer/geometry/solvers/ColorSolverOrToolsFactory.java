package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

import com.google.ortools.Loader;
import com.google.ortools.linearsolver.MPSolver;

import fr.up.xlim.ig.jerboa.ebds.factories.Color3Factory;

/**
 * Creation of an OrTool based solver for the color embedding.
 * 
 * @author Romain
 *
 */
public class ColorSolverOrToolsFactory
		implements VectEbdSolverAbstractFactory {

	@Override
	public VectEbdSolverOrTools createVectEbdSolver() {
		Loader.loadNativeLibraries(); 
		final double infinity = MPSolver.infinity();
		return new VectEbdSolverOrTools(
				new Color3Factory(),
				4,
				new double[] { -infinity, -infinity, -infinity, -infinity },
				new double[] { infinity, infinity, infinity, infinity }) {
		};
	}

}
