package fr.up.xlim.ig.jerboa.studio.orbitgenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import up.jerboa.core.JerboaOrbit;

/**
 * This class can be used to generate all JerboaOrbit for a given maximal
 * dimension.
 * 
 * @author romain
 *
 */
public class JerboaOrbitGenerator implements Iterable<JerboaOrbit> {

	private List<Integer> availableDims;

	public JerboaOrbitGenerator(List<Integer> availableDims) {
		this.availableDims = availableDims;
	}

	public JerboaOrbitGenerator(int maxDim) {
		this(IntStream.rangeClosed(0, maxDim).boxed()
				.collect(Collectors.toList()));
	}
	
	public static List<JerboaOrbit> getAllOrbits(int maxDim) {
		return StreamSupport
				.stream((new JerboaOrbitGenerator(maxDim)).spliterator(), false)
				.collect(Collectors.toList());
	}

	private class JerboaOrbitIterator implements Iterator<JerboaOrbit> {
		private int mask;

		private JerboaOrbitIterator() {
			this.mask = 0;
		}

		@Override
		public boolean hasNext() {
			return mask < (1 << (availableDims.size()));
		}

		@Override
		public JerboaOrbit next() {
			List<Integer> dimensions = new ArrayList<Integer>();
			for (int dimIdx = 0; dimIdx < availableDims.size(); dimIdx++) {
				if ((mask & (1 << dimIdx)) != 0) {
					dimensions.add(availableDims.get(dimIdx));
				}
			}
			mask++;
			return new JerboaOrbit(dimensions);
		}
	}

	@Override
	public Iterator<JerboaOrbit> iterator() {
		return new JerboaOrbitIterator();
	}

	public static void main(String[] args) {
		
		for (JerboaOrbit orbit : new JerboaOrbitGenerator(3)) {
			System.out.println(orbit);
		}
	}

}
