package fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import fr.up.xlim.ig.jerboa.studio.infer.JMSExtendedRule;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.PoIStrategy;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers.VectEbdSolver;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEEmbeddingInfo;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERuleAtomic;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaOrbit;

/**
 * Allows to add the missing embedding on a complete rule.
 * 
 * @author Romain
 *
 */
public class VectEbdEnrichmentRule {

	public static boolean debug = false;

	private JMSExtendedRule extRule;
	private JMEEmbeddingInfo ebdinfo;
	private PoIStrategy poiStrat;
	private boolean isEbdEnriched;
	private Map<JMENode, VectEbdEnrichmentNode> ebdEnrichment;
	private boolean ebdHasRefNodes;
	private List<JMENode> ebdRefNodes;
	private VectEbdSolver solver;
	private EnrichmentNodeAbstractFactory factory;
	private double threshold;

	public VectEbdEnrichmentRule(JMSExtendedRule extRule, VectEbdSolver solver,
			PoIStrategy poiStrat, JMEEmbeddingInfo ebdinfo, EnrichmentNodeAbstractFactory factory, double threshold) {
		this.extRule = extRule;
		this.solver = solver;
		this.ebdinfo = ebdinfo;
		this.poiStrat = poiStrat;
		this.isEbdEnriched = false;
		this.ebdEnrichment = new Hashtable<>();
		this.ebdHasRefNodes = false;
		this.ebdRefNodes = new ArrayList<>();
		this.factory = factory;
		this.threshold = threshold;
	}
	
	/**
	 * For creation rules
	 * 
	 * @param extRule :  extended rule (with embedding computations)
	 * @param ebdinfo : embedding informations (i.e., the code)	
	 * @param factory : factory to be used for the enrichment
	 */
	public VectEbdEnrichmentRule(JMSExtendedRule extRule, JMEEmbeddingInfo ebdinfo, EnrichmentNodeAbstractFactory factory) {
		this(extRule, null, null, ebdinfo, factory, 0);
	}

	/**
	 * 
	 * @return extRule : extended rule with mapping and possibly embedding
	 *         enrichment
	 */
	public JMSExtendedRule getExtRule() {
		return extRule;
	}
	
	/**
	 * @return the solver
	 */
	public VectEbdSolver getSolver() {
		return solver;
	}

	public JMEEmbeddingInfo getEbdinfo() {
		return ebdinfo;
	}

	public PoIStrategy getPoiStrat() {
		return poiStrat;
	}
	
	public double getThreshold() {
		return threshold;
	}

	public Map<JMENode, VectEbdEnrichmentNode> getEbdEnrichment() {
		return ebdEnrichment;
	}

	public boolean isEmbeddingEnriched() {
		return isEbdEnriched;
	}

	public void buildEbdEnrichment() {
		if (!isEbdEnriched)
			enrichEmbedding();
	}

	public List<JMENode> getEmbeddingRefNodes() {
		if (!ebdHasRefNodes)
			buildEmbeddingRefNodes();
		return ebdRefNodes;
	}

	/**
	 * Build the embedding enrichment for all the needing nodes of the rule.
	 */
	private void enrichEmbedding() {
		JMERuleAtomic rule = extRule.getRule();
		// Nodes that we can use for the computation in the RHS
		buildEmbeddingRefNodes();

		Map<Boolean, List<JMENode>> preserved = rule.getRight().getNodes()
				.stream().collect(Collectors.partitioningBy(
						n -> rule.getLeft().getMatchNode(n.getName()) != null));

		for (JMENode node : preserved.get(true)) {
			if (needEbdPreserved(node, preserved.get(true))) {
				enrichEmbedding(node);
			}
		}

		for (JMENode node : preserved.get(false)) {
			if (needEbdCreated(node, preserved.get(true))) {
				enrichEmbedding(node);
			}
		}
		isEbdEnriched = true;

	}

	/**
	 * Check whether a preserved node need an expression for the position
	 * embedding
	 * 
	 * @param node      : the preserved node
	 * @param preserved : the list of preserved nodes (in the JMERule)
	 * @return : true if the node needs an embedding expression, false otherwise
	 */
	private boolean needEbdPreserved(JMENode node, List<JMENode> preserved) {

		Set<JMENode> orbNodes = this.extRule.getRule().getRight().orbit(node,
				ebdinfo.getOrbit());
		boolean orbitNodeHasExpr = orbNodes.stream()
				.filter(n -> n.existExpression(ebdinfo)).count() != 0;

		if (orbitNodeHasExpr)
			return false;

		List<VectorialEbd> ebdR = new ArrayList<>();
		for (JerboaDart d : this.extRule.getMappingR().get(node))
			ebdR.add(d.ebd(this.ebdinfo.getName()));
		List<VectorialEbd> ebdL = new ArrayList<>();
		for (JerboaDart d : this.extRule.getMappingL().get(
				this.extRule.getRule().getLeft().getMatchNode(node.getName())))
			ebdL.add(d.ebd(this.ebdinfo.getName()));

		System.err.println(ebdR);
		System.err.println(ebdL);
		
		return !ebdR.equals(ebdL);
	}

	/**
	 * Check whether a created node need an expression for the position
	 * embedding
	 * 
	 * @param node      : the preserved node
	 * @param preserved : the list of preserved nodes (in the JMERule)
	 * @return : true if the node needs an embedding expression, false otherwise
	 */
	private boolean needEbdCreated(JMENode node, List<JMENode> preserved) {

		Set<JMENode> orbNodes = this.extRule.getRule().getRight().orbit(node,
				ebdinfo.getOrbit());
		boolean orbitNodeHasExpr = orbNodes.stream()
				.filter(n -> n.existExpression(ebdinfo)).count() != 0;

		if (orbitNodeHasExpr)
			return false;

		return orbNodes.stream().filter(n -> {
			return preserved.contains(n);
		}).count() == 0;
	}

	public void buildEmbeddingRefNodes() {
		JMERuleAtomic rule = extRule.getRule();
		JerboaOrbit ebdorbit = ebdinfo.getOrbit();
		this.ebdRefNodes = rule.getLeft().oneNodePerOrbit(ebdorbit)
				.stream().collect(Collectors.toList());
		this.ebdHasRefNodes = true;
	}
	
	public void enrichEmbedding(JMENode node) {
		VectEbdEnrichmentNode nodeEnrichment = EnrichmentNodeFactory.getEnrichmentNode(factory, this, node);
		nodeEnrichment.buildSystem();
		nodeEnrichment.solveSystem();
		ebdEnrichment.put(node, nodeEnrichment);
	}
	
	
	public void solveCreationRule() {
		JMERuleAtomic rule = extRule.getRule();
		Map<JMENode, List<JerboaDart>> mappingR = extRule.getMappingR();
	
		for (JMENode node : rule.getRight().getNodes()) {
			VectEbdEnrichmentNode nodeEnrichment = new ValuedEnrichmentNode(this, node, mappingR.get(node).get(0));
			ebdEnrichment.put(node, nodeEnrichment);
		}
		isEbdEnriched = true;
	}
}
