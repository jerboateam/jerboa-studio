package fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments;

import java.util.List;

import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.inject.EnrichedVectEbd;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.PointOfInterest;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.ValuePoI;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENodeExpression;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.rule.JerboaRowPattern;
import up.jerboa.core.util.Pair;
import up.jerboa.exception.JerboaException;

/**
 * Enrichment nodes with a static value.
 * 
 * Should be used for creation rules.
 * 
 * @author Romain
 *
 */
public class ValuedEnrichmentNode extends VectEbdEnrichmentNode {

	JerboaDart assocDart;
	PointOfInterest<VectorialEbd> poi;
	
	public ValuedEnrichmentNode(VectEbdEnrichmentRule enrichmentRule, JMENode node, JerboaDart assocDart) {
		super(enrichmentRule, node);
		this.assocDart = assocDart;
		this.poi = new ValuePoI<VectorialEbd>(node, assocDart, enrichmentRule.getEbdinfo().getName());		
		node.addExplicitExpression(new JMENodeExpression(node, enrichRule.getEbdinfo(), getDefaultExpresion()));
		solved = true;
		systemIsUnsolvable = false;
	}

	@Override
	protected String debug(Pair<List<Double>, VectorialEbd> result) {
		return getDefaultExpresion();
	}

	@Override
	protected String getEbdExpression(Pair<List<Double>, VectorialEbd> result, double threshold) {
		return getDefaultExpresion();
	}

	@Override
	protected String getDefaultExpresion() {
		return poi.exportToCode("");
	}
	
	@Override
	public Object compute(EnrichedVectEbd enrichEmbedding,
			JerboaRowPattern leftfilter) throws JerboaException {
		VectorialEbd res = poi.computeValue(null);
		return res;
	}

}
