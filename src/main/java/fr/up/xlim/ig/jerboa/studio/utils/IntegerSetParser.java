package fr.up.xlim.ig.jerboa.studio.utils;

import java.text.ParseException;
import java.util.HashSet;

import javax.swing.text.DefaultFormatter;


/**
 * Class to parse (back and forth) between a set of Integers and a String.
 * Inherits of the DefaultFormatter of Java in order to be used in a TextField
 * (swing or AWT) or any other visual component that relies on this mechanism.
 * 
 * @author romain
 *
 */
public class IntegerSetParser extends DefaultFormatter {
	
	
	private static final long serialVersionUID = 7449491404308381216L;

		
	public IntegerSetParser() {}
	
	@Override
	public String valueToString(Object value) throws ParseException {
		if(value == null)
			return "";
		else
			return value.toString();
	}
	
	@Override
	public Object stringToValue(String string) throws ParseException {
		HashSet<Integer> loopsToRemove = new HashSet<>();
		if (!string.isBlank()) {
			int pos=0;
			boolean foundInt = false;
			String buff="";
			
			while(pos<string.length()){
				buff="";
				foundInt = false;

				// read non integers
				while(!Character.isDigit(string.charAt(pos))){
					pos++;
					if(pos>=string.length())
						break;
				}
				
				// concatenate integers
				if(pos<string.length()) {
					while(Character.isDigit(string.charAt(pos))){
						buff = buff + string.charAt(pos);
						pos++;
						foundInt=true;
						if(pos>=string.length())
							break;
					}
						
					if(foundInt){
						loopsToRemove.add(Integer.parseInt(buff));
					}
				}
			}
		}
		return loopsToRemove;
	}
}
