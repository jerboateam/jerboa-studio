package fr.up.xlim.ig.jerboa.studio.utils;

/**
 * 
 * @author romain
 *
 */
public class Triplet<T1, T2, T3> {

	private T1 t1;
	private T2 t2;
	private T3 t3;

	public Triplet(T1 t1, T2 t2, T3 t3) {
		this.t1 = t1;
		this.t2 = t2;
		this.t3 = t3;
	}

	public T1 fst() {
		return t1;
	}

	public T2 snd() {
		return t2;
	}

	public T3 trd() {
		return t3;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<");
		sb.append(t1);
		sb.append(" ; ");
		sb.append(t2);
		sb.append(" ; ");
		sb.append(t3);
		sb.append(">");
		return sb.toString();
	}
}
