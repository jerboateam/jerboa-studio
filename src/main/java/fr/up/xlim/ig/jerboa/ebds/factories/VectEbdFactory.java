package fr.up.xlim.ig.jerboa.ebds.factories;

import java.util.List;

import com.google.ortools.linearsolver.MPVariable;

import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import up.jerboa.core.JerboaDart;

/**
 * 
 * @author Romain
 *
 */
public class VectEbdFactory {
	
	/**
	 * Get the name of the embedding
	 * 
	 * @param factory : factory that encapsulates the embedding
	 * @return name of the embedding
	 */
	public static String getEbdName(VectEbdAbstractFactory factory) {
		return factory.getEbdName();
	}
	
	/**
	 * Get the class name of the embedding
	 * 
	 * @param factory : factory that encapsulates the embedding
	 * @return class name of the embedding
	 */
	public static String getEbdClass(VectEbdAbstractFactory factory) {
		return factory.getEbdClass();
	}

	/**
	 * Get an instance of an embedding with default values.
	 * 
	 * @param factory : factory to instantiate the embedding
	 * @return instance of the embedding
	 */
	public static VectorialEbd getEbdDefaultValue(VectEbdAbstractFactory factory) {
		return factory.createEbdDefaultValue();
	}
	
	/**
	 * Get an instance of an embedding with from an OrTools variable.
	 * 
	 * @param factory : factory to instantiate the embedding
	 * @param translation: OrTools variables for the translation
	 * @return instance of the embedding
	 */
	public static VectorialEbd getEbdOrToolsVariable(VectEbdAbstractFactory factory, MPVariable[] translation) {
		return factory.createEbdOrToolsVariable(translation);
	}
	
	/**
	 * Get an instance of an embedding as a copy.
	 * 
	 * @param factory : factory to instantiate the embedding
	 * @param embedding: value used for copy.
	 * @return instance of the embedding
	 */
	public static VectorialEbd getEbdCopy(VectEbdAbstractFactory factory, VectorialEbd embedding) {
		return factory.createEbdCopy(embedding);
	}
	
	/**
	 * Get an instance of an embedding with random values.
	 * 
	 * @param factory : factory to instantiate the embedding
	 * @return instance of the embedding
	 */
	public static VectorialEbd getEbdRandom(VectEbdAbstractFactory factory) {
		return factory.createEbdRandom();
	}
	
	/**
	 * Get an instance of an embedding as the middle of embedding values of a list of darts.
	 * 
	 * @param factory : factory to instantiate the embedding
	 * @param darts : list of darts to compute the middle from.
	 * @return instance of the embedding
	 */
	public static VectorialEbd getEbdMiddle(VectEbdAbstractFactory factory, List<JerboaDart> darts) {
		return factory.createEbdMiddle(darts);
	}
}
