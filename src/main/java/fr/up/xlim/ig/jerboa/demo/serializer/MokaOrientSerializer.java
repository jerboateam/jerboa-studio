/**
 * 
 */
package fr.up.xlim.ig.jerboa.demo.serializer;

import java.util.List;

import fr.up.xlim.ig.jerboa.ebds.Point3;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaModeler;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.exception.JerboaException;
import up.jerboa.util.serialization.EmbeddingSerializationKind;
import up.jerboa.util.serialization.moka.MokaEmbeddingSerialization;
import up.jerboa.util.serialization.moka.MokaPoint;

/**
 * @author Hakim Belhaouari
 *
 */
public class MokaOrientSerializer implements MokaEmbeddingSerialization {

	private JerboaModeler modeler;

	public MokaOrientSerializer(JerboaModeler modeler) {
		this.modeler = modeler;
	}

	/* (non-Javadoc)
	 * @see fr.up.jerboa.util.serialization.EmbeddingSerialization#unserialize(up.jerboa.core.JerboaEmbeddingInfo, java.lang.Object)
	 */
	@Override
	public Object unserialize(JerboaEmbeddingInfo info, MokaPoint stream) {
		MokaPoint point = (MokaPoint)stream;
		return new Point3(point.x, point.y, point.z);
	}

	/* (non-Javadoc)
	 * @see up.jerboa.util.serialization.EmbeddingSerialization#serialize(up.jerboa.core.JerboaEmbeddingInfo, java.lang.Object)
	 */
	@Override
	public MokaPoint serialize(JerboaEmbeddingInfo info, Object value) {
		Point3 p = (Point3)value;
		return new MokaPoint((float)p.getX(), (float)p.getY(), (float)p.getZ());
	}

	/**
	 * @see up.jerboa.util.serialization.EmbeddingSerialization#kind()
	 */
	@Override
	public EmbeddingSerializationKind kind() {
		return EmbeddingSerializationKind.SAVEANDLOAD;
	}

	/**
	 * @see up.jerboa.util.serialization.EmbeddingSerialization#manageDimension(int)
	 */
	@Override
	public boolean manageDimension(int dim) {
		return (modeler.getDimension() == dim);
	}

	
	/**
	 * @see up.jerboa.util.serialization.EmbeddingSerialization#searchCompatibleEmbedding(java.lang.String, up.jerboa.core.JerboaOrbit, java.lang.String)
	 */
	@Override
	public JerboaEmbeddingInfo searchCompatibleEmbedding(String name,
			JerboaOrbit orbit, String type) {
		if("point".equals(name) || "position".equals(name))
			return modeler.getEmbedding("position");
		else
			return null;
	}

	@Override
	public void completeProcess(JerboaGMap gmap, List<JerboaDart> nodes) throws JerboaException {
		/*
		{
			JerboaMark markerorient = gmap.creatFreeMarker();
			final JerboaEmbeddingInfo orientinfo = modeler.getOrient();
			final int oid = orientinfo.getID();
			final int dimension = gmap.getDimension();
			for (JerboaDart node : nodes) {
				
				if(node.isNotMarked(markerorient)) {
					Deque<Pair<JerboaDart, JerboaDart>> gnodes = new LinkedList<>();
					node.setEmbedding(oid, Boolean.TRUE);
					for(int i = 0;i <= dimension; i++) {
						gnodes.push(new Pair<JerboaDart, JerboaDart>(node, node.alpha(i)));
					}
					while(gnodes.size() > 0) {
						Pair<JerboaDart,JerboaDart> pair = gnodes.pop();
						final JerboaDart noder = pair.r();
						if(noder.isNotMarked(markerorient)) {
							noder.setEmbedding(oid, ! pair.l().<Boolean>ebd(oid) );
							gmap.mark(markerorient, noder);
							for(int i = 0;i <= dimension; i++) {
								gnodes.push(new Pair<JerboaDart, JerboaDart>(noder, noder.alpha(i)));
							}
						}
					}
				}
			}
			gmap.freeMarker(markerorient);
		}
		
		
		{
			JerboaMark markercol = gmap.creatFreeMarker();
			JerboaEmbeddingInfo colorinfo = modeler.getColor();
			for (JerboaDart node : nodes) {
				if (node.isNotMarked(markercol)) {
					gmap.mark(markercol, node);
					Color3 ebd = Color3.randomColor();
					Collection<JerboaDart> ns = gmap.orbit(node, colorinfo.getOrbit());
					for (JerboaDart j : ns) {
						j.setEmbedding(colorinfo.getID(), ebd);
					}
				}
			}
			gmap.freeMarker(markercol);
		}
		*/
	}

}
