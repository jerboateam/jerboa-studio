package fr.up.xlim.ig.jerboa.studio.infer.topo.foldable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import fr.up.xlim.ig.jerboa.studio.extractors.JMSExtendedRuleExtractor;
import fr.up.xlim.ig.jerboa.studio.infer.JMSExtendedRule;
import fr.up.xlim.ig.jerboa.studio.infer.parameters.JerboaInferRuleParameters;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils.ConnectivityException;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils.JMSFoldingQueueElement;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSMappedGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoDart;
import fr.up.xlim.ig.jerboa.studio.infer.topo.managers.JMSFoldingManager;
import fr.up.xlim.ig.jerboa.studio.orbitgenerator.JerboaOrbitGenerator;
import fr.up.xlim.ig.jerboa.studio.utils.HashCollectionsAuxFunctions;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEModeler;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERuleAtomic;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaOrbit;

/**
 * 
 * Graphs structure used to build foldings.
 * 
 * @author romain
 */
public class JMSFoldGraph extends JMSMappedGraph {

	private boolean isRule;
	private JerboaInferRuleParameters inferRuleParameters;

	/**
	 * By default, graphs are topological graphs with labels on arcs from [0,n]
	 * 
	 * @param maxDim              : maximal dimension
	 * @param inferenceParameters : inference parameters (handled by an instance
	 *                            of JerboaInferRuleParameters)
	 * 
	 */
	public JMSFoldGraph(int maxDim,
			JerboaInferRuleParameters inferenceParameters) {
		super(0, maxDim);
		this.isRule = false;
		this.inferRuleParameters = inferenceParameters;
	}

	/**
	 * Sanity check (all darts have all the dims)
	 * 
	 * Can be used for debugging.
	 * 
	 * @return
	 */
	public boolean sanityCheck() {
		for (JMSTopoDart dart : getDarts()) {
			for (int dim = 0; dim < maxDim; dim++) {
				if (dart.isFree(dim))
					return false;
			}
		}
		return true;
	}

	/**
	 * Set the graph to be folded as a rule.
	 * 
	 * @return Return an offset between left and right darts.
	 */
	public int setAsFoldRule() {
		this.minDim = kappa;
		this.isRule = true;
		return nextAvailableNodeID + 1;
	}

	/**
	 * Get the IDs of all darts that comes from the LHS.
	 * 
	 * @return collection of dart IDs
	 */
	public Collection<Integer> getLeftDartIDs() {
		if (!isRule)
			return getDartIDs();

		return getDartIDs().stream().filter(ID -> getDart(ID).isInLeftGraph())
				.collect(Collectors.toSet());
	}

	/**
	 * Determine if some nodes belong to the left part of the graph.
	 * 
	 * @return true if there is no node from in the LHS.
	 */
	public boolean lhsEmpty() {
		return getLeftDartIDs().isEmpty();
	}

	/**
	 * Check if the darts of the seed have an incident arc for each dimension in
	 * the orbit.
	 * 
	 * When loops (all or only the maximal dimension) are excluded, check if
	 * there is at least one arc that is not a loop on the set of darts
	 * associated to the seed.
	 * 
	 * Note : this is of no use yet but will be needed whenever we try to fold
	 * element extracted from Gmaps and not Gmaps.
	 * 
	 * @param seed : seed node (the dimensions of the orbit are checked).
	 * @return true if all darts associated to the seed node have an incident
	 *         arc for each dimension in the orbit
	 */
	private boolean checkSeedImplicitDims(JMSFoldSeed seed) {
		
		Collection<Integer> loopsExcluded = inferRuleParameters.parseExcluded();
		
		for (int orbitDim : seed.getOrbitDims()) {
			boolean hasOneActualArc = false;

			for (JMSTopoDart seedDart : seed.getOrbitDarts()) {
				Set<Integer> seedIncidentDims = seedDart.getIncidentDims();
				if (!seedIncidentDims.contains(orbitDim))
					return false;
				hasOneActualArc = hasOneActualArc || !seedDart.isLoop(orbitDim);
			}

			if (loopsExcluded.contains(orbitDim) && !hasOneActualArc) {
				System.err.println("Loops removal for dimension:=" + orbitDim + " conflicts with the hook orbit.");
				return false;
			}

		}
		return true;
	}

	/**
	 * Check if a collection of darts can correspond to an orbit. In this step,
	 * we consider only explicit arcs and loops to check is the arc extension is
	 * possible. The verification of implicit arcs is assumed to have be done
	 * already.
	 * 
	 * 1/ For all dimensions i not in the orbit type, if a dart d in the
	 * collection of darts is the source of a i-arc, then the other end point of
	 * the arc is either d (explicit loop) or not in the collection (explicit
	 * arc).
	 * 
	 * 2/ If a dart d in the collection of darts is source of an i-loop (for a
	 * dimension i not in the orbit type), then all darts are source of an
	 * i-loop.
	 * 
	 * 3/ If a dart d in the collection of darts is source of an i-arc which is
	 * not a loop (for a dimension i not in the orbit type), then all darts are
	 * source of an i-arc which is not a loop.
	 * 
	 * Note : we do not check the marks as we forbid kappa to be a dimension in
	 * the orbit.
	 * 
	 * @param assoc : dart-orbit association (the dimensions of the orbit are
	 *              checked).
	 * @return true if the collection of darts can represent a node.
	 */
	private boolean checkExplicitArcs(JMSRuleNode ruleNode) {
		HashMap<Integer, Set<Integer>> incidentLoops = new HashMap<>();
		HashMap<Integer, Set<Integer>> incidentArcs = new HashMap<>();

		if (!(buildExplicitIncidenceSets(ruleNode, incidentLoops,
				incidentArcs)))
			return false;

		if (!compareIncidence(ruleNode, incidentLoops))
			return false;

		if (!compareIncidence(ruleNode, incidentArcs))
			return false;

		return true;
	}

	/**
	 * Build explicit incidence sets (loops and arcs) for an darts assumed to
	 * correspond to a node. The dimensions of the orbit are considered as
	 * implicit and excluded from the sets.
	 * 
	 * @param ruleNode      : node (not in the rule graph yet)
	 * @param incidentLoops : map storing the loops
	 * @param incidentArcs  : map storing the arcs
	 * 
	 * @return true if the sets have correctly been built.
	 */
	private boolean buildExplicitIncidenceSets(JMSRuleNode ruleNode,
			HashMap<Integer, Set<Integer>> incidentLoops,
			HashMap<Integer, Set<Integer>> incidentArcs) {
		Set<JMSTopoDart> orbitDarts = ruleNode.getOrbitDarts();
		for (JMSTopoDart currDart : orbitDarts) {
			for (int dim : currDart.getIncidentDims()) {

				// discard orbit arcs
				if (ruleNode.orbitDimsContains(dim))
					continue;

				JMSTopoDart neighbour = currDart.getNeighbour(dim);

				// loop
				if (currDart.equals(neighbour)) {
					HashCollectionsAuxFunctions.addToSetMap(incidentLoops,
							currDart.getID(), dim);
				}

				// incorrect explicit arc with other end point in the orbit
				else if (orbitDarts.contains(neighbour))
					return false;

				// proper explicit arc
				else {
					HashCollectionsAuxFunctions.addToSetMap(incidentArcs,
							currDart.getID(), dim);
				}
			}
		}
		return true;
	}

	/**
	 * Compare dart incidence sets (either loops or arcs that are not loops).
	 * All darts in the hypothetical orbit need to have the same incident sets.
	 * 
	 * @param ruleNode     : node (not in the rule graph yet)
	 * @param incidenceMap : map (key=dartID, value=incidenceSet)
	 */
	private boolean compareIncidence(JMSRuleNode ruleNode,
			HashMap<Integer, Set<Integer>> incidenceMap) {
		// size check
		if (incidenceMap.keySet().size() == 0)
			return true;

		if (incidenceMap.keySet().size() != ruleNode.size())
			return false;

		// value check
		Set<Integer> refSet = incidenceMap.values().iterator().next();
		for (int dartID : incidenceMap.keySet()) {
			Set<Integer> dartIncidence = incidenceMap.get(dartID);
			if (!dartIncidence.equals(refSet))
				return false;
		}

		return true;
	}

	/**
	 * Check if, given a dart and an orbit, this seed can fold the whole graph.
	 * The methods builds the corresponding scheme graph.
	 * 
	 * NB : the graph should be connected.
	 * 
	 * @param data : folding manager storing all necessary information (in
	 *             particular the seed and the scheme rule build when built.
	 * @return true if the graph can be folded according to the information
	 *         provided.
	 * @throws ConnectivityException if the graph is not connected to avoid
	 *                               further attempt to fold the graph.
	 */
	private boolean tryFolding(JMSFoldingManager data)
			throws ConnectivityException {

		JMSFoldSeed seed = data.getSeed();

		/*
		 * Initialisation of the queue and creation of the scheme graph.
		 */
		data.addSeedAsANode();

		if (!checkSeedImplicitDims(seed))
			return false;
		if (!checkExplicitArcs(seed))
			return false;


		if (!data.initQueue(seed))
			return false;

		/*
		 * BFS
		 */

		while (!data.isQueueEmpty()) {
			JMSFoldingQueueElement headQueue = data.popQueue();

			JMSRuleNode toCheckNode = data.buildSupportNode(headQueue);
			data.addNodeToGraph(toCheckNode, headQueue);

			if (!(checkExplicitArcs(toCheckNode))) // Failed to build orbit
				return false;

			if (!data.updateQueue(toCheckNode)) // Failed to update queue
				return false;

		} // end while (Breadth-first search)

		if (!data.areAllDartSeen()) { // useful for debugging.
			System.err.println(
					"The folding graph has not been completely traverse.");
			System.err.println(data.getSeed());
		}

		return true;
	}

	/**
	 * Build a creation rule (LHS empty) by a direct transposition of the graph.
	 * 
	 * @param extRule : rule where the RHS graph has to be created
	 */
	private void buildCreateRule(JMSExtendedRule extRule) {
		HashMap<JMSTopoDart, JMENode> dartToNodeMap = new HashMap<>();
		JMERuleAtomic rule = extRule.getRule();
		for (JMSTopoDart dart : getDarts()) {
			JMENode ruleNode = rule.getRight().creatNode(0, 0);
			dartToNodeMap.put(dart, ruleNode);

			// mapping management
			ArrayList<JerboaDart> singletonList = new ArrayList<>(1);
			singletonList.add(getMappingR().get(dart));
			extRule.getMappingR().put(ruleNode, singletonList);
		}

		for (JMSTopoDart dart : getDarts()) {
			for (int arcDim : dart.getIncidentDims()) {
				JMSTopoDart neighbour = dart.getNeighbour(arcDim);
				if (dart.getID() < neighbour.getID()) {
					rule.getRight().creatArc(dartToNodeMap.get(dart),
							dartToNodeMap.get(neighbour), arcDim);
				} else if (dart.getID() == neighbour.getID()) {
					rule.getRight().creatLoop(dartToNodeMap.get(dart), arcDim);
				}
			}
		}
	}

	/**
	 * Method to build all possible foldings for the currently built foldable
	 * graph.
	 * 
	 * @param moded    : modeler (of the editor) where rules should be added
	 * @param prefix   : rules' prefix name
	 * @param category : category (of rules in the editor) where the rules
	 *                 should be added
	 * @param extRules    : list of rules built from folding the graph (updated by
	 *                 the function call)
	 * @throws ConnectivityException if the representation of the graph/rule is
	 *                               not connected.
	 */
	public void buildFoldings(JMEModeler moded, String prefix, String category,
			ArrayList<JMSExtendedRule> extRules)
			throws ConnectivityException {

		long startTime = System.nanoTime();

		if (noDart()) {
			System.err.println("Empty LHS and RHS, no rule can be infered.");
			return;
		}

		if (lhsEmpty()) {
			System.err.println("Empty LHS, building a creation rule.");
			creationRule(moded, prefix, category, extRules);
			return;
		}

		ArrayList<JMSFoldingManager> foldedRepr = new ArrayList<>();
		HashMap<Integer, Set<JerboaOrbit>> hasARule = new HashMap<>();

		if (inferRuleParameters.hasInferenceOrbit()) {
			JerboaOrbit specificOrbit = inferRuleParameters
					.convertInferenceOrbit();
			for (int dartID : getLeftDartIDs()) {
				tryOrbit(prefix, foldedRepr, hasARule, dartID, specificOrbit);
			}
		}

		else {
			for (int dartID : getLeftDartIDs()) {
				// The orbits are created with an iterator. The iteration should
				// be started again for each dart.
				for (JerboaOrbit orbit : new JerboaOrbitGenerator(maxDim)) {
					tryOrbit(prefix, foldedRepr, hasARule, dartID, orbit);
				}
			}
		}

		long endTime = System.nanoTime();
		System.out.println("Infered all folded representations for " + category
				+ " in " + (endTime - startTime) / 1000000 + "ms.");

		exportFoldedGraphsToRules(moded, category, extRules, foldedRepr);

	}

	/**
	 * Build a creation rule in case of empty LHS
	 * 
	 * @param moded    : modeler (of the editor) where rules should be added
	 * @param prefix   : rules' prefix name
	 * @param category : category (of rules in the editor) where the rules
	 *                 should be added
	 * @param rules    : list of rules built from folding the graph (updated by
	 *                 the function call)
	 */
	private void creationRule(JMEModeler moded, String prefix, String category,
			ArrayList<JMSExtendedRule> extRules) {

		long startTime = System.nanoTime();

		String ruleName = prefix + "CreatRule";
		JMERuleAtomic ruleed = new JMERuleAtomic(moded, ruleName);
		ruleed.setCategory(category);

		JMSExtendedRule extRule = new JMSExtendedRule(ruleed);
		buildCreateRule(extRule);
		extRules.add(extRule);

		System.out.println("Built creation rule for " + category + " in "
				+ (System.nanoTime() - startTime) / 1000000 + "ms.");
	}

	/**
	 * Try folding the graph along a given orbit with the help of a folding
	 * manager.
	 * 
	 * @param prefix     : rules' prefix name
	 * @param foldedRepr : list of JMSFoldingManager. If the algorithm
	 *                   terminates with the provided orbit and dart, the
	 *                   obtained scheme rule will be added to this list.
	 * @param hasARule   : map (key=dartID, value=orbitSet) containing the
	 *                   orbits already covered for each dart
	 * @param dartID     : ID of the dart used to fold the graph.
	 * @param orbit      : orbit type used to fold the graph.
	 * @throws ConnectivityException if the representation of the graph/rule is
	 *                               not connected.
	 */
	private void tryOrbit(String prefix,
			ArrayList<JMSFoldingManager> foldedRepr,
			HashMap<Integer, Set<JerboaOrbit>> hasARule, int dartID,
			JerboaOrbit orbit) throws ConnectivityException {
		if (!HashCollectionsAuxFunctions.containsSetMap(hasARule, dartID,
				orbit)) {
			System.err.println("Trying folding with dartID " + dartID + " and orbit " + orbit);
			String ruleName = prefix + "Dart" + dartID + "Orbit"
					+ orbit.javaCompliantString();
			JMSFoldingManager data = new JMSFoldingManager(this,
					getDart(dartID), orbit, ruleName, inferRuleParameters);

			if (tryFolding(data))
				addIfNotIsomorphic(foldedRepr, data);

			markEquivalentSeeds(hasARule, data);
		}
	}

	/**
	 * Determine whether a folded rule is isomorphic to one of the folded
	 * representation of a foldable graph. In other words, determine if the last
	 * rule scheme built is isomorphic to one previously obtained.
	 * 
	 * In the case where the rule is `new`, it is added to the list of rules.
	 * 
	 * @param foldedRepr : list of already found rules
	 * @param data       : representation of the newly found rule
	 */
	private void addIfNotIsomorphic(ArrayList<JMSFoldingManager> foldedRepr,
			JMSFoldingManager data) {
		boolean toAdd = true;

		for (JMSFoldingManager foldingData : foldedRepr) {
			if (foldingData.getRuleGraph().isIsomorphic(data.getRuleGraph())) {
				toAdd = false;
				break;
			}
		}

		if (toAdd)
			foldedRepr.add(data);
	}

	/**
	 * Mark pairs of (dart, orbit) that would lead to similar scheme rules.
	 * 
	 * @param hasARule : map (key=dartID, value=orbitSet) containing the orbits
	 *                 already covered for each dart
	 * @param data     : data obtained from the current folding of the graph.
	 */
	private void markEquivalentSeeds(
			HashMap<Integer, Set<JerboaOrbit>> hasARule,
			final JMSFoldingManager data) {
		for (JMSRuleNode ruleNode : data.getRuleGraph().getDarts()) {
			Collection<Integer> orbitDartIDs = ruleNode.getOrbitDartIDs();

			if (ruleNode.isOrbitFull() && ruleNode.isInLeftGraph()) {
				for (int orbitDartID : orbitDartIDs) {
					hasARule.computeIfAbsent(orbitDartID,
							k -> new HashSet<JerboaOrbit>())
							.add(ruleNode.getOrbit());

				}
			}
		}
	}

	/**
	 * Export all rules built from the folding algorithm to JMERules.
	 * 
	 * @param moded      : modeler (of the editor) where rules should be added
	 * @param category   : category (of rules in the editor) where the rules
	 *                   should be added
	 * @param extRules   : list of rules built from folding the graph (updated
	 *                   by the function call)
	 *
	 * @param foldedRepr : list of folded representation of rules
	 */
	private void exportFoldedGraphsToRules(JMEModeler moded, String category,
			ArrayList<JMSExtendedRule> extRules,
			ArrayList<JMSFoldingManager> foldedRepr) {
		for (JMSFoldingManager foldingData : foldedRepr) {

			JMSRuleGraph ruleGraph = foldingData.getRuleGraph();
			List<JMSTopoDart> seedDarts = new ArrayList<JMSTopoDart>(
					foldingData.getSeed().getOrbitDarts());
			Collections.sort(seedDarts);

			JMSExtendedRule extRule = JMSExtendedRuleExtractor
					.buildMappingFromFoldedGraph(this, moded, category,
							ruleGraph, seedDarts, inferRuleParameters);

			extRules.add(extRule);
		}
	}
}
