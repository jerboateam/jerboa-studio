package fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils;

import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleNode;

/**
 * Queue elements consists of a dart-orbit association and a dimension.
 * 
 * @author romain
 *
 */
public class JMSFoldingQueueElement {
	private JMSRuleNode node;
	private int arcDim;

	public JMSFoldingQueueElement(JMSRuleNode node, int arcDim) {
		this.node = node;
		this.arcDim = arcDim;
	}

	public JMSRuleNode getNode() {
		return node;
	}

	public int getArcDim() {
		return arcDim;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arcDim;
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JMSFoldingQueueElement other = (JMSFoldingQueueElement) obj;
		if (arcDim != other.arcDim)
			return false;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JMSFoldingQueueElement [node=");
		builder.append(node);
		builder.append(", arcDim=");
		builder.append(arcDim);
		builder.append("]");
		return builder.toString();
	}

}