package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

import java.util.ArrayList;
import java.util.List;

import com.google.ortools.Loader;
import com.google.ortools.linearsolver.MPConstraint;
import com.google.ortools.linearsolver.MPObjective;
import com.google.ortools.linearsolver.MPSolver;
import com.google.ortools.linearsolver.MPVariable;

import fr.up.xlim.ig.jerboa.ebds.VectorialEbd;
import fr.up.xlim.ig.jerboa.ebds.factories.VectEbdAbstractFactory;
import fr.up.xlim.ig.jerboa.ebds.factories.VectEbdFactory;
import up.jerboa.core.util.Pair;

/**
 * OrTools-based implementation of the solver for vectorial embeddings.
 * 
 * @author Romain
 *
 */
public class VectEbdSolverOrTools implements VectEbdSolver {

	private VectEbdAbstractFactory ebdFactory;
	private int ebdDimSize;
	private double[] minTranslation;
	private double[] maxTranslation;
	private final double infinity;

	public VectEbdSolverOrTools(VectEbdAbstractFactory ebdFactory,
			int ebdDimSize, double[] minTranslation, double[] maxTranslation) {
		this.ebdFactory = ebdFactory;
		this.ebdDimSize = ebdDimSize;
		this.minTranslation = minTranslation;
		this.maxTranslation = maxTranslation;
		Loader.loadNativeLibraries();
		this.infinity = MPSolver.infinity();
	}

	public Pair<List<Double>, VectorialEbd> guessCoefsSystem(
			List<Pair<List<VectorialEbd>, VectorialEbd>> equationSystem)
			throws LPSolverException {
		try {
			System.out.println("Attempt without translation");
			return guessCoefsSystemTranslation(equationSystem, false);
		} catch (LPSolverException lpse) {
			System.out.println("Attempt with translation");
			return guessCoefsSystemTranslation(equationSystem, true);
		}
	}

	public Pair<List<Double>, VectorialEbd> guessCoefsSystemTranslation(
			List<Pair<List<VectorialEbd>, VectorialEbd>> equationSystem,
			boolean withTranslation) throws LPSolverException {
		MPSolver solver = MPSolver.createSolver("GLOP");
		solver.clear(); // Clears the objective (including the optimization
						// direction), all variables and constraints
		List<Double> res = new ArrayList<>();
		List<MPVariable> vars = new ArrayList<>();

		// La translation est commune a tous les points
		MPVariable[] translation = new MPVariable[ebdDimSize];
		for (int ebdDim = 0; ebdDim < ebdDimSize; ebdDim++) {
			translation[ebdDim] = solver.makeNumVar(minTranslation[ebdDim],
					maxTranslation[ebdDim], "tx" + ebdDim);
		}

		// Les coeffs sont communs a toutes les variables
		for (int i = 0; i < equationSystem.get(0).l().size(); ++i) {
			MPVariable coef = solver.makeNumVar(-infinity, infinity, "a" + i);
			vars.add(coef);
		}

		for (int eqnIndex = 0; eqnIndex < equationSystem.size(); eqnIndex++) {
			Pair<List<VectorialEbd>, VectorialEbd> eqn = equationSystem
					.get(eqnIndex);
			List<VectorialEbd> pts = eqn.l();
			VectorialEbd d = eqn.r();

			MPConstraint[] cs = new MPConstraint[ebdDimSize];
			for (int ebdDim = 0; ebdDim < ebdDimSize; ebdDim++) {
				cs[ebdDim] = solver.makeConstraint(
						d.getFromDim(ebdDim).doubleValue(),
						d.getFromDim(ebdDim).doubleValue(),
						"x" + ebdDim + "_" + eqnIndex);
			}

			for (int ebdDim = 0; ebdDim < ebdDimSize; ebdDim++) {
				for (int i = 0; i < pts.size(); ++i) {
					VectorialEbd p = pts.get(i);
					cs[ebdDim].setCoefficient(vars.get(i),
							p.getFromDim(ebdDim).doubleValue());
				}
				if (withTranslation)
					cs[ebdDim].setCoefficient(translation[ebdDim], 1);
				else
					cs[ebdDim].setCoefficient(translation[ebdDim], 0);
			}
		}

		System.out.println("SOLVER OrTools: VARS=" + solver.numVariables()
				+ " Constraints=" + solver.numConstraints());
		MPObjective objective = solver.objective();
		objective.setMinimization();

		final MPSolver.ResultStatus resultStatus = solver.solve();
		System.out.println("Solved? " + resultStatus);
		if (resultStatus == MPSolver.ResultStatus.OPTIMAL
				|| resultStatus == MPSolver.ResultStatus.FEASIBLE) {
			
			
			Pair<List<VectorialEbd>, VectorialEbd> eqn = equationSystem.get(0);
			List<VectorialEbd> pts = eqn.l();
			VectorialEbd d = eqn.r();
			for(int dim = 0; dim < ebdDimSize; ++dim) {
				for(int i = 0;i < pts.size(); ++i) {
					VectorialEbd p = pts.get(i);
					MPVariable a = vars.get(i);
					System.out.print((i == 0 ? " " : " + ") + a.solutionValue()
							+ " * " + p.getFromDim(dim));
				}
				
				System.out.println(" = " + d.getFromDim(dim));
			}
			
			
			for (int i = 0; i < equationSystem.get(0).l().size(); ++i) {
				res.add(vars.get(i).solutionValue());
			}

		} else {
			System.err
					.println("The problem does not have an optimal solution!");
			throw new LPSolverException();
		}
		if (withTranslation)
			return new Pair<List<Double>, VectorialEbd>(res,
					VectEbdFactory.getEbdOrToolsVariable(ebdFactory, translation));
		else
			return new Pair<List<Double>, VectorialEbd>(res,
					VectEbdFactory.getEbdDefaultValue(ebdFactory));
	}

}
