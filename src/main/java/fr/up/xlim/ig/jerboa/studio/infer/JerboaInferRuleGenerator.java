package fr.up.xlim.ig.jerboa.studio.infer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudioWithInference;
import fr.up.xlim.ig.jerboa.studio.JerboaStudio;
import fr.up.xlim.ig.jerboa.studio.extractors.JMSDisplayExtractor;
import fr.up.xlim.ig.jerboa.studio.extractors.JMSGmapExtractor;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.inject.JerboaRuleGeneratedImpl;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.BaryPositionStrategy;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.MixColorStrategy;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.PoIStrategy;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers.ColorSolverOrToolsFactory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers.PositionSolverOrToolsFactory;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers.VectEbdSolver;
import fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers.VectEbdSolverFactory;
import fr.up.xlim.ig.jerboa.studio.infer.parameters.JerboaInferGeomParameters;
import fr.up.xlim.ig.jerboa.studio.infer.parameters.JerboaInferRuleParameters;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSFoldGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils.ConnectivityException;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils.GmapException;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEArc;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEGraph;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEModeler;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERule;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERuleAtomic;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaGMapArray;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.core.rule.JerboaRuleNode;
import up.jerboa.core.util.JerboaGMapDuplicateFactory;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaGMapDuplicateException;

/**
 * Generator for the inference mechanism.
 * 
 * @author Romain and Hakim
 *
 */
public class JerboaInferRuleGenerator {

	protected JerboaGMap before;
	protected JerboaGMap after;
	private JerboaInferRuleGeneratorDupFactory factory;
	private JerboaStudio studio;
	private JerboaInferRuleParameters inferenceTopoParameters;
	private JerboaInferGeomParameters inferenceGeomParameters;

	public JerboaInferRuleGenerator(JerboaStudio studio) {
		factory = new JerboaInferRuleGeneratorDupFactory();
		this.studio = studio;
		this.inferenceTopoParameters = new JerboaInferRuleParameters();
		this.inferenceGeomParameters = new JerboaInferGeomParameters();
	}


	public void setLeftPatternRaw(JerboaGMap gmap) {
		before = gmap;
	}

	public void setRightPatternRaw(JerboaGMap gmap) {
		after = gmap;
	}

	public JerboaInferRuleParameters getTopoParameters() {
		return inferenceTopoParameters;
	}

	public JerboaInferGeomParameters getGeomParameters() {
		return inferenceGeomParameters;
	}

	public Map<String, JerboaRuleNode> convertGraph(
			JerboaModelerStudioWithInference modeler, JMEGraph graph,
			List<JerboaRuleNode> leftnodes) {
		HashMap<String, JerboaRuleNode> mapLeft = new HashMap<>();
		List<JMENode> lnodes = graph.getNodes();
		for (JMENode lnode : lnodes) {
			JerboaOrbit orbit = new JerboaOrbit(lnode.getOrbit());
			JerboaRuleNode jlnode = new JerboaRuleNode(lnode.getName(),
					lnode.getID(), orbit, modeler.getDimension());
			leftnodes.add(jlnode);
			mapLeft.put(lnode.getName(), jlnode);
		}
		List<JMEArc> larcs = graph.getArcs();
		for (JMEArc larc : larcs) {
			int dim = larc.getDimension();
			String source = larc.getSource().getName();
			String target = larc.getDestination().getName();
			JerboaRuleNode jls = mapLeft.get(source);
			JerboaRuleNode jlt = mapLeft.get(target);
			jls.setAlpha(dim, jlt);
		}
		return mapLeft;
	}

	public JerboaRuleGeneratedImpl convertToView(JMERule atomic) {
		JerboaModelerStudioWithInference modeler = studio.getModelerLeft();
		JerboaRuleGeneratedImpl rule = new JerboaRuleGeneratedImpl(modeler,
				atomic.getName(), atomic.getCategory());

		ArrayList<JerboaRuleNode> leftnodes = new ArrayList<>();
		ArrayList<JerboaRuleNode> rightnodes = new ArrayList<>();

		Map<String, JerboaRuleNode> lnodes = convertGraph(modeler,
				atomic.getLeft(), leftnodes);
		Map<String, JerboaRuleNode> rnodes = convertGraph(modeler,
				atomic.getRight(), rightnodes);
		List<JerboaRuleNode> hooks = new ArrayList<>();

		for (JMENode hook : atomic.getHooks()) {
			JerboaRuleNode nhook = lnodes.get(hook.getName());
			hooks.add(nhook);
		}

		rule.getLeft().addAll(leftnodes);
		rule.getRight().addAll(rightnodes);
		rule.getHooks().addAll(hooks);

		try {
			rule.compile();
			System.out.println(
					"TRADUCTION OF INFERED RULE INTO JERBOA RULE DONE WITH SUCCESS");
		} catch (JerboaException e) {
			System.out.println(
					"TRADUCTION OF INFERED RULE INTO JERBOA RULE DONE WITH FAILURE");
			e.printStackTrace();
		}

		return rule;
	}

	/**
	 * 
	 * Topological and geometric inference of an operation as a set of rules.
	 * 
	 * @author romain
	 * 
	 * @param moded          : modeler with all the rules
	 * @param modeler        : modeler with the script for the inference
	 * @param prefix         : rules prefix
	 * @param category       : rules category
	 * @param mapping        : left to right mapping (in the Gmaps)
	 * @param leftOutOfRule  : left darts to consider as not being part of the
	 *                       operation
	 * @param rightOutOfRule : right darts to consider as not being part of the
	 *                       operation
	 * @return : list of inferred rules
	 */
	public List<JMSExtendedRule> inferExtendedRules(JMEModeler moded,
			JerboaModelerStudioWithInference modeler, String prefix,
			String category, Map<Integer, Integer> mapping,
			Set<JerboaDart> leftOutOfRule,
			Set<JerboaDart> rightOutOfRule) {

		ArrayList<JMSExtendedRule> extRules = new ArrayList<>();
		
		if (before.size() == leftOutOfRule.size() && after.size() == rightOutOfRule.size()) {
			System.err.println("Empty inputs, no rule generated.");
			return extRules;
		}

		/*
		 * Topo
		 */

		buildFoldings(moded, modeler, prefix, category, extRules, mapping,
				leftOutOfRule, rightOutOfRule);
		List<JMERuleAtomic> rules = extRules.stream().map(mr -> mr.getRule())
				.collect(Collectors.toList());
		System.out.println("nb rules: " + rules.size());

		/*
		 * Display
		 */

		long startDisplayTime = System.nanoTime();
		for (JMSExtendedRule extRule : extRules) {
			JMERuleAtomic rule = extRule.getRule();
			JMSDisplayExtractor display = new JMSDisplayExtractor(rule);
			display.randomDisplay();

			if (inferenceTopoParameters.getDisplay())
				display.fixDisplay();
		}
		System.out.println("Rules display in "
				+ (System.nanoTime() - startDisplayTime) / 1000000 + "ms.");

		/*
		 * Geom
		 */
		
		if (inferenceGeomParameters.getInferPositions()) {
			long startTime = System.nanoTime();
			
			// hack for creation rules
			if (before.size() == 0) {
				System.err.println("Inferring positions on a creation rule.");
				JMSExtendedRule creationRule = extRules.get(0);
				creationRule.enrichPositionCreationRule();
			}

			else {
				VectEbdSolver psolver = VectEbdSolverFactory
						.createVectEbdSolver(
								new PositionSolverOrToolsFactory());
				PoIStrategy poiStrat = new BaryPositionStrategy();
				for (JMSExtendedRule rule : extRules) {
					rule.enrichPositions(psolver, poiStrat, inferenceGeomParameters.getCoefThreshold());
				}
			}

			long endTime = System.nanoTime();
			System.out.println("Positions Inference in "
					+ (endTime - startTime) / 1000000 + "ms.");
		}
		
		if (inferenceGeomParameters.getInferColors()) {
			long startTime = System.nanoTime();
			
			// hack for creation rules
			if (before.size() == 0) {
				System.err.println("Inferring colors on a creation rule.");
				JMSExtendedRule creationRule = extRules.get(0);
				creationRule.enrichColorCreationRule();
			}

			else {
				VectEbdSolver csolver = VectEbdSolverFactory
						.createVectEbdSolver(new ColorSolverOrToolsFactory());
				PoIStrategy poiStrat = new MixColorStrategy();
				for (JMSExtendedRule rule : extRules) {
					rule.enrichColors(csolver, poiStrat, inferenceGeomParameters.getCoefThreshold());
				}
			}
			
			long endTime = System.nanoTime();
			System.out.println("Colors Inference in "
					+ (endTime - startTime) / 1000000 + "ms.");
		}

		return extRules;
	}

	/**
	 * Builds rule schemes from a two G-maps.
	 * 
	 * @author romain
	 *
	 * @param modeler
	 * @param rightOutOfRuleIDs
	 * @param leftOutOfRuleIDs
	 * 
	 * @return A list of rule schemes obtained by valid foldings of the two
	 *         G-maps.
	 */
	private void buildFoldings(JMEModeler moded,
			JerboaModelerStudioWithInference modeler, String prefix,
			String category, ArrayList<JMSExtendedRule> extRules,
			Map<Integer, Integer> mapping, Set<JerboaDart> leftOutOfRule,
			Set<JerboaDart> rightOutOfRule) {
		try {
			JMSFoldGraph graphToFold = JMSGmapExtractor.buildFoldGraphFromRule(
					before, after, inferenceTopoParameters, mapping,
					leftOutOfRule, rightOutOfRule);
			graphToFold.buildFoldings(moded, prefix, category, extRules);
		} catch (ConnectivityException e) {
			System.err.println(
					"The union representation of LHS and RHS is not connected. Not supported (yet).");
		} catch (GmapException e) {
			System.err.println(e.getMessage());
		}
	}

	class JerboaInferRuleGeneratorDupFactory
			implements JerboaGMapDuplicateFactory {

		@Override
		public Object duplicate(JerboaEmbeddingInfo info, Object value) {
			return value; // on s'en fiche des plongements
		}

		@Override
		public JerboaEmbeddingInfo convert(JerboaEmbeddingInfo info) {
			return info;
		}

		@Override
		public boolean manageEmbedding(JerboaEmbeddingInfo info) {
			return true;
		}

	}

	/**
	 * Has to be done once the viewer has been created.
	 * 
	 * @author Romain
	 */
	public void addParameters() {
		studio.getViewer().addTab("Scheme inference param",
				inferenceTopoParameters.getDialog().displayInPanel());
		studio.getViewer().addTab("Misc",
				inferenceGeomParameters.getDialog().displayInPanel());
	}

}
