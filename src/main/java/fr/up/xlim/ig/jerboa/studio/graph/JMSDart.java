package fr.up.xlim.ig.jerboa.studio.graph;

/**
 * Interface for darts in a graph.
 * 
 * @author romain
 *
 * @param <ArcLabelType> : type of arc labels.
 */
public interface JMSDart<ArcLabelType> {

	/**
	 * Add a dart as a neighbour for a given label. If the node previously had
	 * another neighbour, it is replaced.
	 * 
	 * @param dart  : other dart
	 * @param label : arc label
	 */
	void addArc(JMSDart<ArcLabelType> dart, ArcLabelType label);

	/**
	 * Removes the arc for the specified label.
	 * 
	 * @param label : arc label
	 * @return true if the arc was removed
	 */
	boolean removeArc(ArcLabelType label);

	/**
	 * Removes the arc for the specified label only when the arc link the dart
	 * to the specified dart.
	 * 
	 * @param dart  : arc target
	 * @param label : arc label
	 * @return true if the arc was removed
	 */
	boolean removeArc(JMSDart<ArcLabelType> dart, ArcLabelType label);

	/**
	 * Check if the dart has a neighbour for a given label.
	 * 
	 * @param label : arc label
	 * @return boolean
	 */
	boolean hasNeighbour(ArcLabelType label);

	/**
	 * Get the neighbouring dart for a given label. Returns NULL if none exists.
	 * 
	 * @param label : label of the arc
	 * @return neighbouring dart if exists. NULL otherwise.
	 */
	JMSDart<ArcLabelType> getNeighbour(ArcLabelType label);
}