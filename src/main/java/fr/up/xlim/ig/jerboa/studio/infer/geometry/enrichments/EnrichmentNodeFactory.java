package fr.up.xlim.ig.jerboa.studio.infer.geometry.enrichments;

import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;

/**
 * 
 * @author Romain
 *
 */
public interface EnrichmentNodeFactory {
	
	/**
	 * Get an instance of an enrichment node.
	 * 
	 * @return instance of the enrichment node
	 */
	public static VectEbdEnrichmentNode getEnrichmentNode(EnrichmentNodeAbstractFactory factory, VectEbdEnrichmentRule eRule, JMENode node) {
		return factory.createEnrichmentNode(eRule, node);
	}
}
