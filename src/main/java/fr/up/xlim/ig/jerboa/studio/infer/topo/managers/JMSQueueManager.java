package fr.up.xlim.ig.jerboa.studio.infer.topo.managers;

import java.util.ArrayDeque;
import java.util.Set;

import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSFoldSeed;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleNode;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.utils.JMSFoldingQueueElement;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoDart;

/**
 * Take care of queue management during the folding construction.
 * 
 * @author romain
 *
 */
public class JMSQueueManager {

	private JMSFoldingManager foldingManager;
	private ArrayDeque<JMSFoldingQueueElement> queue;

	public JMSQueueManager(JMSFoldingManager foldingManager) {
		this.foldingManager = foldingManager;
		this.queue = new ArrayDeque<JMSFoldingQueueElement>();
	}

	public boolean isQueueEmpty() {
		return queue.isEmpty();
	}

	public JMSFoldingQueueElement popQueue() {
		return queue.pop();
	}

	/**
	 * Initialise the dequeue (add seed) to run the graph traversal. The dart
	 * corresponding to the seed must have been mapped the the node
	 * preemptively.
	 * 
	 * @param seed : node (mapped from darts) used for the local search.
	 * 
	 * @return true if the queue has properly been initialised.
	 */
	public boolean initQueue(JMSFoldSeed seed) {
		Set<Integer> arcExtension = seed.dimsForArcExtension();
		for (int dim : arcExtension) {
			JMSFoldingQueueElement toAdd = new JMSFoldingQueueElement(seed,
					dim);
			queue.add(toAdd);
		}
		return true;
	}

	/**
	 * Update queue (add new elements) after a node has been added to the graph,
	 * i.e. initialise the arc extension process.
	 * 
	 * 1) If the darts that correspond to the rule node do not point towards
	 * darts that either all correspond to the same rule node or all correspond
	 * to no rule node (yet), the arc extension is not valid. We stop the
	 * algorithm and no folding can be built.
	 * 
	 * 2) If these darts correspond to no rule node, we initialise an arc
	 * extension by adding the corresponding couple (node, dim) to the queue.
	 * 
	 * 3) If these darts correspond to the same node, we check that the arcs in
	 * the folding graph are coherent with the arc that we are about to add in
	 * the rule graph (in practice, this is checked by comparing the associated
	 * darts). If the arcs are coherent, we remove the target rule node from the
	 * queue and add an arc to the graph.
	 * 
	 * @param node : we initialise the arc extension process for each dimension
	 *             of arc incident to this node.
	 * 
	 * @return true is the extension process can be initialised, i.e. if the
	 *         queue can be updated (no contradiction with elements already in
	 *         the queue).
	 */
	public boolean updateQueue(JMSRuleNode node) {

		Set<Integer> arcExtension = node.dimsForArcExtension();

		for (int dim : arcExtension) {
			JMSFoldingQueueElement toAdd = new JMSFoldingQueueElement(node,
					dim);
			JMSTopoDart dartNeighbor = node.getAssocDart().getNeighbour(dim);
			JMSRuleNode mappedNode = foldingManager.nodeFromDart(dartNeighbor);

			if (!checkDartNeighbours(node, dim, mappedNode))
				return false;

			if (mappedNode == null) { // initialise arc extension
				queue.add(toAdd);
				continue;
			}

			if (!mappedNode.getAssocDart().equals(dartNeighbor)) // coherence
				return false;

			JMSFoldingQueueElement toRemove = new JMSFoldingQueueElement(
					mappedNode, dim);
			queue.remove(toRemove);
			foldingManager.getRuleGraph().addArc(node, mappedNode, dim);
		}
		return true;
	}

	/**
	 * Check if an explicit arc of a given dimension can be added between two
	 * nodes.
	 * 
	 * @param node       : node that we want to add.
	 * @param dim        : dimension of the arc
	 * @param mappedNode : node mapped to the neighbour of the dart associated
	 *                   to node (can be NULL);
	 * @return true if an explicit arc could be added between the two nodes.
	 *         Either all darts associated to `node` points towards darts
	 *         associated to the same node. Or they all points towards darts not
	 *         (yet) associated to a node.
	 */
	private boolean checkDartNeighbours(JMSRuleNode node, int dim,
			JMSRuleNode mappedNode) {
		for (JMSTopoDart dart : node.getOrbitDarts()) {
			JMSTopoDart neighbourDart = dart.getNeighbour(dim);

			if (mappedNode == null && foldingManager.hasNode(neighbourDart))
				return false;

			if (mappedNode == null && !foldingManager.hasNode(neighbourDart))
				continue;

			if (mappedNode != null && !foldingManager.hasNode(neighbourDart))
				return false;

			JMSRuleNode neighbourNode = foldingManager
					.nodeFromDart(neighbourDart);
			if (!neighbourNode.equals(mappedNode))
				return false;
		}
		return true;
	}

}
