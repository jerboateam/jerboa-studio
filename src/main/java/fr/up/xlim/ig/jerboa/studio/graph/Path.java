package fr.up.xlim.ig.jerboa.studio.graph;

/**
 * This might be a bit useless. The idea is to represent paths in the graph,
 * mostly to compute display. At the moment, nothing else than source and target
 * are stored.
 * 
 * source to target should be the same as target to source
 * 
 * @author Romain
 *
 * @param <DartType> type of darts in the underlying graph.
 */
public class Path<DartType extends JMSDart<?>> {
	private DartType source;
	private DartType target;

	/**
	 * @return the source
	 */
	public DartType getSource() {
		return source;
	}

	/**
	 * @return the target
	 */
	public DartType getTarget() {
		return target;
	}

	/**
	 * note that source to target is the same as target to source
	 * 
	 * @param source
	 * @param target
	 */

	public Path(DartType source, DartType target) {
		this.source = source;
		this.target = target;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		return prime * (((source == null) ? 0 : source.hashCode())
				+ ((target == null) ? 0 : target.hashCode()));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Path<?> other = (Path<?>) obj;
		if (source == null) {
			if (other.source != null)
				return false;
		}
		if (other.source == null) {
			if (source != null)
				return false;
		} 
		if (target == null) {
			if (other.target != null)
				return false;
		}
		if (other.target == null) {
			if (target != null)
				return false;
		}
		if (source.equals(other.source) && target.equals(other.target))
			return true;
		if (source.equals(other.target) && target.equals(other.source))
			return true;
		return false;
	}

}
