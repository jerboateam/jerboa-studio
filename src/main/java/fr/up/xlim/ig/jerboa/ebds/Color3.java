package fr.up.xlim.ig.jerboa.ebds;

import java.awt.Color;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.swing.JColorChooser;

import up.jerboa.core.JerboaDart;


/**
 * 
 * @author Hakim BELHAOUARI and romain
 *
 */

public class Color3 implements VectorialEbd{

	private float r, g, b, a;
	
	public static final Color3 RED = new Color3  (1,0,0);
	public static final Color3 GREEN = new Color3(0,1,0);
	public static final Color3 BLUE = new Color3 (0,0,1);
	public static final Color3 LIGHTGRAY = new Color3 (0.5f,0.5f,0.5f);

	public Color3(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public Color3(float r, float g, float b) {
		this(r, g, b, 1);
	}

	public Color3(Color color) {
		float[] rgb = color.getRGBComponents(null);
		r = rgb[0];
		g = rgb[1];
		b = rgb[2];
		a = rgb[3];
	}

	public Color3(Color3 ebd) {
		r = ebd.r;
		g = ebd.g;
		b = ebd.b;
		a = ebd.a;
	}
	
	public Color3() {
		this(0,0,0,0);
	}

	public float getR() {
		return r;
	}

	public void setR(float r) {
		this.r = r;
	}

	public float getG() {
		return g;
	}

	public void setG(float g) {
		this.g = g;
	}

	public float getB() {
		return b;
	}

	public void setB(float b) {
		this.b = b;
	}

	public void setA(float a) {
		this.a = a;
	}

	public float getA() {
		return a;
	}
	
	public Number getFromDim(int i) {
		switch(i) {
		case 0: return (double)r;
		case 1: return (double)g;
		case 2: return (double)b;
		case 3: return a;
		default: return 0;
		}
	}

	public void setRGB(float[] rgb) {
		r = rgb[0];
		g = rgb[1];
		b = rgb[2];
		if (rgb.length > 3)
			a = rgb[3];
	}

	public void setRGB(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public void setRGB(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public void setRGB(Color color) {
		float[] rgb = color.getRGBComponents(null);
		r = rgb[0];
		g = rgb[1];
		b = rgb[2];
		a = rgb[3];
	}

	public static Color3 middle(Color3 a, Color3 b) {
		return new Color3((a.r + b.r) / 2, (a.g + b.g) / 2, (a.b + b.b) / 2, (a.a + b.a) / 2);
	}

	public static Color3 middle(List<Color3> colors) {

		float r = 0, g = 0, b = 0, a = 0;
		int size = 0;

		for (Color3 c : colors) {
			r += c.r;
			g += c.g;
			b += c.b;
			a += c.a;
			size++;
		}

		return new Color3(r / size, g / size, b / size, a / size);
	}
	
	public static Color3 middle(Collection<Color3> colors) {

		float r = 0, g = 0, b = 0, a = 0;
		int size = 0;

		for (Color3 c : colors) {
			r += c.r;
			g += c.g;
			b += c.b;
			a += c.a;
			size++;
		}

		return new Color3(r / size, g / size, b / size, a / size);
	}
	
	public static Color3 middle(List<JerboaDart> darts, String ebdname) {
		return middle(darts.stream().map(dart -> (Color3)dart.ebd(ebdname)).collect(Collectors.toList()));
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Color<");
		sb.append(r).append(";").append(g).append(";").append(b).append("|").append(a).append(">");
		return sb.toString();
	}

	public static Color3 randomColor() {
		Random r = new Random();
		return new Color3(r.nextFloat(), r.nextFloat(), r.nextFloat());
	}

	protected static final float FACTOR = 0.7f;

	public static Color3 darker(Color3 ebd) {

		return new Color3(Math.max(ebd.r * FACTOR, 0), Math.max(ebd.g * FACTOR, 0), Math.max(ebd.b * FACTOR, 0));
	}

	public static Color3 askColor(Color3 ebd) {
		Color c = JColorChooser.showDialog(null, "Choose color:", new Color(ebd.r, ebd.g, ebd.b, ebd.a));
		return new Color3(c);
	}

	public static float BRIGHTER = 20;
	public static float DARKER = -20;
	
	public Color3 brighter() {
		float r = this.r;
		float g = this.g;
		float b = this.b;
		float a = this.a;
		
		// while(turn < -360) turn += 360;
		
		// convert to HSV
		final float max = Math.max(Math.max(r, g), b);
		final float min = Math.min(Math.min(r, g), b);
		final float delta = max - min;
		
		float t;
		if(max == min) {
			t = 0;
		}
		else if(max == r) {
			t = (60 * ((g-b)/delta) + 360)%360;
		}
		else if(max == g) {
			t = (60 * ((b-r)/delta) + 120);
		}
		else {
			t = (60 * ((r - g)/delta) + 240);
		}
		
		float s = (max == 0)? 0 : 1 - (min/max);
		
		float v = max;
		
		// lighter
		// t = (t+turn + 360)%360;
		t = (t+BRIGHTER)%360;
		
		
		// retour a rgb
		int ti =  ((int)(t/60.f))%6;
		float f = t/60.f - ti;
		float l = v*(1 -s);
		float m = v*(1 - f *s);
		float n = v*(1 - (1 -f)*s);
		switch(ti) {
		case 0: return new Color3(v,n,l,a);
		case 1: return new Color3(m,v,l,a);
		case 2: return new Color3(l,v,n,a);
		case 3: return new Color3(l,m,v,a);
		case 4: return new Color3(n,l,v,a);
		case 5: return new Color3(v,l,m,a);
		default: return new Color3(this);
		}
		
	}

	public Color toColor() {
		return new Color(r, g, b, a);
	}

	public void scale(float d) {
		this.r *= d;
		this.g *= d;
		this.b *= d;
		
	}
	
	
	public void scaleAdd(double r) {
		this.r += (float)r;
		this.g += (float)r;
		this.b += (float)r;
	}

	public void scale(double d) {
		scale((float)d);
	}

	public Color3 brighter(float i) {
		float backup = BRIGHTER;
		BRIGHTER = i;
		Color3 c = brighter();
		BRIGHTER = backup;
		return c;
	}
	
	public void add(Color3 c) {
		this.r += c.r;
		this.g += c.g;
		this.b += c.b;
		this.a += c.a;
	}

	@Override
	public void scaleVect(Number coef) {
		float d = coef.floatValue();
		this.r *= d;
		this.g *= d;
		this.b *= d;
		this.a *= d;
	}

	@Override
	public void addVect(VectorialEbd ebdVect) {
		if (ebdVect.getClass() == getClass())
			add((Color3) ebdVect);
	}

	@Override
	public void clampVect() {
		this.r = Math.min(Math.max(r, 0), 1);
		this.g = Math.min(Math.max(g, 0), 1);
		this.b = Math.min(Math.max(b, 0), 1);
		this.a = Math.min(Math.max(a, 0), 1);
		
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Color3) {
			Color3 c = (Color3)o;
			return ((Math.abs(c.r-r)<=EPSILON)
					&&(Math.abs(c.g-g)<=EPSILON)
					&&(Math.abs(c.b-b)<=EPSILON)
					&&(Math.abs(c.a-a)<=EPSILON));
		}
		return false;
	}
	
	
}
