package fr.up.xlim.ig.jerboa.studio.infer.geometry.solvers;

import java.util.ArrayList;
import java.util.List;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.FPExpr;
import com.microsoft.z3.FPNum;
import com.microsoft.z3.FPRMExpr;
import com.microsoft.z3.FPSort;
import com.microsoft.z3.Model;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;
import com.microsoft.z3.StringSymbol;

import fr.up.xlim.ig.jerboa.ebds.Point3;
import up.jerboa.core.util.Pair;

/**
 * Z3-based implementation of the linear programming solution for the
 * geometric inference.
 * 
 * This was for testing purposes and probably no longer used.
 * 
 * @author Romain and Hakim
 *
 */
public class LPsolverZ3 implements LPsolver {
	
	@Override
	public Pair<List<Double>,Point3> guessCoefsSystem(List<Pair<List<Point3>, Point3>> equationSystem) throws LPSolverException{
		Pair<List<Point3>, Point3> first = equationSystem.get(0);
		List<Point3> pts = first.l();
		Point3 d = first.r();
		return guessCoefs(pts, d);
	}

	@Override
	public Pair<List<Double>,Point3> guessCoefs(List<Point3> pts, Point3 d) throws LPSolverException {
		Context context = new Context();
		Solver solver = polynome(context, pts, d);
		// System.out.println("SOLVER: Vars: ");
		Status status = solver.check();
		System.out.println("STATUS: " + status);
		List<Double> res = new ArrayList<>();
		Point3 translation;
		if(status == Status.SATISFIABLE) {
			Model model = solver.getModel();
			System.out.println("MODEL: " + model);
 
			{
				for(int i = 0; i < pts.size(); ++i) {
					float ai = extractValueOf(context, model, "a" + i);
					res.add((double)ai);	
				}
				for(int dim = 0; dim < 3; ++dim) {
					for(int i = 0; i < pts.size(); ++i) {
						Point3 p = pts.get(i);
						double ai = res.get(i);
						System.out.print((i==0? " " : " + ") +ai + " * " + p.get(dim));
					}
					System.out.println(" = " + d.get(dim));
				}
				
				float cte0 = extractValueOf(context, model, "cte0");
				float cte1 = extractValueOf(context, model, "cte1");
				float cte2 = extractValueOf(context, model, "cte2");
				translation = new Point3(cte0, cte1, cte2);
				System.out.println("TRANSLATION: " + translation);
			}
		}
		else
			throw new LPSolverException();
		return new Pair<>(res,translation);
	}
	
	private static Solver polynome(Context ctx, List<Point3> maillage, Point3 ex) {
		FPRMExpr roundingmode =  ctx.mkFPRoundNearestTiesToEven();
		FPSort tfloat = ctx.mkFPSort32();
		BoolExpr eqx = linearEq(ctx,maillage,ex,0);
		BoolExpr eqy = linearEq(ctx,maillage,ex,1);
		BoolExpr eqz = linearEq(ctx,maillage,ex,2);
		
		
		final FPExpr zero = ctx.mkFPZero(tfloat, false);
		String ax = "a";
		int index = 0;
		FPExpr norm = zero;
		for(Point3 p : maillage) {
			StringSymbol saxi = ctx.mkSymbol(ax + (index++));
			FPExpr axi = (FPExpr) ctx.mkConst(saxi, tfloat);
			
			norm = ctx.mkFPAdd(roundingmode, axi, norm);
		}
		FPExpr absnorm = ctx.mkFPAbs(norm);
		BoolExpr consistence = ctx.mkFPGt(absnorm, zero);
		
		
		Solver solver = ctx.mkSolver("QF_FP");
		
		solver.add(ctx.mkAnd(eqx,eqy,eqz, consistence));
		return solver;
	}

	private static BoolExpr linearEq(Context ctx,List<Point3> maillage, Point3 ex, int dim) {
		String ax = "a";
		int index = 0;
		FPSort tfloat = ctx.mkFPSort32();
		FPRMExpr roundingmode =  ctx.mkFPRoundNearestTiesToEven();
		
		final FPExpr zero = ctx.mkFPZero(tfloat, false);
		
		ArrayList<FPExpr> coefs = new ArrayList<>();
		
		for(Point3 p : maillage) {
			StringSymbol saxi = ctx.mkSymbol(ax + (index++));
			FPExpr axi = (FPExpr) ctx.mkConst(saxi, tfloat);
			FPNum xi = ctx.mkFP(p.get(dim), tfloat);

			FPExpr axi_xi = ctx.mkFPMul(roundingmode, axi, xi);
			coefs.add(axi_xi);
		}
		
		FPExpr sum = (FPExpr) ctx.mkConst("cte"+dim, tfloat);
		
		for (FPExpr t : coefs) {
			sum = ctx.mkFPAdd(roundingmode, sum, t); 
		}
		
		FPNum dxi = ctx.mkFP(ex.get(dim), tfloat);
		BoolExpr lineq = ctx.mkFPEq(sum, dxi);
		lineq = ctx.mkAnd(lineq);
				
		return lineq;
	}
	
	

	private static float extractValueOf(Context context, Model model, String name) {
		FPNum num = (FPNum) model.getConstInterp(context.mkConst(name, context.mkFPSort32()));
		return convertFPNumToFloat(num);
	}
	
	private static float convertFPNumToFloat(FPNum num) {
		if (num.isNaN())
			return Float.NaN;
		if(num.isInf()) {
			if(num.isPositive())
				return Float.POSITIVE_INFINITY;
			else if(num.isNegative())
				return Float.NEGATIVE_INFINITY;
		}
		if(num.isZero())
			return 0;
				
		String exponent = num.getExponent(false); // il faut que se soit biaise cf wikipedia IEE 754
		String mantisse = num.getSignificand();
		
		int e = Integer.parseInt(exponent);
		double m = Double.parseDouble(mantisse);
		
		double v = Math.pow(2, e) * (m) ;
		if(num.getSign())
			v *= -1;
		return (float)v;
	}
}
