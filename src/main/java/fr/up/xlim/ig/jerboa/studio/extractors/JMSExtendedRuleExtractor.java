package fr.up.xlim.ig.jerboa.studio.extractors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fr.up.xlim.ig.jerboa.studio.graph.JMSSideMark;
import fr.up.xlim.ig.jerboa.studio.infer.JMSExtendedRule;
import fr.up.xlim.ig.jerboa.studio.infer.parameters.JerboaInferRuleParameters;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSFoldGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSFoldSeed;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.foldable.JMSRuleNode;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoDart;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEGraph;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMEModeler;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENodeKind;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMERuleAtomic;
import up.jerboa.core.JerboaDart;

/**
 * Extract information from a folded representation of a graph to build a mapped
 * rule, i.e., a JMERule together with the left and right mappings (from the
 * initial G-maps)
 * 
 * @author romain
 *
 */
public class JMSExtendedRuleExtractor {

	private HashMap<JMSRuleNode, JMENode> conversion;
	private JMSFoldGraph jmsFoldGraph;
	private JMSExtendedRule extRule;
	private int nextAvailableRightNodeID;
	private JerboaInferRuleParameters inferenceParameters;

	private JMSExtendedRuleExtractor(JMSFoldGraph jmsFoldGraph,
			JMEModeler moded, String category, JMSRuleGraph ruleGraph, JerboaInferRuleParameters inferenceParameters) {
		this.conversion = new HashMap<JMSRuleNode, JMENode>();
		this.jmsFoldGraph = jmsFoldGraph;
		JMERuleAtomic jmeRule = new JMERuleAtomic(moded, ruleGraph.getName());
		jmeRule.setCategory(category);
		jmeRule.setUpdateExprs(false);
		this.extRule = new JMSExtendedRule(jmeRule);
		this.nextAvailableRightNodeID = 0;
		this.inferenceParameters = inferenceParameters;
	}

	private JMSExtendedRule getExtRule() {
		return extRule;
	}

	private JMERuleAtomic getJMERule() {
		return extRule.getRule();
	}

	private JMENode getConvertedNode(JMSRuleNode ruleNode) {
		return conversion.get(ruleNode);
	}

	private JMEGraph getEditorGraph(JMSRuleNode ruleNode) {
		JMEGraph editorGraph = (ruleNode.getAssocDart()
				.getMark() == JMSSideMark.Left) ? getJMERule().getLeft()
						: getJMERule().getRight();
		return editorGraph;
	}

	private void updateNextAvailableRightNodeID() {
		nextAvailableRightNodeID = getJMERule().getLeft().getNodes().size();
	}

	private int getNextAvailableRightNodeID() {
		return nextAvailableRightNodeID++;
	}

	/**
	 * Sub-routine to add a node to the rule.
	 * 
	 * @param ruleNode : node (in JMSRuleNode format) that should be added.
	 * @return the created JMENode.
	 */
	private JMENode addJMENode(JMSRuleNode ruleNode) {
		JMENode node = getEditorGraph(ruleNode).creatNode(0, 0);
		node.setOrbit(ruleNode.getOrbit());
		conversion.put(ruleNode, node);
		return node;
	}

	/**
	 * Builder of {@link JMSExtendedRule} from a rule expressed as a
	 * {@link JMSRuleGraph}.
	 * 
	 * @param jmsFoldGraph : foldGraph used for the folding
	 * @param moded        : modeller editor for the created mapped rule.
	 * @param category     : category of the created mapped rule.
	 * @param ruleGraph    : JMSRuleGraph to build the mapped rule from.
	 * @param seedDarts    : list of darts from the seed used to order the
	 *                     mapping
	 * @param inferenceParameters 
	 * @return the created extended rule with the mappings.
	 */
	public static JMSExtendedRule buildMappingFromFoldedGraph(
			JMSFoldGraph jmsFoldGraph, JMEModeler moded, String category,
			JMSRuleGraph ruleGraph, List<JMSTopoDart> seedDarts, JerboaInferRuleParameters inferenceParameters) {

		JMSExtendedRuleExtractor extractor = new JMSExtendedRuleExtractor(
				jmsFoldGraph, moded, category, ruleGraph, inferenceParameters);
		
		extractor.extractNodes(ruleGraph);
		extractor.extractArcs(ruleGraph);
		extractor.updateNextAvailableRightNodeID();
		extractor.fixNodeNaming(ruleGraph);
		extractor.fixMissingHooks();

		extractor.fixMapping(ruleGraph, seedDarts);

		return extractor.getExtRule();
	}

	/**
	 * Extract nodes from the rule graph and add them to the corresponding graph
	 * of the JMERule.
	 * 
	 * @param ruleGraph : JMSRuleGraph from which the nodes should be extracted.
	 */
	private void extractNodes(JMSRuleGraph ruleGraph) {
		for (JMSRuleNode ruleNode : ruleGraph.getDarts()) {
			JMENode jmeNode = addJMENode(ruleNode);
			if (ruleNode instanceof JMSFoldSeed)
				jmeNode.setKind(JMENodeKind.HOOK);
		}
	}

	/**
	 * Extract arcs from the rule graph and add them to the corresponding graph
	 * of the JMERule.
	 * 
	 * @param ruleGraph : JMSRuleGraph from which the arcs should be extracted.
	 */
	private void extractArcs(JMSRuleGraph ruleGraph) {
		
		Set<Integer> posteriorCut = inferenceParameters.parsePosteriorCut();
		
		for (JMSRuleNode ruleNode : ruleGraph.getDarts()) {
			JMEGraph editorGraph = getEditorGraph(ruleNode);
			JMENode jmeNode = getConvertedNode(ruleNode);

			for (int incidentDim : ruleNode.getIncidentDims()) {
				if (incidentDim == JMSRuleGraph.kappa)
					continue; // taken care of afterwards to avoid conflicts

				if (ruleNode.isLoop(incidentDim) && !(posteriorCut.contains(incidentDim))) {
					editorGraph.creatLoop(jmeNode, incidentDim);
					continue;
				}

				JMSRuleNode targetNode = ruleNode.getNeighbour(incidentDim);
				if (targetNode.getID() < ruleNode.getID()) {
					JMENode targetJMENode = getConvertedNode(targetNode);
					editorGraph.creatArc(jmeNode, targetJMENode, incidentDim);
				}
			}
		}
	}

	/**
	 * Fix node naming when to ensure the offset between right node names and
	 * left node names. Also fix the preserved node names.
	 * 
	 * @param ruleGraph : graph whose nodes need name fixing.
	 */
	private void fixNodeNaming(JMSRuleGraph ruleGraph) {
		for (JMSRuleNode ruleNode : ruleGraph.getRightNodes()) {
			JMENode rightNode = getConvertedNode(ruleNode);
			if (ruleNode.isNodePreserved())
				rightNode.setName(getConvertedNode(ruleNode.getPreservedTwin())
						.getName());
			else
				rightNode.setName("n" + (getNextAvailableRightNodeID()));
		}
	}

	/**
	 * Try to add a hook to connected components without one (take any node with
	 * full orbit).
	 */
	private void fixMissingHooks() {
		getJMERule().enforceHooks();
	}

	/**
	 * Build the left and right mapping between the node in the created
	 * JMERuleAtomic and the darts from the initial left and right instances.
	 * 
	 * @param ruleGraph : JMSRuleGraph to build the mapping from.
	 * @param seedDarts
	 */
	private void fixMapping(JMSRuleGraph ruleGraph,
			List<JMSTopoDart> seedDarts) {
		Map<JMSTopoDart, JerboaDart> mappingL = jmsFoldGraph.getMappingL();
		Map<JMSTopoDart, JerboaDart> mappingR = jmsFoldGraph.getMappingR();
		for (JMSRuleNode rNode : conversion.keySet()) {

			if (rNode.isInLeftGraph()) {
				extRule.getMappingL().put(conversion.get(rNode),
						rNode.orbitDartsOrderedfromSeed(seedDarts).stream()
								.map(d -> mappingL.get(d))
								.collect(Collectors.toList()));
			}

			else {
				extRule.getMappingR().put(conversion.get(rNode),
						rNode.orbitDartsOrderedfromSeed(seedDarts).stream()
								.map(d -> mappingR.get(d))
								.collect(Collectors.toList()));
			}
		}
	}

}
