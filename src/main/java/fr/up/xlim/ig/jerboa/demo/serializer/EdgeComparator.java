package fr.up.xlim.ig.jerboa.demo.serializer;

import java.util.Comparator;

import fr.up.xlim.ig.jerboa.ebds.Normal3;
import fr.up.xlim.ig.jerboa.ebds.Point3;


public class EdgeComparator implements Comparator<Edge> {
	private OBJImporter owner;
	private Normal3 refnorm;
	private Point3 vec;

	public EdgeComparator(OBJImporter objImporter, Edge base) {
		owner = objImporter;
		refnorm = base.owner.geonorm;
		Point3 a = owner.getPoint(base.start-1);
		Point3 b = owner.getPoint(base.end-1);
		
		vec = new Point3(a, b);
	}

	@Override
	public int compare(Edge e0, Edge e1) {
		double a = vec.angle(refnorm, e0.owner.geonorm);
		double b = vec.angle(refnorm, e1.owner.geonorm);
		return Double.compare(a, b);
	}

}
