package fr.up.xlim.ig.jerboa.studio.infer.topo.foldable;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.up.xlim.ig.jerboa.studio.graph.JMSDartBasedGraph;
import fr.up.xlim.ig.jerboa.studio.infer.topo.isomorphism.JMSRuleIsomorphism;
import fr.up.xlim.ig.jerboa.studio.infer.topo.isomorphism.JMSRuleNodeSignature;

/**
 * Represent a (scheme) rule as a graph. The LHS and RHS are linked using kappa
 * arcs.
 * 
 * @author romain
 *
 */
public class JMSRuleGraph implements JMSDartBasedGraph<Integer, JMSRuleNode> {

	public final static int kappa = -1;

	private String name;

	private HashSet<JMSRuleNode> darts;
	private int minDim;
	private int maxDim;

	private int availableID;

	private HashMap<JMSRuleNodeSignature, Integer> signatureOccurrences;
	private HashMap<JMSRuleNodeSignature, HashSet<JMSRuleNode>> signatureToNode;
	private boolean signaturesOutOfDate;

	public JMSRuleGraph(JMSFoldGraph graph, String name) {
		this.name = name;
		this.darts = new HashSet<JMSRuleNode>();
		this.minDim = graph.getMinDim();
		this.maxDim = graph.getMaxDim();
		this.availableID = 0;
		this.signaturesOutOfDate = true;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((darts == null) ? 0 : darts.hashCode());
		result = prime * result + maxDim;
		result = prime * result + minDim;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JMSRuleGraph other = (JMSRuleGraph) obj;
		if (darts == null) {
			if (other.darts != null)
				return false;
		} else if (!darts.equals(other.darts))
			return false;
		if (maxDim != other.maxDim)
			return false;
		if (minDim != other.minDim)
			return false;
		return true;
	}

	@Override
	public boolean noDart() {
		return size() == 0;
	}

	@Override
	public int size() {
		return darts.size();
	}

	@Override
	public Collection<JMSRuleNode> getDarts() {
		return darts;
	}

	@Override
	public JMSRuleNode getAnyDart() {
		return darts.iterator().next();
	}

	@Override
	public boolean hasDart(JMSRuleNode dart) {
		return darts.contains(dart);
	}

	@Override
	public boolean labelValid(Integer dim) {
		return dim >= minDim || dim <= maxDim;
	}

	@Override
	public void addDart(JMSRuleNode dart) {
		if (hasDart(dart)) {
			System.err
					.println("Error: A dart with the same ID already exists.");
			dart.setID(availableID);
		}
		if (dart.getID() >= availableID)
			availableID = dart.getID() + 1;
		signaturesOutOfDate = true;
		darts.add(dart);
		dart.setGraph(this);
	}

	@Override
	public void removeDart(JMSRuleNode dart) {
		signaturesOutOfDate = true;
		darts.remove(dart);
	}

	@Override
	public void addArc(JMSRuleNode sourceDart, JMSRuleNode targetDart,
			Integer dim) {
		if (!labelValid(dim))
			return;
		signaturesOutOfDate = true;
		if (!sourceDart.hasNeighbour(dim) && !targetDart.hasNeighbour(dim)) {
			sourceDart.addArc(targetDart, dim);
			targetDart.addArc(sourceDart, dim);
		}

	}

	@Override
	public void removeArc(JMSRuleNode dart, Integer label) {
		JMSRuleNode target = (JMSRuleNode) dart.getNeighbour(label);
		if (target != null) {
			signaturesOutOfDate = true;
			target.removeArc(dart, label);
			dart.removeArc(target, label);
		}
	}

	@Override
	public boolean equalsAllDarts(Collection<JMSRuleNode> dartCollection) {
		HashSet<JMSRuleNode> toTest = new HashSet<>(dartCollection);
		return darts.equals(toTest);
	}

	/**
	 * Array of dimensions (i.e. all dims between minDim and maxDim included).
	 * 
	 * @return array of dimensions.
	 */
	public int[] getDims() {
		return IntStream.rangeClosed(minDim, maxDim).toArray();
	}

	/**
	 * Get the nodes that comes from the LHS.
	 * 
	 * @return collection of nodes
	 */
	public Collection<JMSRuleNode> getLeftNodes() {
		return getDarts().stream()
				.filter(node -> node.assocDart.isInLeftGraph())
				.collect(Collectors.toSet());
	}

	/**
	 * Get the nodes that comes from the RHS.
	 * 
	 * @return collection of nodes
	 */
	public Collection<JMSRuleNode> getRightNodes() {
		return getDarts().stream()
				.filter(node -> !node.assocDart.isInLeftGraph())
				.collect(Collectors.toSet());
	}

	/**
	 * Build signatures of each node.
	 */

	private void buildSignature() {
		signatureOccurrences = new HashMap<JMSRuleNodeSignature, Integer>();
		signatureToNode = new HashMap<JMSRuleNodeSignature, HashSet<JMSRuleNode>>();
		for (JMSRuleNode node : darts) {
			JMSRuleNodeSignature signature = node.getSignature();
			signatureOccurrences.merge(signature, 1, Integer::sum);
			signatureToNode.computeIfAbsent(signature, k -> new HashSet<>())
					.add(node);
		}
		signaturesOutOfDate = false;
	}

	/**
	 * Count the number of nodes per signature.
	 * 
	 * @return Map associating the number of nodes to each signature (existing
	 *         in the graph).
	 */

	public HashMap<JMSRuleNodeSignature, Integer> getSignatureOccurrences() {
		if (signaturesOutOfDate)
			buildSignature();
		return signatureOccurrences;
	}

	/**
	 * Nodes per signature.
	 * 
	 * @return Map associating the nodes to each signature (existing in the
	 *         graph).
	 */

	public HashMap<JMSRuleNodeSignature, HashSet<JMSRuleNode>> getSignatureToNode() {
		if (signaturesOutOfDate)
			buildSignature();
		return signatureToNode;
	}

	/**
	 * Determine whether two graphs are isomorphic (and thus whether the
	 * corresponding rules are also isomorphic).
	 * 
	 * @param otherGraph : graph to be compared with the current graph.
	 * 
	 * @return true if the two graphs are isomorphic.
	 */
	public boolean isIsomorphic(JMSRuleGraph otherGraph) {
		// TODO we could implement VF2, cf :
		// https://github.com/jgrapht/jgrapht/tree/master/jgrapht-core/src/main/java/org/jgrapht/alg/isomorphism

		// General values
		if (size() != otherGraph.size())
			return false;

		JMSRuleIsomorphism ruleIso = new JMSRuleIsomorphism(this, otherGraph);

		// Vertex Invariants (see Fortin 1996 "The Graph Isomorphism Problem")
		if (!ruleIso.checkVertexInvariants())
			return false;

		return ruleIso.testIsomorphism();
	}
}
