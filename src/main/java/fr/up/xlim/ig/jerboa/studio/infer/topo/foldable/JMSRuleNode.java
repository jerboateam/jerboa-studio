package fr.up.xlim.ig.jerboa.studio.infer.topo.foldable;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import fr.up.xlim.ig.jerboa.studio.graph.JMSDart;
import fr.up.xlim.ig.jerboa.studio.infer.parameters.JerboaInferRuleParameters;
import fr.up.xlim.ig.jerboa.studio.infer.topo.graph.JMSTopoDart;
import fr.up.xlim.ig.jerboa.studio.infer.topo.isomorphism.JMSRuleNodeSignature;
import up.jerboa.core.JerboaOrbit;

/**
 * This class is used to represent a node from a rule scheme for the folding
 * process.
 * 
 * It stores the orbit of the nodes in the rule scheme but also the darts that
 * are mapped to this node and a mapping of the seed.
 * 
 * Note that for the folding algorithm, we need to be able to create such node
 * without them being in a graph.
 * 
 * @author romain
 *
 */
public class JMSRuleNode implements JMSDart<Integer> {

	protected HashMap<Integer, JMSRuleNode> neighbours;
	protected int ID;
	protected JMSRuleGraph graph;
	protected JMSTopoDart assocDart;
	protected JerboaOrbit orbit;
	protected HashMap<JMSTopoDart, JMSTopoDart> seedMap;

	JMSRuleNodeSignature signature;
	boolean modified;

	/**
	 * The constructor do not add the dart to the graph.
	 * 
	 * @param topoDart : a reference dart (mapped to this node)
	 */
	private JMSRuleNode(JMSTopoDart topoDart) {
		this.assocDart = topoDart;
		this.ID = topoDart.getID();
		this.neighbours = new HashMap<Integer, JMSRuleNode>();
		this.orbit = null;
		this.seedMap = new HashMap<JMSTopoDart, JMSTopoDart>();
		modified = true;
	}

	/**
	 * 
	 * Constructor for the seed (the orbit type is specified and we compute the
	 * orbits darts).
	 * 
	 * @param topoDart : a reference dart (mapped to this node).
	 * @param orbit    : the orbit type of the node.
	 */
	protected JMSRuleNode(JMSTopoDart topoDart, JerboaOrbit orbit) {
		this(topoDart);
		this.orbit = orbit;
	}

	/**
	 * Generic constructor. Build a new dart-orbit association by extension from
	 * another one, given an arc dimension to extend from.
	 * 
	 * Note that the JerboaOrbit has to be created afterwards.
	 * 
	 * @param previousNode : previous node to extend from. Should be an image of
	 *                     the seed.
	 * @param arcDimExtended  : dimension used for the extension
	 */
	public JMSRuleNode(JMSRuleNode previousNode, int arcDimExtended) {
		this(previousNode.getAssocDart().getNeighbour(arcDimExtended));
		for (JMSTopoDart seedDart : previousNode.seedMap.keySet()) {
			JMSTopoDart seedEquivalentDart = previousNode.seedMap.get(seedDart)
					.getNeighbour(arcDimExtended);
			if (seedEquivalentDart == null) {
				System.err.println("mapping to no dart");
				System.out.println(previousNode);
				System.out.println("dim" + arcDimExtended);
				System.out.println("node" + seedDart);
			}
			this.seedMap.put(seedDart, seedEquivalentDart);
		}
	}

	/**
	 * Built the orbit of the node by computing a dimension map (i.e. the
	 * relabelling function) from the seed.
	 * 
	 * The mapping is done in the reserve way (compared to
	 * buildOrbitAsSeedMappingOld). We map the incident dimensions into the seed
	 * dimensions.
	 * 
	 * @param seed                : seed used as reference
	 * @param inferRuleParameters : inference parameter, e.g., whether or not to
	 *                            exclude loops (i.e. when all darts have loops
	 *                            for a given dimension) in the relabelling
	 *                            function.
	 */
	public void buildOrbitAsSeedMapping(JMSFoldSeed seed,
			JerboaInferRuleParameters inferRuleParameters) {
		if (!seed.getOrbitDarts().equals(seedMap.keySet())) {
			System.err.println("Mapping is not built from the proper seed");
			return;
		}

		JMSBuildOrbitAsMapping orbitBuilder = new JMSBuildOrbitAsMapping(this,
				inferRuleParameters);
		this.orbit = orbitBuilder.buildOrbitAsSeedMapping(seed);
		modified = true;
	}

	public int getID() {
		return ID;
	}

	protected void setID(int ID) {
		this.ID = ID;
	}

	public JMSTopoDart getAssocDart() {
		return assocDart;
	}

	public boolean isInLeftGraph() {
		return assocDart.isInLeftGraph();
	}

	public JMSRuleGraph getGraph() {
		return graph;
	}

	protected void setGraph(JMSRuleGraph graph) {
		this.graph = graph;
	}

	public JerboaOrbit getOrbit() {
		return orbit;
	}

	public boolean isNeighbour(JMSRuleNode node) {
		return neighbours.containsValue(node);
	}

	@Override
	public int hashCode() { // TODO: add more elements ?
		return ID;
	}

	@Override
	public boolean equals(Object obj) { // TODO: add more elements ?
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JMSRuleNode other = (JMSRuleNode) obj;
		if (ID != other.ID)
			return false;
		return true;
	}

	/**
	 * For each loop incident to the associated dart, a loop is added to the
	 * node.
	 * 
	 * This function can only be safely used after checking the incidence sets.
	 */
	public void extractLoops() {
		for (int loopDim : assocDart.getLoops()) {
			if (!orbitDimsContains(loopDim)) {
				addArc(this, loopDim);
			}
		}
	}

	@Override
	public void addArc(JMSDart<Integer> dart, Integer dim) {
		if (!(dart instanceof JMSRuleNode))
			return;
		modified = true;
		neighbours.put(dim, (JMSRuleNode) dart);
	}

	@Override
	public boolean removeArc(Integer dim) {
		boolean arcRemoved = neighbours.remove(dim) != null;
		modified = modified && arcRemoved;
		return arcRemoved;
	}

	@Override
	public boolean removeArc(JMSDart<Integer> dart, Integer label) {
		boolean arcRemoved = neighbours.remove(label, (JMSTopoDart) dart);
		modified = modified && arcRemoved;
		return arcRemoved;
	}

	@Override
	public boolean hasNeighbour(Integer dim) {
		return neighbours.containsKey(dim);
	}

	@Override
	public JMSRuleNode getNeighbour(Integer dim) {
		return neighbours.get(dim);
	}

	/**
	 * The size of a node is the number of topological dart associated to it.
	 * 
	 * @return the size of the map.
	 */
	public int size() {
		return seedMap.size();
	}

	/**
	 * Return the dimension of the orbit (i.e. all actual values, excluding
	 * NOVALUE).
	 * 
	 * @return the set of dimensions
	 */
	public Set<Integer> getOrbitDims() {
		Set<Integer> orbitDims = Arrays.stream(orbit.tab()).boxed()
				.collect(Collectors.toSet());
		orbitDims.remove(JerboaOrbit.NOVALUE);
		return orbitDims;
	}

	/**
	 * Check if the orbit of the node contains a dimension
	 * 
	 * @param dim : dimension to be checked
	 * @return true if the dimension is in the orbit
	 */
	public boolean orbitDimsContains(int dim) {
		return getOrbitDims().contains(dim);
	}

	/**
	 * Return the set of darts in the association.
	 * 
	 * @return set of darts
	 */
	public Set<JMSTopoDart> getOrbitDarts() {
		return seedMap.values().stream().collect(Collectors.toSet());
	}

	/**
	 * Get the dart equivalent to a given seed dart, i.e. the dart that would be
	 * obtained by following the path from the seed for a given dart of the
	 * seed.
	 *
	 * @param seedDart : dart from the seed node
	 * @return : equivalent dart for the node
	 */
	public JMSTopoDart getSeedEquivalent(JMSTopoDart seedDart) {
		return seedMap.get(seedDart);
	}

	/**
	 * Build the set of darts that correspond to the image of the orbit graph of
	 * the seed for the path that link the dart from the seed dart.
	 * 
	 * When used on a seed, the algorithm is different and build the orbit
	 * graph.
	 * 
	 * @return set of dart IDs
	 */
	public Set<Integer> getOrbitDartIDs() {
		return this.getOrbitDarts().stream().map(dart -> dart.getID())
				.collect(Collectors.toSet());
	}

	/**
	 * Check is the orbit is full (i.e. contains a dimension in each possible
	 * placeholder position.
	 * 
	 * @return true if the orbit is full
	 */
	public boolean isOrbitFull() {
		return !orbit.contains(JerboaOrbit.NOVALUE);
	}

	/**
	 * Check if the node is preserved (all corresponding darts would thus be
	 * preserved).
	 * 
	 * @return true if the dart is preserved.
	 */
	public boolean isNodePreserved() {
		return hasNeighbour(JMSRuleGraph.kappa);
	}

	/**
	 * Get the twin dart for a preserved node.
	 * 
	 * @return the corresponding dart. NULL if the node is not preserved.
	 */
	public JMSRuleNode getPreservedTwin() {
		if (isNodePreserved())
			return getNeighbour(JMSRuleGraph.kappa);
		return null;
	}

	/**
	 * Get the dimensions of arc to be extended in the folding process. The
	 * implicit and explicit dimensions should be checked beforehand.
	 * 
	 * The returned set of dimension is a subset of the dimensions incident to
	 * the associated dart.
	 */
	public Set<Integer> dimsForArcExtension() {
		HashSet<Integer> assocDartIncidentDims = new HashSet<>();

		for (int dim : assocDart.getIncidentDims())
			if (!assocDart.isLoop(dim) && !orbitDimsContains(dim)
					&& !hasNeighbour(dim))
				assocDartIncidentDims.add(dim);
		return assocDartIncidentDims;
	}

	public Collection<Integer> getIncidentDims() {
		return neighbours.keySet();
	}

	public boolean isLoop(int dim) {
		return hasNeighbour(dim) && getNeighbour(dim).equals(this);
	}

	/**
	 * Build signature for the isomorphism computation.
	 */
	private void buildSignature() {
		signature = new JMSRuleNodeSignature(this);
		modified = false;
	}

	/**
	 * Get the node signature for the isomorphism computation. The signature
	 * should only be recomputed if it was modified.
	 * 
	 * @return the node signature
	 */
	public JMSRuleNodeSignature getSignature() {
		if (modified)
			buildSignature();
		return signature;
	}

	/**
	 * Return the darts in the association, in a list ordered similarly to the
	 * list given.
	 * 
	 * Expects a list of dart from the seed.
	 * 
	 * @return list of darts
	 */
	public List<JMSTopoDart> orbitDartsOrderedfromSeed(
			List<JMSTopoDart> seedOrdering) {
		return seedOrdering.stream().map(seedDart -> seedMap.get(seedDart))
				.collect(Collectors.toList());
	}

}