package fr.up.xlim.ig.jerboa.bridge;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.up.xlim.ig.jerboa.demo.bridge.net.JerboaStudioConverter;
import fr.up.xlim.ig.jerboa.demo.serializer.JBASerializer;
import fr.up.xlim.ig.jerboa.demo.serializer.MokaOrientSerializer;
import fr.up.xlim.ig.jerboa.demo.serializer.OBJImporter;
import fr.up.xlim.ig.jerboa.ebds.Color3;
import fr.up.xlim.ig.jerboa.ebds.Point3;
import fr.up.xlim.ig.jerboa.studio.JerboaModelerStudio;
import fr.up.xlim.ig.jerboa.studio.JerboaStudio;
/*import fr.up.xlim.sic.ig.jerboa.modeler.basic.embedding.Color3;
import fr.up.xlim.sic.ig.jerboa.modeler.basic.embedding.Point3;
import fr.up.xlim.sic.ig.jerboa.modeler.basic.script.JerboaModeler3DS;
 */import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaMonitorInfo;
import fr.up.xlim.sic.ig.jerboa.trigger.tools.JerboaMonitorInfoConsole;
import fr.up.xlim.sic.ig.jerboa.viewer.IJerboaModelerViewer;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerBridge;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerColor;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerPoint;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.GMapViewerTuple;
import fr.up.xlim.sic.ig.jerboa.viewer.tools.JerboaMonitorInfoBridgeSerializerMonitor;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaGMapArray;
import up.jerboa.core.JerboaMark;
import up.jerboa.core.JerboaModeler;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.core.JerboaRuleAtomic;
import up.jerboa.core.rule.engine.JerboaRuleEngine;
import up.jerboa.core.util.JerboaGMapDuplicateFactory;
import up.jerboa.core.util.JerboaIslet;
import up.jerboa.core.util.Pair;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaGMapDuplicateException;
import up.jerboa.exception.JerboaNoFreeTagException;
import up.jerboa.util.JerboaSerializerMonitor;
import up.jerboa.util.serialization.JerboaSerializeException;
import up.jerboa.util.serialization.jba.JBAEmbeddingSerialization;
import up.jerboa.util.serialization.jba.JBAFormat;
import up.jerboa.util.serialization.jba.JBCFormat;
import up.jerboa.util.serialization.jba.JBZFormat;
import up.jerboa.util.serialization.moka.MokaEmbeddingSerialization;
import up.jerboa.util.serialization.moka.MokaExtension;
import up.xlim.ig.jerboa.transmitter.JMVTransmitterServer;

 /**
  * @author Hakim Belhaouari
  *
  */
 public class ViewerBridge implements GMapViewerBridge, JerboaGMapDuplicateFactory {

	 private JerboaStudio.JerboaStudioPart studio;
	 private static final String ebdpoint = "position";
	 private static final String ebdcolor = "color";
	 
	 private JMVTransmitterServer<JerboaModelerStudio> transmitter;

	 private JBAEmbeddingSerialization factoryJBA;
	 private MokaEmbeddingSerialization factoryMOKA;
	 private OBJImporter factoryOBJ;


	 public ViewerBridge(JerboaStudio.JerboaStudioPart studio) {
		 this.studio = studio;
		 /*
		 this.modeler = studio.getModeler();
		 ebdPointID = modeler.getEmbedding("position").getID();
		 ebdColorID = modeler.getEmbedding("normal").getID();
*/
		 factoryJBA = new JBASerializer(studio.getModeler());
		 factoryMOKA = new MokaOrientSerializer(studio.getModeler());
		 factoryOBJ = new OBJImporter(studio.getModeler());
	 }

	 @Override
	 public boolean hasColor() {
		 return true;
	 }

	 @Override
	 public boolean hasNormal() {
		 return false;
	 }

	 @Override
	 public GMapViewerPoint coords(JerboaDart n) {
		 try {
			 Point3 p = n.<Point3> ebd(ebdpoint);

			 GMapViewerPoint res = new GMapViewerPoint((float) p.getX(), (float) p.getY(), (float) p.getZ());

			 return res;
		 } catch (NullPointerException e) {
			 // System.err.println("Error in node " + n.getID());
			 int size = getGMap().size();
			 // System.err.println("No coord, make a false one on size: " + size );
			 size = (int)Math.max(Math.sqrt(size),1);
			 return new GMapViewerPoint((float) (Math.random() * size),(float) (Math.random() * size),(float) (Math.random() * size));
		 }

	 }

	 @Override
	 public GMapViewerColor colors(JerboaDart n) {
		 try {
			 Color3 c = n.<Color3> ebd(ebdcolor);
			 GMapViewerColor res = new GMapViewerColor(c.getR(), c.getG(), c.getB(), c.getA());
			 return res;
		 }
		 catch(Exception e) {
			 return new GMapViewerColor(0.8f, 0.8f, 0.8f, 1.0f);
		 }
	 }

	 @Override
	 public GMapViewerTuple normals(JerboaDart n) {
		return null;
	 }

	 @Override
	 public void load(IJerboaModelerViewer view, JerboaMonitorInfo worker) {
		 JFileChooser filec = new JFileChooser(".");
		 FileFilter jbafilter = new FileNameExtensionFilter("JBA format (*.jba)", "jba");
		 filec.addChoosableFileFilter(jbafilter);
		 FileFilter jbzfilter = new FileNameExtensionFilter("JBZ format (*.jbz)", "jbz");
		 filec.addChoosableFileFilter(jbzfilter);
		 FileFilter jbcfilter = new FileNameExtensionFilter("JBC format (*.jbc)", "jbc");
		 filec.addChoosableFileFilter(jbcfilter);
		 FileFilter mokafilter = new FileNameExtensionFilter("Moka (*.moka;*.mok)", "moka", "mok");
		 filec.addChoosableFileFilter(mokafilter);
		 FileFilter offfilter = new FileNameExtensionFilter("OFF file (*.off)", "off");
		 filec.addChoosableFileFilter(offfilter);
		 FileFilter objfilter = new FileNameExtensionFilter("OBJ file (*.obj)", "obj");
		 filec.addChoosableFileFilter(objfilter);
		 filec.setFileFilter(jbafilter);


		 if (filec.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			 File file = filec.getSelectedFile();
			 FILEFORMAT fileformat;
			 if(filec.getFileFilter() == jbafilter) {
				 fileformat = FILEFORMAT.JERBOA;
			 }
			 else if(filec.getFileFilter() == jbzfilter) {
				 fileformat = FILEFORMAT.JERBOAZIP;
			 }
			 else if(filec.getFileFilter() == jbcfilter) {
				 fileformat = FILEFORMAT.JERBOACOMPLETE;
			 }
			 else if(filec.getFileFilter() == mokafilter)
				 fileformat = FILEFORMAT.MOKA;
			 else if(filec.getFileFilter() == objfilter)
				 fileformat = FILEFORMAT.OBJ;
			 else
				 fileformat = FILEFORMAT.AUTO;
			 loadFile(file, fileformat, worker);
		 }
	 }

	 public void loadFile(File file, FILEFORMAT kind,JerboaMonitorInfo worker) {
		 JerboaSerializerMonitor monitor = new JerboaMonitorInfoBridgeSerializerMonitor(worker);
		 if(kind == FILEFORMAT.AUTO) {
			 if(file.getName().toLowerCase().endsWith(".jba"))
				 kind = FILEFORMAT.JERBOA;
			 else if(file.getName().toLowerCase().endsWith(".jbz"))
				 kind = FILEFORMAT.JERBOAZIP;
			 else if(file.getName().toLowerCase().endsWith(".jbc"))
				 kind = FILEFORMAT.JERBOACOMPLETE;
			 else if(file.getName().toLowerCase().endsWith(".moka"))
				 kind = FILEFORMAT.MOKA;
			 else if(file.getName().toLowerCase().endsWith(".mok"))
				 kind = FILEFORMAT.MOKA;
			 else if(file.getName().toLowerCase().endsWith(".OBJ"))
				 kind = FILEFORMAT.OBJ;
			 else
				 kind = FILEFORMAT.JERBOA;
		 }
		 System.out.println("FORMAT: "+kind+" LOAD FILE: "+file);

		 try(FileInputStream fis = new FileInputStream(file)) {
			 switch(kind) {
			 case JERBOA:
				 JBAFormat formatJBA = new JBAFormat(studio.getModeler(), monitor, factoryJBA);
				 formatJBA.load(fis);
				 break;
			 case JERBOAZIP:
				 JBZFormat formatJBZ = new JBZFormat(studio.getModeler(), monitor, factoryJBA);
				 formatJBZ.load(fis);
				 break;
			 case JERBOACOMPLETE:
				 JBCFormat formatJBC = new JBCFormat(studio.getModeler(), monitor, factoryJBA);
				 formatJBC.load(fis);
				 break;
			 case MOKA:
				 MokaExtension formatMOKA = new MokaExtension(studio.getModeler(), monitor, factoryMOKA);
				 formatMOKA.load(fis);
				 break;
			 case OBJ:
				 factoryOBJ.parse(fis);
				 break;
			 case AUTO:
			 }	
		 } catch (FileNotFoundException e) {
			 e.printStackTrace();
		 } catch (IOException e) {
			 e.printStackTrace();
		 } catch (JerboaSerializeException e) {
			 e.printStackTrace();
		 } catch (JerboaException e) {
			 e.printStackTrace();
		 }

	 }

	 protected File checkFilename(File file, String def, String... exts) {
		 boolean find = false;
		 for (String ext : exts) {
			 if(file.getName().toLowerCase().endsWith(ext)) {
				 find = true;
			 }
		 }

		 if(!find) {
			 File parent = file.getParentFile();
			 file = new File(parent, file.getName()+def);
		 }
		 return file;
	 }

	 @Override
	 public void save(IJerboaModelerViewer view, JerboaMonitorInfo worker) {
		 JFileChooser filec = new JFileChooser();
		 FileFilter jbafilter = new FileNameExtensionFilter("JBA format (*.jba)", "jba");
		 filec.addChoosableFileFilter(jbafilter);

		 FileFilter jbzfilter = new FileNameExtensionFilter("JBZ format (*.jbz)", "jbz");
		 filec.addChoosableFileFilter(jbzfilter);
		 
		 FileFilter jbcfilter = new FileNameExtensionFilter("JBC format (*.jbc)", "jbc");
		 filec.addChoosableFileFilter(jbcfilter);

		 
		 FileFilter mokafilter = new FileNameExtensionFilter("Moka (*.moka;*.mok)", "moka", "mok");
		 filec.addChoosableFileFilter(mokafilter);
	
		 filec.setFileFilter(jbafilter);

		 if (filec.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			 File file = filec.getSelectedFile();
			 FILEFORMAT fileformat;
			 if(filec.getFileFilter() == jbafilter) {
				 fileformat = FILEFORMAT.JERBOA;
			 }
			 else if(filec.getFileFilter() == jbzfilter) {
				 fileformat = FILEFORMAT.JERBOAZIP;
			 }
			 else if(filec.getFileFilter() == jbcfilter) {
				 fileformat = FILEFORMAT.JERBOACOMPLETE;
			 }
			 else if(filec.getFileFilter() == mokafilter)
				 fileformat = FILEFORMAT.MOKA;
			 else
				 fileformat = FILEFORMAT.AUTO;
			 saveFile(file,fileformat,worker);
		 }

	 }

	 public void saveFile(File file, FILEFORMAT kind, JerboaMonitorInfo worker) {
		 JerboaSerializerMonitor monitor = new JerboaMonitorInfoBridgeSerializerMonitor(worker);
		 if(kind == FILEFORMAT.AUTO) {
			 if(file.getName().toLowerCase().endsWith(".jba"))
				 kind = FILEFORMAT.JERBOA;
			 else if(file.getName().toLowerCase().endsWith(".jbz"))
				 kind = FILEFORMAT.JERBOAZIP;
			 else if(file.getName().toLowerCase().endsWith(".jbc"))
				 kind = FILEFORMAT.JERBOACOMPLETE;
			 else if(file.getName().toLowerCase().endsWith(".moka"))
				 kind = FILEFORMAT.MOKA;
			 else if(file.getName().toLowerCase().endsWith(".mok"))
				 kind = FILEFORMAT.MOKA;
			 else
				 kind = FILEFORMAT.JERBOA;
		 }

		 switch(kind) {
		 case JERBOA:
			 file = checkFilename(file, ".jba", ".jba");
			 break;
		 case JERBOAZIP:
			 file = checkFilename(file, ".jbz", ".jbz");
			 break;
		 case JERBOACOMPLETE:
			 file = checkFilename(file, ".jbc", ".jbc");
			 break;
		 case MOKA:
			 file = checkFilename(file, ".moka", ".moka",".mok");
			 break;
		 case AUTO:
		 }

		 System.out.println("FORMAT: "+kind+" SAVE FILE: "+file);

		 try(FileOutputStream fos= new FileOutputStream(file)) {
			 switch(kind) {
			 case JERBOA:
				 JBAFormat formatJBA = new JBAFormat(studio.getModeler(), monitor, factoryJBA);
				 formatJBA.save(fos);
				 break;
			 case JERBOAZIP:
				 JBZFormat formatJBZ = new JBZFormat(studio.getModeler(), monitor, factoryJBA);
				 formatJBZ.save(fos);
				 break;
			 case JERBOACOMPLETE:
				 JBCFormat formatJBC = new JBCFormat(studio.getModeler(), monitor, factoryJBA);
				 formatJBC.save(fos);
				 break;
			 case MOKA:
				 MokaExtension formatMOKA = new MokaExtension(studio.getModeler(), monitor, factoryMOKA);
				 formatMOKA.save(fos);
				 break;
			 case AUTO:
			 }	
		 } catch (FileNotFoundException e) {
			 e.printStackTrace();
		 } catch (IOException e) {
			 e.printStackTrace();
		 } catch (JerboaSerializeException e) {
			 e.printStackTrace();
		 } catch (JerboaException e) {
			 e.printStackTrace();
		 }

	 }

	@Override
	 public boolean canUndo() {
		 return true;
	 }

	 @Override
	 public JerboaGMap duplicate(JerboaGMap gmap) throws JerboaGMapDuplicateException {
		 JerboaGMap res = new JerboaGMapArray(studio.getModeler(),gmap.getCapacity());
		 gmap.duplicateInGMap(res, this);
		 return res;
	 }

	 @Override
	 public List<Pair<String, String>> getCommandLineHelper() {
		 ArrayList<Pair<String,String>> lists = new ArrayList<>();
		 lists.add(new Pair<>("setEngine"," Ex: setEngine <rulename> <engine> (with engine: default, debug, classicV8, update, updatePara, updateV8, redun)"));
		 lists.add(new Pair<>("getEngine"," Ex: getEngine <rulename> "));
		 lists.add(new Pair<>("checkOrient"," check orientation of the GMAP "));
		 lists.add(new Pair<>("orient"," alias of checkOrient"));
		 lists.add(new Pair<>("suborbit","Ex: suborbit <DartID> <ORBIT> <SUBORBIT> as suborbit 0 <0,1,2> <0,3>"));
		 return lists;
	 }

	 @Override
	 public boolean parseCommandLine(PrintStream ps, String line) {
		 System.out.println("ECHO [ "+line+"]");
		 StringTokenizer tokenizer = new StringTokenizer(line);
		 String cmd = tokenizer.nextToken();
		 JerboaModeler modeler = studio.getModeler();
		 switch(cmd) {
		 case "setEngine":
		 {
			 String rulename = tokenizer.nextToken();
			 String enginename = tokenizer.nextToken();
			 try {
				 
				 JerboaRuleAtomic jra = (JerboaRuleAtomic) modeler.getRule(rulename);
				 jra.chooseEngine(enginename);
				 JerboaRuleEngine engine = jra.getEngine();
				 if(engine == null) {
					 System.out.println("No engine");
				 }
				 else {
					 System.out.println("Engine: "+engine.getName());
				 }
			 }
			 catch(Exception e) {
				 e.printStackTrace();
			 }
			 return true;
		 }
		 			 
		 case "getEngine": 
		 {
			 String rulename = tokenizer.nextToken();
			 try {
				 JerboaRuleAtomic jra = (JerboaRuleAtomic) modeler.getRule(rulename);
				 JerboaRuleEngine engine = jra.getEngine();
				 if(engine == null) {
					 System.out.println("No engine");
				 }
				 else {
					 System.out.println("Engine: "+engine.getName());
				 }
			 }
			 catch(Exception e) {
				 e.printStackTrace();
			 }
			 return true;
		 }
		 case "orient":
		 case "checkOrient": {
			 int countErr = 0;
			 for (JerboaDart dart : studio.getViewer().getGMap()) {
				 boolean flag = dart.ebd("orient");
				 for(int i = 0;i <= modeler.getDimension(); i++) {
					 boolean voisin = dart.alpha(i).ebd("orient");
					 if(!dart.isFree(i) && flag == voisin) {
						 System.out.println("ERROR ORIENTATION BETWEEN: "+dart+" ---"+i+"---> "+dart.alpha(i));
						 countErr++;
					 }
				 }
			 }
			 System.out.println("ORIENTATION ERROR: "+countErr);
			 return true;
		 }

		 case "suborbit": {
			 String sdartID = tokenizer.nextToken();
			 System.out.println("DartID: "+ sdartID);
			 String sorbit = tokenizer.nextToken(">");
			 System.out.println("Orbit (raw): " + sorbit);
			 String ssuborbit = tokenizer.nextToken();
			 System.out.println("SubOrbit (raw): " + ssuborbit);
			 
			 int dartID = Integer.parseInt(sdartID);
			 JerboaGMap gmap = getGMap();
			 JerboaOrbit orb = JerboaOrbit.parseOrbit(sorbit);
			 JerboaOrbit sorb = JerboaOrbit.parseOrbit(ssuborbit);
			 JerboaDart dart = gmap.getNode(dartID);
			 try {
				 long start = System.currentTimeMillis();
				 List<JerboaDart> list = gmap.collect(dart, orb, sorb);
				 long end = System.currentTimeMillis();	
				 System.out.println("Size: " + list.size());
				 System.out.println("TIME: "+(end-start) + " ms");
				 studio.getViewer().clearDartSelection();
				 studio.getViewer().addDartSelection(list);
			 } catch (JerboaException e) {
				 e.printStackTrace();
			 }
			 return true;
		 }
		 case "orbit": {
			 String sdartID = tokenizer.nextToken();
			 System.out.println("DartID: "+ sdartID);
			 String sorbit = tokenizer.nextToken("");
			 System.out.println("Orbit (raw): " + sorbit);

			 int dartID = Integer.parseInt(sdartID);
			 JerboaGMap gmap = getGMap();
			 JerboaOrbit orb = JerboaOrbit.parseOrbit(sorbit);
			 JerboaDart dart = gmap.getNode(dartID);
			 try {
				 long start = System.currentTimeMillis();
				 List<JerboaDart> list = gmap.orbit(dart, orb);
				 long end = System.currentTimeMillis();	
				 System.out.println("Size: " + list.size());
				 System.out.println("TIME: "+(end-start) + " ms");
			 } catch (JerboaException e) {
				 e.printStackTrace();
			 }
			 return true;
		 }
		 case "neworbit": {
			 String sdartID = tokenizer.nextToken();
			 int dartID = Integer.parseInt(sdartID);
			 JerboaGMap gmap = getGMap();
			 JerboaOrbit orb = JerboaOrbit.parseOrbit(tokenizer.nextToken("\0"));
			 JerboaDart dart = gmap.getNode(dartID);
			 try {
				 long start = System.currentTimeMillis();
				 List<JerboaDart> list = neworbit(gmap,dart, orb);
				 long end = System.currentTimeMillis();	
				 System.out.println("Size: " + list.size());
				 System.out.println("TIME: "+(end-start) + " ms");
			 } catch (JerboaException e) {
				 e.printStackTrace();
			 }
			 return true;
		 }
		 case "neworbit2": {
			 String sdartID = tokenizer.nextToken();
			 int dartID = Integer.parseInt(sdartID);
			 JerboaGMap gmap = getGMap();
			 JerboaOrbit orb = JerboaOrbit.parseOrbit(tokenizer.nextToken("\0"));
			 JerboaDart dart = gmap.getNode(dartID);
			 try {
				 long start = System.currentTimeMillis();
				 List<JerboaDart> list = neworbit2(gmap,dart, orb);
				 long end = System.currentTimeMillis();	
				 System.out.println("Size: " + list.size());
				 System.out.println("TIME: "+(end-start) + " ms");
			 } catch (JerboaException e) {
				 e.printStackTrace();
			 }
			 return true;
		 }

		 case "islet": {
			 String sorbit = tokenizer.nextToken("\0");
			 JerboaOrbit orb = JerboaOrbit.parseOrbit(sorbit);

			 JerboaGMap gmap = getGMap();
			 System.out.println("Islet... ");
			 List<JerboaDart> darts = studio.getViewer().getSelectedJerboaNodes();
			 int[] res;
			 try {
				 long start = System.currentTimeMillis();
				 res = JerboaIslet.islet(gmap, darts, orb);
				 long end = System.currentTimeMillis();
				 System.out.println("Islet: "+ (end-start) + " ms.");
				 System.out.print("Result: ");
				 for (int i : res) {
					 System.out.print(" "+i);
				 }
				 System.out.println();
				 System.out.print("Result (compact): ");
				 List<Integer> compactres = Arrays.stream(res).distinct().boxed().collect(Collectors.toList());
				 for (int i : compactres) {
					 System.out.print(" "+i);
				 }
				 System.out.println();	
			 } catch (JerboaNoFreeTagException e) {
				 e.printStackTrace();
			 }
			 return true;
		 }
		 case "isletseq": {
			 String sorbit = tokenizer.nextToken("\0");
			 JerboaOrbit orb = JerboaOrbit.parseOrbit(sorbit);

			 JerboaGMap gmap = getGMap();
			 System.out.println("Islet (sequential)... ");
			 List<JerboaDart> darts = studio.getViewer().getSelectedJerboaNodes();
			 int[] res;
			 try {
				 long start = System.currentTimeMillis();
				 res = JerboaIslet.islet_seq(gmap, darts, orb);
				 long end = System.currentTimeMillis();
				 System.out.println("Islet: "+ (end-start) + " ms.");
				 System.out.print("Result: ");
				 for (int i : res) {
					 System.out.print(" "+i);
				 }
				 System.out.println();
				 System.out.print("Result (compact): ");
				 List<Integer> compactres = Arrays.stream(res).distinct().boxed().collect(Collectors.toList());
				 for (int i : compactres) {
					 System.out.print(" "+i);
				 }
				 System.out.println();	
			 } catch (JerboaNoFreeTagException e) {
				 e.printStackTrace();
			 }
			 return true;
		 }
		 
		 case "isletbis": {
			 String sorbit = tokenizer.nextToken("\0");
			 JerboaOrbit orb = JerboaOrbit.parseOrbit(sorbit);

			 JerboaGMap gmap = getGMap();
			 System.out.println("Islet on all gmap... ");
			 List<JerboaDart> darts = studio.getViewer().getSelectedJerboaNodes();
			 List<JerboaDart> res;
			 try {
				 long start = System.currentTimeMillis();
				 res = JerboaIslet.islet_seq_alt(gmap, darts, orb);
				 long end = System.currentTimeMillis();
				 System.out.println("Islet: "+ (end-start) + " ms.");
				 System.out.print("Result: ");
				 for (JerboaDart i : res) {
					 System.out.print(" "+i.getID());
				 }
				 System.out.println();
			 } catch (JerboaNoFreeTagException e) {
				 e.printStackTrace();
			 }
			 return true;
		 }
		 case "json":case "gson": case "convertjson": {
			convertJSON();
			return true;
		 } 
		 case "startnet": {
			 startnet();
			 return true;
		 }
		 case "stopnet": {
			 stopnet();
			 return true;
		 }
		 case "sendgmap": {
			 sendgmap();
			 return true;
		 }
		 case "sendmodeler": {
			 sendmodeler();
			 return true;
		 }
		 
		 } // end switch

		 return false;
	 }
	 

	private void convertJSON() {
		 JerboaModelerStudio modeler = studio.getModeler();
		 JerboaStudioConverter conv = new JerboaStudioConverter(modeler, studio.getViewer());
		 String msg = conv.convertModelerToJSON();
		 System.out.println("===========================================================");
		 System.out.println(msg);
		 System.out.println("===========================================================");
		 if(JOptionPane.showConfirmDialog(null, "Save JSON output in file?", "Export JSON", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
			 JFileChooser chooser = new JFileChooser();
			 FileNameExtensionFilter filter = new FileNameExtensionFilter(
					 "JSon file", "json", "js");
			 chooser.setFileFilter(filter);
			 int returnVal = chooser.showSaveDialog(null);
			 if(returnVal == JFileChooser.APPROVE_OPTION) {
				 File fout = chooser.getSelectedFile();
				 System.out.println("You chose to open this file: " +
						 chooser.getSelectedFile().getName());
				 try {
					 PrintStream ps = new PrintStream(fout);
					 ps.println(msg);
					 ps.close();
				 } catch (FileNotFoundException e) {
					 e.printStackTrace();
				 }
			 }

		 }
		 
	}

	private void sendmodeler() {
		if(transmitter != null) {
			// transmitter.sendModeler();
			System.err.println("JMVTranmitter disabled!!!!");
			
		}
		else {
			System.err.println("JMVTranmitter is not initialized!!");
		}
	}

	private void sendgmap() {
		if(transmitter != null) {
			// transmitter.sendGMap(getGMap());
			System.err.println("JMVTranmitter disabled!!!!");
		}
		else {
			System.err.println("JMVTranmitter is not initialized!!");
		}
	}

	private void stopnet() {
		transmitter.stop();
		transmitter = null;
	}

	private void startnet() {
		JerboaModelerStudio modeler = studio.getModeler();
		// JerboaStudioJMVConverter conv = new JerboaStudioJMVConverter(modeler);
		JerboaStudioConverter conv = new JerboaStudioConverter(modeler, studio.getViewer());
		if(transmitter != null)
			stopnet();
		
		try {
			transmitter = new JMVTransmitterServer<JerboaModelerStudio>(modeler, conv);
			transmitter.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private List<JerboaDart> neworbit(JerboaGMap gmap, JerboaDart start, JerboaOrbit orbit) throws JerboaException {
		 //		
		 //		if(orbit.getMaxDim() > dimension)
		 //				throw new JerboaOrbitIncompatibleException("In orbit find a dimension "+orbit.getMaxDim()+" whereas max accepted is "+dimension);
		 ArrayList<JerboaDart> res = new ArrayList<JerboaDart>();


		 Optional<Pair<Integer, List<Integer>>> mostCycle = Arrays.stream(orbit.tab()).parallel().mapToObj(d -> {
			 List<Integer> list = Arrays.stream(orbit.tab()).filter(p -> { return (Math.abs(p - d) >= 2); }).boxed().collect(Collectors.toList());
			 return new Pair<Integer, List<Integer>>(d, list);
		 }).sorted(new Comparator<Pair<Integer, List<Integer>>>() {
			 @Override
			 public int compare(Pair<Integer, List<Integer>> o1, Pair<Integer, List<Integer>> o2) {
				 return Integer.compare(o2.r().size(), o1.r().size()); // on inverse o1 et o2 pour trier dans sens decroissant
			 }
		 }).findFirst();



		 JerboaMark marker = gmap.creatFreeMarker();
		 ArrayDeque<JerboaDart> stack = new ArrayDeque<JerboaDart>();
		 stack.push(start);
		 // TODO parallelisation du moteur
		 // synchronized(vmarkers[marker]) 
		 if(mostCycle.isPresent()) {
			 Pair<Integer, List<Integer>> norb = mostCycle.get();
			 List<Integer> otherAlpha = Arrays.stream(orbit.tab()).filter(i -> {
				 return ( i != norb.l() && !norb.r().contains(i));
			 }).mapToObj(i -> new Integer(i)).collect(Collectors.toList());

			 /*System.out.println("Initial Orbit: " + orbit);
			System.out.println("   Main Dim & cycle: " + norb.toString());
			System.out.println("   Other alpha: "+ otherAlpha);
			  */
			 while(!stack.isEmpty()) {
				 JerboaDart cur = stack.pop();
				 if(cur.isNotMarked(marker)) {
					 gmap.mark(marker,cur);
					 res.add(cur);
					 for(int a : norb.r()) {
						 final JerboaDart d = cur.alpha(a);
						 if(d.isNotMarked(marker)) {
							 gmap.mark(marker,d);
							 res.add(d);
							 for(int i : otherAlpha) {
								 stack.push(d.alpha(i));
							 }
						 }
					 }
					 JerboaDart opcur = cur.alpha(norb.l());
					 if (opcur != cur) {
						 gmap.mark(marker,opcur);
						 res.add(opcur);
						 for(int a : norb.r()) {
							 final JerboaDart d = opcur.alpha(a);
							 if(d.isNotMarked(marker)) {
								 gmap.mark(marker,d);
								 res.add(d);
								 for(int i : otherAlpha) {
									 stack.push(d.alpha(i));
								 }
							 }
						 }
					 }
				 }// end if
			 }
		 } else {
			 while(!stack.isEmpty()) {
				 JerboaDart cur = stack.pop();
				 if(cur.isNotMarked(marker)) {
					 gmap.mark(marker,cur);
					 res.add(cur);
					 for(int a : orbit.tab()) {
						 stack.push(cur.alpha(a));
					 }
				 }
			 }
		 }
		 return res;

	 }

	 private List<JerboaDart> neworbit2(JerboaGMap gmap, JerboaDart start, JerboaOrbit orbit) throws JerboaException {
		 ArrayList<JerboaDart> res = new ArrayList<JerboaDart>();
		 System.err.println(orbit);

		 ArrayList<Integer> turnerNotCycle = new ArrayList<>();
		 for(int i=0;i<orbit.size();i++) {
			 if(turnerNotCycle.size()<=0) {
				 for(int j=i+1;j<orbit.size();j++) {
					 if( Math.abs(orbit.get(i) - orbit.get(j)) < 2 ) {
						 turnerNotCycle.add(orbit.get(i));
						 turnerNotCycle.add(orbit.get(j));
						 break;
					 }
				 }
			 }else {
				 break;
			 }
		 }
		 if(orbit.size()>0 && turnerNotCycle.size()<=0) {
			 turnerNotCycle.add(orbit.get(0));
		 }


		 ArrayList<Integer> cyclesAll = new ArrayList<>();

		 ArrayList<ArrayList<Integer>> cycles = new ArrayList<>();

		 for(int i=0; i<turnerNotCycle.size();i++) {
			 Integer currentDim = turnerNotCycle.get(i);
			 cycles.add(new ArrayList<>());
			 for(Integer dim : orbit) {
				 if(dim!= currentDim &&  Math.abs(turnerNotCycle.get(i) - dim) >= 2 ) {
					 // on a un cycle avec currentDim
					 boolean notCycleWithOthers = true;
					 for(Integer otherDims : turnerNotCycle) {
						 // on verifie si c'est pas aussi un cycle avec un autre
						 if(otherDims!=currentDim && Math.abs(dim - otherDims) >= 2) {
							 notCycleWithOthers = false;
						 }
					 }
					 if(notCycleWithOthers)
						 cycles.get(i).add(dim);
					 else {
						 if(!cyclesAll.contains(dim)) {
							 cyclesAll.add(dim);
						 }
					 }
				 }
			 }
		 }
		 // TODO : faire la liste des dimensions qui cyclent avec les 2 dimensions choisies
		 System.err.println(turnerNotCycle);
		 System.err.println(cycles);
		 System.err.println(cyclesAll);



		 JerboaMark marker = gmap.creatFreeMarker();
		 ArrayDeque<JerboaDart> stack = new ArrayDeque<JerboaDart>();
		 stack.push(start);
		 // TODO : g�rer le cas ou tout est cycle
		 while(!stack.isEmpty()) {
			 JerboaDart currentDart = stack.pop();
			 if(currentDart.isNotMarked(marker)) {
				 // todo : ajout des voisin par cycle qui sont cycle pour les 2 dimensions choisies

				 for(Integer dimAllCycle : cyclesAll) {
					 if(currentDart.isNotMarked(marker))
						 stack.add(currentDart.alpha(dimAllCycle));
				 }
				 JerboaDart tmp = currentDart;
				 int curDimId = 0;
				 do{
					 gmap.mark(marker, tmp);
					 res.add(tmp);
					 for(Integer dimCycle : cycles.get(curDimId)) {
						 stack.add(tmp.alpha(dimCycle));
					 }
					 curDimId = (curDimId+1)%2;
					 tmp = tmp.alpha(turnerNotCycle.get(curDimId));
					 // tant qu'on arrive pas au bout
				 }while(tmp.getID() != currentDart.getID()
						 && tmp.alpha(turnerNotCycle.get(curDimId)).getID() != tmp.getID());
				 if(tmp.getID()!=currentDart.getID()) {
					 // si on a pas cycl� on repart de l'autre bout 
					 tmp = currentDart;
					 curDimId = 1;
					 do{
						 gmap.mark(marker, tmp);
						 res.add(tmp);
						 for(Integer dimCycle : cycles.get(curDimId)) {
							 stack.add(tmp.alpha(dimCycle));
						 }
						 curDimId = (curDimId+1)%2;
						 tmp = tmp.alpha(turnerNotCycle.get(curDimId));
						 // tant qu'on arrive pas au bout
					 }while(tmp.getID() != currentDart.getID()
							 && tmp.alpha(turnerNotCycle.get(curDimId)).getID() != tmp.getID());
				 }
			 }
		 }

		 gmap.freeMarker(marker);
		 return res;

	 }

	 @Override
	 public boolean hasOrient() {
		 return false;
	 }

	 @Override
	 public boolean getOrient(JerboaDart n) {
		 return false;
		 // return n.ebd("orient");
	 }


	 // ON FAIT UNE COPIE DE SURFACE!!! ATTENTION CE N EST PAS BIEN
	 // MAIS SOUVENT SUFFISANT POUR DU DEBUG
	 @Override
	 public Object duplicate(JerboaEmbeddingInfo info, Object value) {
		 return value;
	 }

	 @Override
	 public JerboaEmbeddingInfo convert(JerboaEmbeddingInfo info) {
		 return info;
	 }

	 @Override
	 public boolean manageEmbedding(JerboaEmbeddingInfo info) {
		 switch(info.getName()) {
		 case "point":case "position": return true;
		 case "color":return true;
		 case "orient":return false;
		 }
		 // System.out.println("EXTRA EMBEDDING: "+info);
		 return false;
	 }
	 // END COPIE DE SURFACE

	 @Override
	 public JerboaModeler getModeler() {
		 return studio.getModeler();
	 }

	 @Override
	 public JerboaGMap getGMap() {
		 return getModeler().getGMap();
	 }

	 @Override
		public void loadFile(String filepath) {
			loadFile(new File(filepath), FILEFORMAT.AUTO, new JerboaMonitorInfoConsole());
		}

 }
