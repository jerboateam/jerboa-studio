package fr.up.xlim.ig.jerboa.studio.infer.geometry.poi;

import java.util.List;

import fr.up.xlim.ig.jerboa.studio.infer.geometry.system.LinearItem;
import fr.up.xlim.sic.ig.jerboa.jme.model.JMENode;

/**
 * Strategy to generate the points of interests
 * 
 * @author romain
 *
 */
public interface PoIStrategy {
	List<LinearItem> getItems(JMENode node);
}
