package fr.up.xlim.ig.jerboa.studio.infer.geometry.system;

import fr.up.xlim.ig.jerboa.studio.infer.geometry.poi.PointOfInterest;

/**
 * Class used to encode the linear system given to the solver.
 * 
 * @author Romain and Hakim
 * 
 *
 */
public class LinearItem {

	private Double coef;
	private PointOfInterest<?> poi;

	public LinearItem(Double coef, PointOfInterest<?> poi) {
		this.coef = coef;
		this.poi = poi;
	}

	public Double getCoef() {
		return coef;
	}

	public void setCoef(Double coef) {
		this.coef = coef;
	}

	public PointOfInterest<?> getPoi() {
		return poi;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("< coef=");
		builder.append(coef);
		builder.append("; poi=");
		builder.append(poi);
		builder.append(">");
		return builder.toString();
	}

}
